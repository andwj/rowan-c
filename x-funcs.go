// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"
// import "fmt"
// import "strings"
// import "unicode"

const CODE_MAX_LINE = 90

func (gen *Generator) BuildAllOutFuncs() {
	for _, def := range cur_mod.ord_defs {
		switch def.kind {
		case DEF_Func:
			out_name := gen.EncodeName(def.d_func.name)
			def.d_func._out = NewOutThing(ODEF_Func, out_name)

		case DEF_Method:
			recv_type := def.d_func.ty.param[0].ty.sub
			meth_name := gen.MakeMethodName(def.d_func.name, recv_type)
			out_name  := gen.EncodeName(meth_name)
			def.d_func._out = NewOutThing(ODEF_Func, out_name)
		}
	}

	gen.CreateAbortFuncs()
}

func (gen *Generator) CreateAbortFuncs() {
	out := NewOutThing(ODEF_Func, "__rw_abort")
	out.forward  = "/* extern */ _rwNoReturn " + out.name + "(uint8_t* msg);"
	out.required = true

	gen.AddThing(out)

	out = NewOutThing(ODEF_Func, "__rw_null_abort")
	out.forward  = "/* extern */ _rwNoReturn " + out.name + "(uint8_t* func, int line);"
	out.required = true

	gen.AddThing(out)

	out = NewOutThing(ODEF_Func, "__rw_index_abort")
	out.forward  = "/* extern */ _rwNoReturn " + out.name + "(uint8_t* func, int line);"
	out.required = true

	gen.AddThing(out)
}

//----------------------------------------------------------------------

func (gen *Generator) CompileFunction(fu *FuncDef) {
	// inline functions never exist in the output assembly
	if fu.inline {
		return
	}

	out := fu._out

	gen.AddThing(out)

	// create a forward decl
	qual    := gen.FuncQualifier(fu)
	ret_str := gen.FuncReturnType(fu)
	par_str := gen.ConvertParameters(fu.ty, nil)

	decl_str := qual + ret_str + " " + out.name + par_str
	out.forward = decl_str + ";"

	if fu.extern {
		// the forward decl is enough
		out.required = true
		return
	}

	if cur_mod == main_mod && fu.name == "main" {
		gen.need_main = true
	}

	// compile body

	gen.fu = fu

	out.Token(decl_str)
	out.Token(" ")
	out.BeginBlock()

//	Dumpty(fu.name, fu.body, 0)

	gen.FuncLocalVars()
	gen.FuncStackVars()

	for _, t := range fu.body.children {
		gen.CompileNode(t)
	}

	out.EndBlock()
	out.FlushLine(0)
}

func (gen *Generator) FuncQualifier(fu *FuncDef) string {
	qual := ""

	if fu.extern {
		qual = "/* extern */ "
	} else if !fu.public {
		qual = "static "
	}
	return qual
}

func (gen *Generator) FuncReturnType(fu *FuncDef) string {
	ret_type := fu.ty.sub

	if ret_type.kind == TYP_NoReturn {
		return "_rwNoReturn"
	}

	return gen.ConvertType(ret_type, false, nil)
}

func (gen *Generator) FuncLocalVars() {
	fu  := gen.fu
	out := fu._out

	seen := make(map[string]bool)

	for _, local := range fu.locals {
		name := gen.LocalName(local, seen)
		local.out_name = name

		if local.param_idx < 0 {
			type_name := gen.ConvertType(local.ty, true, nil)
			full_def  := gen.FormVarDefinition(type_name, name)

			out.Token(full_def)
			out.Token(";")
			out.FlushLine(0)
		}
	}

	if len(seen) > 0 {
		out.FlushLine(0)
	}
}

func (gen *Generator) LocalName(local *LocalInfo, seen map[string]bool) string {
	name := gen.EncodeName(local.name)

	if !seen[name] {
		seen[name] = true
		return name
	}

	// need a suffix to distinguish it

	suffix := 2
	for {
		new_name := name + "__" + strconv.Itoa(suffix)
		if !seen[new_name] {
			seen[new_name] = true
			return new_name
		}
		suffix += 1
	}
}

func (gen *Generator) FuncStackVars() {
	fu  := gen.fu
	out := fu._out

	fu.stack_vars = make([]string, 0)

	for _, t := range fu.body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_StackVar {
			if comp.ty.kind != TYP_Pointer {
				panic("weird NH_StackVar type")
			}
			real_type := comp.ty.sub

			// store the name
			comp.offset = int64(len(fu.stack_vars))

			name := "_stackvar" + strconv.Itoa(len(fu.stack_vars))
			fu.stack_vars = append(fu.stack_vars, name)

			// create the C variable
			type_name := gen.ConvertType(real_type, true, nil)
			full_def  := gen.FormVarDefinition(type_name, name)

			out.Token(full_def)
			out.Token(";")
			out.FlushLine(0)
		}
	}

	if len(fu.stack_vars) > 0 {
		out.FlushLine(0)
	}
}

//----------------------------------------------------------------------

func (gen *Generator) CompileNode(t *Node) {
	switch t.kind {
	case NH_Label:
		gen.DoLabel(t)
		return

	case NH_Comment:
		gen.DoComment(t)
		return

	case NH_Drop:
		gen.DoDrop(t)

	case NH_Move:
		gen.DoMove(t, false)

	case NH_MemWrite:
		gen.DoMove(t, true)

	case NH_Swap:
		gen.DoSwap(t)

	case NH_Jump:
		gen.DoJump(t)

	case NH_Return:
		gen.DoReturn(t)

	case NH_Abort:
		gen.DoAbort(t)

	case NH_ChkPtr:
		gen.DoCheckPointer(t)

	case NH_ChkIndex:
		gen.DoCheckIndex(t)

	default:
		panic("unknown node kind in CompileNode")
	}
}

func (gen *Generator) DoLabel(t *Node) {
	out := gen.fu._out

	out_name := gen.EncodeLabel(t.str)

	out.Token(out_name)
	out.Token(": {}")
	out.FlushLine(0)
}

func (gen *Generator) DoComment(t *Node) {
/*
	out := gen.fu._out

	if t.str == "" {
		out.FlushLine(0)
	} else {
		out.Token("// " + t.str)
		out.FlushLine(0)
	}
*/
}

func (gen *Generator) DoDrop(t *Node) {
	out := gen.fu._out

	gen.CalcComputation(t.children[0])
	out.Token(";")
	out.FlushLine(0)
}

func (gen *Generator) DoMove(t *Node, mem_write bool) {
	t_dest := t.children[0]
	t_comp := t.children[1]

	out := gen.fu._out

	// WISH: for mem_write, prevent being optimized away

	gen.OperandNode(t_dest)

	out.Token(" = ")

	gen.CalcComputation(t_comp)
	out.Token(";")
	out.FlushLine(0)
}

func (gen *Generator) DoSwap(t *Node) {
	out := gen.fu._out

	t_left  := t.children[0]
	t_right := t.children[1]

	out.BeginBlock()

	// copy LHS to a temporary var
	tmp_var := "__swap"

	type_name := gen.ConvertType(t_left.ty, true, nil)
	full_def  := gen.FormVarDefinition(type_name, tmp_var)

	out.Token(full_def)
	out.Token(" = ")
	gen.OperandNode(t_left)

	out.Token(";")
	out.FlushLine(0)

	// copy RHS to LHS
	gen.OperandNode(t_left)
	out.Token(" = ")
	gen.OperandNode(t_right)

	out.Token(";")
	out.FlushLine(0)

	// copy temp var to RHS
	gen.OperandNode(t_right)
	out.Token(" = ")
	out.Token(tmp_var)
	out.Token(";")
	out.FlushLine(0)

	out.EndBlock()
	out.FlushLine(0)
}

func (gen *Generator) DoJump(t *Node) {
	out := gen.fu._out

	out_name := gen.EncodeLabel(t.str)

	if t.Len() > 0 {
		t_cond := t.children[0]
		out.Token("if (")

		if t.negate {
			out.Token("! ")
		}
		gen.CalcComputation(t_cond)

		out.Token(") ")
		out.BeginBlock()
	}

	out.Token("goto ")
	out.Token(out_name)
	out.Token(";")
	out.FlushLine(0)

	if t.Len() > 0 {
		out.EndBlock()
		out.FlushLine(0)
	}
}

func (gen *Generator) DoReturn(t *Node) {
	out := gen.fu._out

	out.Token("return")

	if t.Len() > 0 {
		out.Token(" ")
		gen.CalcComputation(t.children[0])
	}

	out.Token(";")
	out.FlushLine(0)
}

func (gen *Generator) DoAbort(t *Node) {
	out := gen.fu._out

	msg_str := "(uint8_t *)" + gen.ConvertStringUTF8(t.str)

	out.Token("__rw_abort(")
	out.Token(msg_str)
	out.Token(");")
	out.FlushLine(0)
}

func (gen *Generator) DoCheckPointer(t *Node) {
	out := gen.fu._out

	t_arg := t.children[0]

	out.Token("if (_rwNULL == ")
	gen.OperandNode(t_arg)
	out.Token(") ")

	out.BeginBlock()
	{
		func_str := "(uint8_t *)" + gen.ConvertStringUTF8(gen.fu.name)
		line_str := strconv.Itoa(t.pos.line)

		out.Token("__rw_null_abort(")
		out.Token(func_str)
		out.Token(", ")
		out.Token(line_str)
		out.Token(");")
		out.FlushLine(0)
	}
	out.EndBlock()
	out.FlushLine(0)
}

func (gen *Generator) DoCheckIndex(t *Node) {
	out := gen.fu._out

	t_arg := t.children[0]

	cast_str := ""
	if !t_arg.ty.unsigned {
		cast_str = strconv.Itoa(t_arg.ty.bits)
		cast_str = "(uint" + cast_str + "_t)"
	}

	bound_str := strconv.Itoa(int(t.offset))

	out.Token("if (")
	out.Token(bound_str)
	out.Token(" <= ")
	out.Token(cast_str)
	gen.OperandNode(t_arg)
	out.Token(") ")

	out.BeginBlock()
	{
		func_str := "(uint8_t *)" + gen.ConvertStringUTF8(gen.fu.name)
		line_str := strconv.Itoa(t.pos.line)

		out.Token("__rw_index_abort(")
		out.Token(func_str)
		out.Token(", ")
		out.Token(line_str)
		out.Token(");")
		out.FlushLine(0)
	}
	out.EndBlock()
	out.FlushLine(0)
}

//----------------------------------------------------------------------

func (gen *Generator) CalcComputation(t *Node) {
	switch t.kind {
	case NC_Call:
		gen.CalcCall(t, false)

	case NC_OpSeq:
		gen.CalcOpSeq(t)

	case NC_MemRead:
		gen.CalcMemRead(t)

	case NC_MultiEq:
		gen.CalcMultiEq(t)

	case NC_StackVar:
		gen.CalcStackVar(t)

	default:
		// node must be a normal operand
		gen.OperandNode(t)
	}
}

func (gen *Generator) CalcCall(t *Node, is_tail bool) {
	out := gen.fu._out

	t_func := t.children[0]
	params := t.children[1:]

	fun_type := t_func.ty
	if fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}
	if fun_type.kind != TYP_Function {
		panic("weird fun type")
	}

	switch t_func.kind {
	case NP_Method:
		recv_type := params[0].ty.sub
		recv_mod  := recv_type.def.module

		meth_name := gen.MakeMethodName(t_func.str, recv_type)
		out_name  := gen.EncodeFullName(meth_name, recv_mod)

		out.Token(out_name)

	default:
		gen.OperandNode(t_func)
	}

	out.Token("(")

	for i, t_par := range params {
		if i > 0 {
			out.Token(", ")
			out.FlushLine(CODE_MAX_LINE)
		}
		gen.OperandNode(t_par)
	}

	out.Token(")")
}

func (gen *Generator) CalcMemRead(t *Node) {
	// WISH: for mem_read, prevent being optimized away

	gen.OperandNode(t.children[0])
}

func (gen *Generator) CalcMultiEq(t *Node) {
	out := gen.fu._out

	out.Token("(")

	t_main := t.children[0]

	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		if i > 1 {
			out.Token(" ||")
			out.FlushLine(CODE_MAX_LINE)
			out.Token(" ")
		}
		gen.OperandNode(t_main)
		out.Token(" == ")
		gen.OperandNode(t_arg)
	}
	out.Token(")")
}

func (gen *Generator) CalcStackVar(t *Node) {
	fu  := gen.fu
	out := gen.fu._out

	out.Token("&")
	out.Token(fu.stack_vars[t.offset])
}

//----------------------------------------------------------------------

func (gen *Generator) CalcOpSeq(t *Node) {
	// convert the sequence of operations into a tree, which we can
	// then handle recursively.
	tree := gen.TransformOpSeq(t, t.Len() - 1)

	gen.MathNode(tree)
}

func (gen *Generator) TransformOpSeq(t *Node, pos int) *Node {
	op := t.children[pos]
	if pos > 0 {
		lhs := gen.TransformOpSeq(t, pos - 1)
		op.Add(lhs)
	}
	return op
}

func (gen *Generator) MathNode(t *Node) {
	out := gen.fu._out

	if t.kind != NX_Operator {
		gen.OperandNode(t)
		return
	}

	is_unary := t.Len() < 2

	// handle some things which are not C operators

	switch t.str {
	case "conv":
		gen.MathConversion(t)

	case "raw-cast":
		gen.MathRawCast(t)

	case "endian-swap":
		gen.MathEndianSwap(t)

	case "iabs":
		gen.MathIntAbs(t)

	case "imin":
		gen.MathMinMax(t, "min", "<")

	case "imax":
		gen.MathMinMax(t, "max", ">")

	case "fremt":   gen.MathFloatFunc(t, "fmodf", "fmod", 1)

	case "fabs":    gen.MathFloatFunc(t, "fabsf", "fabs", 1)
	case "fsqrt":   gen.MathFloatFunc(t, "sqrtf", "sqrt", 1)
	case "fmin":    gen.MathFloatFunc(t, "fminf", "fmin", 1)
	case "fmax":    gen.MathFloatFunc(t, "fmaxf", "fmax", 1)

	case "fround":  gen.MathFloatFunc(t, "roundf", "round", 1)
	case "ftrunc":  gen.MathFloatFunc(t, "truncf", "trunc", 1)
	case "ffloor":  gen.MathFloatFunc(t, "floorf", "floor", 1)
	case "fceil":   gen.MathFloatFunc(t, "ceilf",  "ceil",  1)

	case "finf?":   gen.MathFloatFunc(t, "isinf",  "isinf", 1)
	case "fnan?":   gen.MathFloatFunc(t, "isnan",  "isnan", 1)

	default:
		out.Token("(")

		if !is_unary {
			gen.MathNode(t.Last())
			out.Token(" ")
		}

		out.Token(t.str)
		out.Token(" ")

		gen.MathNode(t.children[0])

		out.Token(")")
	}
}

func (gen *Generator) MathFloatFunc(t *Node, name32 string, name64 string, where int) {
	out := gen.fu._out

	if where == 1 {
		gen.NeedHeader("<math.h>")
	}

	if t.ty.bits > 32 {
		out.Token(name64)
	} else {
		out.Token(name32)
	}

	out.Token("(")

	if t.Len() > 1 {
		gen.MathNode(t.Last())
		out.Token(", ")
	}

	gen.MathNode(t.children[0])
	out.Token(")")
}

func (gen *Generator) MathConversion(t *Node) {
	out := gen.fu._out

	t_value := t.children[0]

	type_str := gen.ConvertType(t.ty, false, nil)

	out.Token("(")
	out.Token(type_str)
	out.Token(")")

	gen.MathNode(t_value)
}

func (gen *Generator) MathEndianSwap(t *Node) {
	out := gen.fu._out

	t_value := t.children[0]

	// nothing needed for byte-sized values
	if t.ty.bits <= 8 {
		gen.MathNode(t_value)
		return
	}

	gen.need_endian = true

	bit_str := strconv.Itoa(t.ty.bits)

	fun_name   := "_rwSwap" + bit_str
	s_type_str := "uint" + bit_str + "_t"
	d_type_str := gen.ConvertType(t.ty, false, nil)

	if !t.ty.unsigned {
		out.Token("(")
		out.Token(d_type_str)
		out.Token(")")
	}

	out.Token(fun_name)
	out.Token("(")

	if !t.ty.unsigned {
		out.Token("(")
		out.Token(s_type_str)
		out.Token(")")
	}

	gen.MathNode(t_value)
	out.Token(")")
}

func (gen *Generator) MathRawCast(t *Node) {
	out := gen.fu._out

	t_value := t.children[0]

	if t.ty.kind == TYP_Float || t_value.ty.kind == TYP_Float {
		gen.MathRawCast_Float(t, t_value)
		return
	}

	// bools, integers and pointers are all essentially the same
	// thing, hence can use the normal C casting syntax.

	type_str := gen.ConvertType(t.ty, false, nil)

	out.Token("(")
	out.Token(type_str)
	out.Token(")")

	gen.MathNode(t_value)
}

func (gen *Generator) MathRawCast_Float(t *Node, t_value *Node) {
	out := gen.fu._out

	s_type := t_value.ty
	d_type := t.ty

	s_abbr := gen.MathAbbreviateType(s_type)
	d_abbr := gen.MathAbbreviateType(d_type)

	fun_name := "_cast_" + s_abbr + "_to_" + d_abbr

	if !gen.HasSpecFunc(fun_name) {
		// generate a special func to do the raw cast
		f := NewOutThing(ODEF_Func, fun_name)

		s_type_str := gen.ConvertType(s_type, false, nil)
		d_type_str := gen.ConvertType(d_type, false, nil)

		f.Token("static inline ")
		f.Token(d_type_str)
		f.Token(" ")
		f.Token(fun_name)
		f.Token("(")
		f.Token(s_type_str)
		f.Token(" val) ")

		f.BeginBlock()

		f.Token("union ")
		f.BeginBlock()
		{
			f.Token(s_type_str)
			f.Token(" s;")
			f.FlushLine(0)

			f.Token(d_type_str)
			f.Token(" d;")
			f.FlushLine(0)

			f.EndBlock()
		}
		f.Token(" u;")
		f.FlushLine(0)

		f.Token("u.s = val;")
		f.FlushLine(0)
		f.Token("return u.d;")

		f.EndBlock()
		f.FlushLine(0)

		gen.AddSpecFunc(f)
	}

	out.Token(fun_name)
	out.Token("(")
	gen.MathNode(t_value)
	out.Token(")")
}

func (gen *Generator) MathIntAbs(t *Node) {
	out := gen.fu._out

	t_value := t.children[0]

	if t.ty.unsigned {
		gen.MathNode(t_value)
		return
	}

	gen.NeedHeader("<math.h>")

	// use the libc `llabs` function for 64 bits
	if t.ty.bits == 64 {
		out.Token("llabs(")
		gen.MathNode(t_value)
		out.Token(")")
		return
	}

	// use the libc `abs` function for 32 bits OR LESS.
	// if less, we rely on integer promotion of the argument, and
	// add a suitable cast to produce the smaller type.

	if t.ty.bits < 32 {
		d_type_str := gen.ConvertType(t.ty, false, nil)
		out.Token("(")
		out.Token(d_type_str)
		out.Token(")")
	}

	out.Token("abs(")
	gen.MathNode(t_value)
	out.Token(")")
}

func (gen *Generator) MathMinMax(t *Node, name string, op string) {
	out := gen.fu._out

	value_a := t.children[1]
	value_b := t.children[0]

	abbr     := gen.MathAbbreviateType(t.ty)
	fun_name := "_" + name + "_" + abbr

	if !gen.HasSpecFunc(fun_name) {
		// generate a special func to do the raw cast
		f := NewOutThing(ODEF_Func, fun_name)

		type_str := gen.ConvertType(t.ty, false, nil)

		f.Token("static inline ")
		f.Token(type_str)
		f.Token(" ")
		f.Token(fun_name)
		f.Token("(")
		f.Token(type_str)
		f.Token(" a, ")
		f.Token(type_str)
		f.Token(" b) ")

		f.BeginBlock()
		{
			f.Token("if (a ")
			f.Token(op)
			f.Token(" b) return a;")
			f.FlushLine(0)

			f.Token("else return b;")
			f.FlushLine(0)
		}
		f.EndBlock()
		f.FlushLine(0)

		gen.AddSpecFunc(f)
	}

	out.Token(fun_name)
	out.Token("(")
	gen.MathNode(value_a)
	out.Token(", ")
	gen.MathNode(value_b)
	out.Token(")")
}

func (gen *Generator) MathAbbreviateType(ty *Type) string {
	if ty.kind == TYP_Float {
		return "f" + strconv.Itoa(ty.bits)
	} else if ty.unsigned {
		return "u" + strconv.Itoa(ty.bits)
	} else {
		return "s" + strconv.Itoa(ty.bits)
	}
}

//----------------------------------------------------------------------

func (gen *Generator) OperandNode(t *Node) {
	if t.IsLiteral() {
		gen.OpLiteral(t)
		return
	}

	switch t.kind {
	case NP_Local:
		gen.OpLocal(t)

	case NP_GlobPtr:
		gen.OpGlobPtr(t, true)

	case NP_FuncRef:
		gen.OpFuncRef(t)

	case NP_CSize:
		gen.OpLiteral(t)

	case NM_Deref:
		gen.OpDeref(t)

	case NM_Field:
		gen.OpField(t)

	case NM_Index:
		gen.OpIndex(t)

	default:
		panic("unsupported operand node")
	}
}

func (gen *Generator) OpLiteral(t *Node) {
	out := gen.fu._out
	val_str := gen.SimpleDataValue(t, t.ty, nil)
	out.Token(val_str)
}

func (gen *Generator) OpLocal(t *Node) {
	out := gen.fu._out
	out.Token(t.local.out_name)
}

func (gen *Generator) OpGlobPtr(t *Node, get_addr bool) {
	out := gen.fu._out
	def := t.def

	if def.kind == DEF_Func {
		dep := def.d_func._out
		out.AddDependency(dep)
	}

	full_name := gen.EncodeFullName(t.str, t.module)

	if get_addr {
		out.Token("&")
	}
	out.Token(full_name)
}

func (gen *Generator) OpFuncRef(t *Node) {
	out := gen.fu._out
	dep := t.def.d_func._out

	out.AddDependency(dep)
	out.Token(dep.name)
}

func (gen *Generator) OpDeref(t *Node) {
	out := gen.fu._out

	base := t.children[0]

	if base.kind == NP_GlobPtr {
		gen.OpGlobPtr(base, false)
		return
	}

	out.Token("*(")
	gen.OperandNode(base)
	out.Token(")")
}

func (gen *Generator) OpField(t *Node) {
	out := gen.fu._out

	out_name := gen.EncodeFieldName(t.str)

	base := t.children[0]

	out.Token("(")
	gen.OperandNode(base)
	out.Token(")")

	if base.ty.kind == TYP_Pointer {
		out.Token("->")
	} else {
		out.Token(".")
	}

	out.Token(out_name)
}

func (gen *Generator) OpIndex(t *Node) {
	out := gen.fu._out

	out.Token("(")

	base    := t.children[0]
	indexor := t.children[1]

	// force base to the exact type
	ptr_str := gen.ConvertType(base.ty, false, nil)

	out.Token("(")
	out.Token(ptr_str)
	out.Token(")")

	gen.OperandNode(base)
	out.Token(")")

	out.Token("[")
	gen.OperandNode(indexor)
	out.Token("]")
}

//----------------------------------------------------------------------

func (gen *Generator) EncodeLabel(s string) string {
	if s[0] == '_' {
		return "lab2" + gen.EncodeName(s)
	} else {
		return "lab_" + gen.EncodeName(s)
	}
}

func (gen *Generator) MakeMethodName(name string, recv_type *Type) string {
	return recv_type.def.name + "__" + name
}
