// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
//import "runtime"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	out_dir string

	// the architecture
	arch_bits  int
	big_endian bool

	no_optim   bool
	all_errors bool
	loc_abort  bool
	help       bool
}

var all_filenames []string

//----------------------------------------------------------------------

func main() {
	InitErrConsts()
	ClearErrors()

	all_filenames = make([]string, 0)

	InitModules()

	// this sets up the `arch` global var.
	// it will fatal error on invalid arguments.
	HandleArgs()

	ClearErrors()

	InitTypes()
	InitOperators()

	// read all the tokens from every file
	// [ the only errors will be file errors, lexing and bad/dup names ]
	LoadEverything()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	CreateOrderedDefs()

	CompileAllConstants()
	CompileAllTypes()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	DeduceAllModules()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	FinalizeTypes()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	ParseAllModules()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	InlineAllFunctions()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	CompileAllModules()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	os.Exit(0)
}

func InitErrConsts() {
	// some important global constants
	OK        = nil
	FAILED    = fmt.Errorf("FAILED")
	P_FAIL    = NewNode(NL_ERROR, "", Position{})
	P_UNKNOWN = NewNode(NL_ERROR, "", Position{})
}

func HandleArgs() {
	Options.arch_bits  = 64
	Options.big_endian = false

	argv.Generic("o", "output",     &Options.out_dir, "file", "name of output file/dir")
	argv.Integer("a", "arch",       &Options.arch_bits, "bits", "CPU architecture (32 or 64)")
	argv.Enabler("b", "big-endian", &Options.big_endian, "specify CPU is big endian ")
	argv.Enabler("e", "all-errors", &Options.all_errors, "remove limit on errors per function")
	argv.Enabler("N", "no-optim",   &Options.no_optim, "disable all optimizations")
	argv.Enabler("X", "loc-abort",  &Options.loc_abort, "abort function is not extern")
	argv.Enabler("h", "help",       &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	switch Options.arch_bits {
	case 32, 64:  // ok
	default:
		FatalError("only 32 and 64 bits supported")
	}

	if Options.help {
		ShowUsage()
		os.Exit(0)
	}

	filenames := argv.Unparsed()

	if len(filenames) == 0 {
		FatalError("no input files")
	}

	// either have one ".list" file or multiple ".rx" files

	if FileHasExtension(filenames[0], "list") {
		if len(filenames) > 1 {
			FatalError("additional filenames to %s", filenames[0])
		}
		LoadModuleList(filenames[0])

	} else {
		for _, name := range filenames {
			if FileHasExtension(name, "list") {
				FatalError("additional filenames to %s", name)
			}
		}
		BuildSingleModule(filenames)
	}
}

func ShowUsage() {
	fmt.Printf("Usage: rowan [Build.list] [OPTIONS]\n")

	fmt.Printf("\n")
	fmt.Printf("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	if cur_mod != nil {
		if cur_mod.out_fp != nil {
			cur_mod.out_fp.Close()

			// remove the useless file
			os.Remove(cur_mod.out_file)
		}
	}
	if format != "" {
		format = "rowan: fatal: " + format + "\n"
		fmt.Fprintf(os.Stderr, format, a...)
	}
	os.Exit(1)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

func AddFilename(name string) int {
	// file numbers are (1 + index)
	all_filenames = append(all_filenames, name)
	return len(all_filenames)
}

//----------------------------------------------------------------------

func LoadEverything() {
	// visit all files, even if some produce an error
	for _, mod := range ordered_mods {
		for _, filename := range mod.files {
			LoadCodeFile(mod, filename)
		}
	}
}

func LoadCodeFile(mod *Module, fullname string) {
	cur_mod = mod

	f_idx := AddFilename(fullname)

	read_fp, err := os.Open(fullname)
	if err != nil {
		Error_SetFile(0)

		if os.IsNotExist(err) {
			PostError("no such file: %s", fullname)
		} else {
			PostError("could not open file: %s", fullname)
		}
		return
	}

	f := NewInputFile(fullname, f_idx, read_fp)

	f.Load()

	read_fp.Close()
}
