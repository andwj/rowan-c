// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"
import "fmt"

// this indicates that a parsing function which normally returns a
// Node pointer has failed, and posted an error via PostError.
var P_FAIL *Node

// this indicates that the expression parser encountered a constant
// which has not been evaluated yet.
var P_UNKNOWN *Node

func DeduceAllFunctions() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Func || def.kind == DEF_Method {
			def.d_func.Deduce()
		}
	}
	Error_SetWhat("")
}

func ParseAllFunctions(inline bool) {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Func || def.kind == DEF_Method {
			if !def.d_func.extern && def.d_func.inline == inline {
				def.d_func.Parse()
			}
		}
	}
	Error_SetWhat("")
}

func CompileAllFunctions() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Func || def.kind == DEF_Method {
			cur_mod.gen.CompileFunction(def.d_func)
		}
	}
	Error_SetWhat("")
}

//----------------------------------------------------------------------

// Deduce determines the overall type of the function.
// It does not examine the body of the function.
func (fu *FuncDef) Deduce() error {
	Error_SetWhat(fu.name)
	Error_SetPos(fu.raw_pars.pos)

	fu.ty = ParseParameters(fu.raw_pars)
	if fu.ty == nil {
		return FAILED
	}
	if ValidateType(fu.ty) != OK {
		return FAILED
	}

	// store methods into the Type which they belong, and check
	// for duplicate names.
	if fu.method {
		if len(fu.ty.param) == 0 {
			PostError("method definition with no receiver")
			return FAILED
		}
		self_type := fu.ty.param[0].ty
		if self_type.kind != TYP_Pointer {
			PostError("method receiver must be a pointer, got a %s",
				self_type.SimpleName())
			return FAILED
		}
		belong_type := self_type.sub
		if belong_type.def == nil {
			PostError("method receiver must refer to a custom type")
			return FAILED
		}
		if belong_type.FindMethod(fu.name) != nil {
			PostError("method '%s' already defined in type %s",
				fu.name, belong_type.def.name)
			return FAILED
		}
		belong_type.AddMethod(fu)
	}

	fu.ptr = NewPointerType(fu.ty)
	return OK
}

//----------------------------------------------------------------------

// Parse analyses the code in the function and produces higher-level
// nodes for the Compile method to use.  it will decompose `loop` and
// `if` forms into simpler forms, and create temporaries for usage of
// expressions in `()` parentheses.
func (fu *FuncDef) Parse() error {
	Error_SetWhat(fu.name)

	// setup some stuff
	fu.labels = make(map[string]*LabelInfo)
	fu.locals = make([]*LocalInfo, 0)
	fu.params = make([]*LocalInfo, 0)
	fu.jumps  = make([]*LabelInfo, 0)

	blk := NewBlockInfo("function")
	blk.tail  = true

	// create a local for each parameter
	for idx, par := range fu.ty.param {
		local := fu.NewLocal(par.name, par.ty, blk)
		local.param_idx = idx
		fu.params = append(fu.params, local)

		// self is immutable
		if idx == 0 && fu.method {
			local.self      = true
			local.immutable = true
		}
	}

	// we will build a new body like Arnie.
	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	// lack of a needed `return` is not detect by ParseBlock or ParseLine code,
	// but it will be detected by the FlowAnalysis pass.

	if fu.ParseBlock(old_body, blk) == P_FAIL {
		return FAILED
	}

	if fu.MissingLabels() != OK {
		return FAILED
	}

	// ensure the body is never empty (for live ranges)
	if fu.body.Len() == 0 {
		cmt := NewNode(NH_Comment, "empty function", fu.body.pos)
		fu.body.Add(cmt)
	}

	if fu.FlowAnalysis() != OK {
		return FAILED
	}

	fu.DeadCodeRemoval()
	fu.UnusedLocals()

	Dumpty(fu.name, fu.body, 0)

	return OK
}

func (fu *FuncDef) ParseBlock(block *Node, info *BlockInfo) *Node {
	// for a "value" block, result will be the computation in last line
	// of the block, or NIL for something `void` or `no-return`.
	//
	// otherwise, result is either NIL or P_FAIL.
	//
	// for a "tail" block, a plain computation in the last line will become
	// an implicit return.

	info.parent = fu.cur_block

	// empty block?
	if block.Len() == 0 {
		return nil
	}

	fu.cur_block = info

	// if last thing is a computation (including literals), return it
	var result *Node

	for idx := 0; idx < block.Len(); idx++ {
		line    := block.children[idx]
		is_last := (idx == block.Len() - 1)

		result = fu.ParseLine(line, is_last)

		if result == P_FAIL {
			// allow a limited number of errors per function
			fu.error_num += 1
			if fu.error_num > 5 && !Options.all_errors {
				break
			}
		}
	}

	fu.cur_block = info.parent

	if fu.error_num > 0 {
		return P_FAIL
	}
	return result
}

func (fu *FuncDef) ParseLine(line *Node, is_last bool) *Node {
	// note that lines are never empty...

	Error_SetPos(line.pos)

	if line.kind != NG_Line {
		panic("weird node for ParseLine")
	}

	fu.CommentLine(line)

	head := line.children[0]

	if head.kind == NG_Block && line.Len() == 1 {
		blk := NewBlockInfo("do")

		if is_last {
			blk.tail = fu.cur_block.tail
		}

		return fu.ParseBlock(head, blk)
	}

	// handle special statements....
	// most of these are void and produce no value.
	switch {
	case head.Match("return"):
		return fu.ParseReturn(line)

	case head.Match("do"):
		return fu.ParseDo(line, is_last)

	case head.Match("let"):
		return fu.ParseLet(line)

	case head.Match("if"):
		return fu.ParseIf(line, is_last)

	case head.Match("match"):
		return fu.ParseMatch(line, is_last)

	case head.Match("assert"):
		return fu.ParseAssert(line)

	case head.Match("loop"):
		return fu.ParseLoop(line)

	case head.Match("jump"):
		return fu.ParseJump(line)

	case head.Match("break"):
		return fu.ParseBreak(line)

	case head.Match("continue"):
		return fu.ParseContinue(line)

	case head.Match("tail-call"):
		return fu.ParseTailCall(line, is_last)

	case head.Match("mem-write"):
		return fu.ParseMemWrite(line)

	case head.Match("swap"):
		return fu.ParseSwap(line)
	}

	// check for an assignment
	if head.Match("=") {
		PostError("missing destination before '='")
		return P_FAIL
	}
	if line.Len() >= 2 {
		if line.children[1].Match("=") {
			return fu.ParseAssignment(line)
		}
	}

	// handle a computation on a line by itself....

	comp := fu.ParseComputation(line.children, "statement", nil)
	if comp == P_FAIL {
		return P_FAIL
	}

	// when calling a `no-return` function, no real need to check types
	if comp.ty != nil {
		if comp.ty.kind == TYP_NoReturn {
			return fu.DropComputation(comp)
		}
	}

	// values in tail position are handled similar to a `return` statement
	if is_last && fu.cur_block.tail {
		ret_type := fu.ty.sub

		if ret_type.kind == TYP_Void || ret_type.kind == TYP_NoReturn {
			// implicit returns make no sense when the current function does not
			// return anything.
		} else {
			return fu.ParseImplicitReturn(comp)
		}
	}

	// function calls are allowed, even though return value is not used
	if comp.kind == NC_Call {
		return fu.DropComputation(comp)
	}

	// for all other computations, an unused value is not allowed

	if comp.ty == nil {
		PostError("unused literal value")
		return P_FAIL
	} else {
		PostError("unused value/computation")
		return P_FAIL
	}
}

func (fu *FuncDef) ParseLet(line *Node) *Node {
	if line.Len() < 2 {
		PostError("missing variable name after let")
		return P_FAIL
	}

	t_var := line.children[1]

	if ValidateName(t_var, "local", ALLOW_SHADOW) != OK {
		return P_FAIL
	}

	// allow using same name in an *outer* block, but not same block
	if fu.cur_block.locals[t_var.str] != nil {
		PostError("local name already used: %s", t_var.str)
		return P_FAIL
	}
	if fu.labels[t_var.str] != nil {
		PostError("local name clashes with a label: %s", t_var.str)
		return P_FAIL
	}

	// inhibit future error messages about the local if the rest
	// of this line fails to parse.
	fu.cur_block.seen[t_var.str] = true

	if line.Len() < 3 || ! line.children[2].Match("=") {
		PostError("missing '=' in let statement")
		return P_FAIL
	}
	if line.Len() < 4 {
		PostError("missing computation in let statement")
		return P_FAIL
	}

	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	source := fu.ParseComputation(line.children[3:], "let var", nil)
	if source == P_FAIL {
		return P_FAIL
	}

	// check the type
	if source.ty == nil {
		PostError("local var '%s' needs an explicit type (literals are untyped)", t_var.str)
		return P_FAIL
	}

	switch source.ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("local var '%s' inferred as %s", t_var.str, source.ty.SimpleName())
		return P_FAIL
	}

	// create local -- must be *after* parsing the value!
	local := fu.NewLocal(t_var.str, source.ty, fu.cur_block)

	dest := NewNode(NP_Local, "", t_var.pos)
	dest.local = local
	dest.ty = local.ty

	return fu.ParseAssignmentCommon(dest, source)
}

func (fu *FuncDef) ParseAssignment(line *Node) *Node {
	if line.Len() < 3 {
		PostError("missing computation in assignment")
		return P_FAIL
	}

	// parse the destination, which is either a local var or an
	// an access expression in `[]` brackets.
	var dest *Node

	t_var := line.children[0]

	switch t_var.kind {
	case NG_Data:
		dest = fu.OperandAccess(t_var, false)
		if dest == P_FAIL {
			return P_FAIL
		}

	case NL_Name:
		if t_var.Match("_") {
			return fu.ParseIgnore(line.children[2:])
		}

		local := fu.cur_block.LookupLocal(t_var.str)

		if local != nil {
			if local.immutable {
				PostError("cannot assign to immutable local %s", t_var.str)
				return P_FAIL
			}

			dest = NewNode(NP_Local, "", t_var.pos)
			dest.local = local
			dest.ty = local.ty
			break
		}

		// handle global vars and class members

		dest = fu.OperandName(t_var, false, true /* auto_access */)
		if dest == P_FAIL {
			return P_FAIL
		}

		if dest.ty == nil {
			PostError("invalid destination, got: %s", t_var.String())
			return P_FAIL
		}

	default:
		PostError("invalid destination, got: %s", t_var.String())
		return P_FAIL
	}

	if dest.ty.IsStructural() {
		PostError("cannot assign to a whole %s", dest.ty.SimpleName())
		return P_FAIL
	}

	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	source := fu.ParseComputation(line.children[2:], "assignment", dest.ty)
	if source == P_FAIL {
		return P_FAIL
	}

	return fu.ParseAssignmentCommon(dest, source)
}

func (fu *FuncDef) ParseAssignmentCommon(dest *Node, source *Node) *Node {
	if dest.ty == nil {
		panic("dest without type")
	}

	t_move := NewNode(NH_Move, "", dest.pos)
	t_move.Add(dest)
	t_move.Add(source)

	fu.body.Add(t_move)

	// an assignment is not a value
	return nil
}

func (fu *FuncDef) ParseIgnore(children []*Node) *Node {
	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	comp := fu.ParseComputation(children, "assignment", nil)
	if comp == P_FAIL {
		return P_FAIL
	}
	if comp.IsLiteral() || comp.kind == NP_CSize {
		// comp is a literal, so nothing needed
		return nil
	}
	if comp.ty == nil {
		panic("assignment value has no type")
	}

	return fu.DropComputation(comp)
}

func (fu *FuncDef) ParseDo(line *Node, is_last bool) *Node {
	if line.Len() < 2 {
		PostError("missing block after do")
		return P_FAIL
	}
	if line.Len() > 3 {
		PostError("too many elements for do block")
		return P_FAIL
	}

	if line.Len() >= 3 {
		t_label := line.children[1]

		if t_label.kind != NL_Name {
			PostError("label must be identifier, got: %s", t_label.String())
			return P_FAIL
		}

		label_name := t_label.str

		// check that labels are unique
		if fu.labels[label_name] != nil {
			PostError("duplicate label: %s", label_name)
			return P_FAIL
		}
		if fu.cur_block.LookupLocal(label_name) != nil {
			PostError("label name clashes with a local: %s", label_name)
			return P_FAIL
		}

		info := new(LabelInfo)
		info.name  = label_name
		info.pos   = t_label.pos
		info.block = fu.cur_block

		fu.labels[label_name] = info
		fu.cur_block.labels[label_name] = true

		fu.EmitLabel(info.name, info.pos)
	}

	t_block := line.children[line.Len()-1]

	if t_block.kind != NG_Block {
		PostError("expected block after do, got: %s", t_block.String())
		return P_FAIL
	}

	blk := NewBlockInfo("do")

	if is_last {
		blk.tail = fu.cur_block.tail
	}

	return fu.ParseBlock(t_block, blk)
}

func (fu *FuncDef) ParseReturn(line *Node) *Node {
	ret_type := fu.ty.sub

	if ret_type.kind == TYP_NoReturn {
		PostError("return used in a no-return function")
		return P_FAIL
	}

	t_ret := NewNode(NH_Return, "", line.pos)

	children := line.children[1:]

	// plain `return` with no value?
	if len(children) == 0 {
		if ret_type.kind != TYP_Void {
			PostError("missing value after return")
			return P_FAIL
		}

		fu.body.Add(t_ret)
		return nil
	}

	if ret_type.kind == TYP_Void {
		PostError("return with value in void function")
		return P_FAIL
	}

	// passing 'ret_type' here will cause type checking
	comp := fu.ParseComputation(children, "return", ret_type)
	if comp == P_FAIL {
		return P_FAIL
	}
	if comp.ty == nil {
		panic("return value should have got a type")
	}

	t_ret.Add(comp)

	fu.body.Add(t_ret)
	return nil
}

func (fu *FuncDef) ParseImplicitReturn(comp *Node) *Node {
	ret_type := fu.ty.sub

	if comp.ty != nil {
		if comp.ty.kind == TYP_Void {
			PostError("control reaches end of non-void function")
			return P_FAIL
		}
	}

	comp = fu.DoTypeStuff(comp, "implicit return", ret_type)
	if comp == P_FAIL {
		return P_FAIL
	}

	t_ret := NewNode(NH_Return, "", comp.pos)
	t_ret.Add(comp)

	fu.body.Add(t_ret)
	return nil
}

func (fu *FuncDef) ParseIf(line *Node, is_last bool) *Node {
	children := line.children[0:]

	// we scan the children backwards, first detecting any `else`
	// clause, and then detecting possibly multiple `else if` clauses.
	// we split them off into their own node (just to keep track).
	// [ NOTE this `clauses` array is in reverse order! ]

	clauses   := make([]*Node, 0)
	have_else := false ; _ = have_else

	for i := len(children)-1; i > 0; i-- {
		t := children[i]

		if t.Match("else") {
			is_elif := false
			if i+1 < len(children)-1 {
				if children[i+1].Match("if") {
					is_elif = true
					children[i] = NewNode(NL_Name, "else if", t.pos)
				}
			}

			if !is_elif {
				if len(clauses) > 0 {
					Error_SetPos(t.pos)
					PostError("else must be last thing in if statement")
					return P_FAIL
				}
				have_else = true
			}

			claw := NewNode(NG_Line, "", t.pos)
			for k := i; k < len(children); k++ {
				if is_elif && k == i+1 {
					// internally we convert "else" + "if" to a single node
				} else {
					claw.Add(children[k])
				}
			}
			clauses = append(clauses, claw)
			children = children[0:i]
		}
	}

	claw := NewNode(NG_Line, "", line.pos)
	for k := 0; k < len(children); k++ {
		claw.Add(children[k])
	}
	clauses = append(clauses, claw)

	// label for exit
	fu.if_labels += 1
	exit_label := "__endif" + strconv.Itoa(fu.if_labels)

	// label for next condition check
	next_label := exit_label
	if len(clauses) > 1 {
		next_label = "__if" + strconv.Itoa(fu.if_labels)
	}

	// process each clause in left-to-right order

	for i := len(clauses)-1; i >= 0; i-- {
		claw := clauses[i]
		head := claw.children[0]

		Error_SetPos(head.pos)

		is_if   := head.Match("if")
		is_else := head.Match("else")
		is_elif := !is_if && !is_else

		if is_else {
			if len(claw.children) < 2 {
				PostError("missing block after %s", head.str)
				return P_FAIL
			}
			if len(claw.children) > 2 {
				PostError("extra rubbish after %s", head.str)
				return P_FAIL
			}
		} else {
			if len(claw.children) < 3 {
				PostError("missing condition after %s", head.str)
				return P_FAIL
			}
		}

		last := claw.Len() - 1

		t_block := claw.children[last]
		if t_block.kind != NG_Block {
			PostError("missing block after %s", head.str)
			return P_FAIL
		}

		if i < len(clauses)-1 {
			fu.EmitLabel(next_label, head.pos)
		}

		if is_elif {
			if i > 0 {
				fu.if_labels += 1
				next_label = "__if" + strconv.Itoa(fu.if_labels)
			} else {
				next_label = exit_label
			}
		}

		// handle the condition...

		if !is_else {
			comp := fu.ParseComputation(claw.children[1:last], "if", bool_type)
			if comp == P_FAIL {
				return P_FAIL
			}

			// TODO: if comp is a NC_OpSeq whose last operation is "bnot",
			//       remove that operation and flip `negate` here

			// WISH: handle TRUE and FALSE literals

			t_jump := NewNode(NH_Jump, next_label, comp.pos)
			t_jump.negate = true
			t_jump.Add(comp)

			fu.body.Add(t_jump)
		}

		// handle the block...

		blk := NewBlockInfo("if")

		if is_last {
			blk.tail = fu.cur_block.tail
		}

		t_value := fu.ParseBlock(t_block, blk)
		if t_value == P_FAIL {
			return P_FAIL
		}

		if i > 0 {
			t_jump := NewNode(NH_Jump, exit_label, head.pos)
			fu.body.Add(t_jump)
		}
	}

	fu.EmitLabel(exit_label, line.pos)
	return nil
}

func (fu *FuncDef) ParseMatch(line *Node, is_last bool) *Node {
	if line.Len() < 2 {
		PostError("missing value for match statement")
		return P_FAIL
	}
	if line.Len() < 3 {
		PostError("missing block for match statement")
		return P_FAIL
	}

	last := line.Len() - 1

	t_block := line.children[last]
	if t_block.kind != NG_Block {
		PostError("match body must be a block, got: %s", t_block.String())
		return P_FAIL
	}

	// label for exit
	fu.if_labels += 1
	exit_label := "__endmatch" + strconv.Itoa(fu.if_labels)

	// grab the value to match
	comp := fu.ParseComputation(line.children[1:last], "match", nil)
	if comp == P_FAIL {
		return P_FAIL
	}

	// check the type
	match_type := comp.ty
	if match_type == nil {
		PostError("cannot match an untyped literal")
		return P_FAIL
	}

	switch match_type.kind {
	case TYP_Int, TYP_Float:
		// ok
	default:
		PostError("cannot match a %s", match_type.SimpleName())
		return P_FAIL
	}

	// ensure it is in a local var
	if comp.kind != NP_Local {
		comp = fu.ShiftToTemporary(comp, "__match")
	}

	// handle each match expression...
	catch_all := false

	for _, rule := range t_block.children {
		Error_SetPos(rule.pos)

		last := rule.Last()

		if last.kind != NG_Block {
			PostError("expected block at end of rule, got: %s", last.String())
			return P_FAIL
		}
		if catch_all {
			PostError("extra rules beyond the else clause")
			return P_FAIL
		}

		var t_match *Node

		head := rule.children[0]

		if head.Match("if") {
			if rule.Len() < 3 {
				PostError("missing values in match rule")
				return P_FAIL
			}

			// collect the values to compare with
			t_match = NewNode(NC_MultiEq, "", rule.pos)

			t_local2 := NewNode(NP_Local, "", comp.pos)
			t_local2.local = comp.local
			t_local2.ty = comp.ty

			t_match.Add(t_local2)

			for i := 1; i < rule.Len()-1; i++ {
				t_val := fu.ParseOperand(rule.children[i], false, false)
				if t_val == P_FAIL {
					return P_FAIL
				}
				if !t_val.IsLiteral() {
					PostError("expected constant in match rule, got: %s", t_val.String())
					return P_FAIL
				}

				if match_type.kind == TYP_Float {
					switch t_val.kind {
					case NL_Integer, NL_Char:
						if IntegerToFloat(t_val) != OK {
							return P_FAIL
						}
					}
				}

				t_val.ty = match_type
				if fu.CheckLiteral(t_val) != OK {
					return P_FAIL
				}

				t_match.Add(t_val)
			}

		} else if head.Match("else") {
			if rule.Len() != 2 {
				PostError("extra rubbish after else keyword")
				return P_FAIL
			}
			catch_all = true

		} else {
			PostError("expected if or else in match body, got: %s", head.String())
			return P_FAIL
		}

		// label for next check
		fu.if_labels += 1
		next_label := "__nextmatch" + strconv.Itoa(fu.if_labels)

		if !catch_all {
			t_jump := NewNode(NH_Jump, next_label, rule.pos)
			t_jump.negate = true
			t_jump.Add(t_match)

			fu.body.Add(t_jump)
		}

		// handle the statement...
		blk := NewBlockInfo("rule")

		if is_last {
			blk.tail = fu.cur_block.tail
		}
		if fu.ParseBlock(last, blk) == P_FAIL {
			return P_FAIL
		}

		t_jump := NewNode(NH_Jump, exit_label, rule.pos)
		fu.body.Add(t_jump)

		if !catch_all {
			fu.EmitLabel(next_label, rule.pos)
		}
	}

	fu.EmitLabel(exit_label, line.pos)
	return nil
}

func (fu *FuncDef) ParseAssert(line *Node) *Node {
	if line.Len() < 2 {
		PostError("missing condition for assert statement")
		return P_FAIL
	}

	// label to jump to
	fu.if_labels += 1
	exit_label := "__assert" + strconv.Itoa(fu.if_labels)

	comp := fu.ParseComputation(line.children[1:], "assert", bool_type)
	if comp == P_FAIL {
		return P_FAIL
	}

	t_jump := NewNode(NH_Jump, exit_label, line.pos)
	t_jump.Add(comp)

	fu.body.Add(t_jump)

	// create the message
	msg := "assertion failure at " + FormatPosition(line.pos) + "\r\n"

	t_abort := NewNode(NH_Abort, msg, line.pos)
	fu.body.Add(t_abort)

	fu.EmitLabel(exit_label, line.pos)
	return nil
}

func (fu *FuncDef) ParseLoop(line *Node) *Node {
	if line.Len() < 2 {
		PostError("missing block for loop statement")
		return P_FAIL
	}

	last := line.Len() - 1

	t_block := line.children[last]
	if t_block.kind != NG_Block {
		PostError("loop body must be a block, got: %s", t_block.String())
		return P_FAIL
	}

	// create a new label
	fu.loop_labels += 1
	lab_name := "__loop" + strconv.Itoa(fu.loop_labels)

	fu.EmitLabel(lab_name, line.pos)

	blk := NewBlockInfo("loop")
	blk.loop_lab = lab_name

	// support a condition after the `loop` keyword
	if last > 1 {
		keyword := line.children[1]
		negate  := false

		if keyword.Match("while") {
			// ok
			negate = true
		} else if keyword.Match("until") {
			// ok
		} else {
			PostError("expected while/until, got: %s", keyword.String())
			return P_FAIL
		}

		children := line.children[2:last]
		if len(children) == 0 {
			PostError("missing expression after %s", keyword.str)
			return P_FAIL
		}

		// create the exit label
		fu.loop_labels += 1
		blk.break_lab = "__break" + strconv.Itoa(fu.loop_labels)

		comp := fu.ParseComputation(children, "loop", bool_type)
		if comp == P_FAIL {
			return P_FAIL
		}

		// TODO: if comp is a NC_OpSeq whose last operation is "bnot",
		//       remove that operation and flip `negate` here

		// WISH: handle TRUE and FALSE literals

		t_jump := NewNode(NH_Jump, blk.break_lab, comp.pos)
		t_jump.negate = negate
		t_jump.Add(comp)

		fu.body.Add(t_jump)
	}

	if fu.ParseBlock(t_block, blk) == P_FAIL {
		return P_FAIL
	}

	t_jump := NewNode(NH_Jump, lab_name, line.pos)
	fu.body.Add(t_jump)

	if blk.break_lab != "" {
		fu.EmitLabel(blk.break_lab, line.pos)
	}

	return nil
}

func (fu *FuncDef) ParseJump(line *Node) *Node {
	if line.Len() < 2 {
		PostError("missing label for jump statement")
		return P_FAIL
	}

	t_label := line.children[1]

	if t_label.kind != NL_Name {
		PostError("expected label name in jump, got: %s", t_label.String())
		return P_FAIL
	}

	// remember this jump for checking later
	jump_info := new(LabelInfo)
	jump_info.name  = t_label.str
	jump_info.pos   = t_label.pos
	jump_info.block = fu.cur_block

	fu.jumps = append(fu.jumps, jump_info)

	t_jump := NewNode(NH_Jump, t_label.str, line.pos)

	if line.Len() > 2 {
		t_cond, negate := fu.ParseJumpCondition(line.children[2:])
		if t_cond == P_FAIL {
			return P_FAIL
		}

		// handle TRUE and FALSE literals
		if t_cond.kind == NL_Bool {
			val := (t_cond.str == "1")
			if negate {
				val = !val
			}

			if val {
				fu.body.Add(t_jump)
			} else {
				// omit the jump completely
			}
			return nil
		}

		t_jump.Add(t_cond)
		t_jump.negate = negate
	}

	fu.body.Add(t_jump)
	return nil
}

func (fu *FuncDef) ParseBreak(line *Node) *Node {
	// find the most recent loop block
	blk := fu.cur_block

	for blk != nil {
		if blk.kind == "loop" {
			break // ok
		}
		blk = blk.parent
	}

	if blk == nil {
		PostError("break used outside of a loop")
		return P_FAIL
	}

	// create the exit label, if not already there
	if blk.break_lab == "" {
		fu.loop_labels += 1
		blk.break_lab = "__break" + strconv.Itoa(fu.loop_labels)
	}

	t_jump := NewNode(NH_Jump, blk.break_lab, line.pos)

	// support a condition after the `break` keyword
	if line.Len() > 1 {
		t_cond, negate := fu.ParseJumpCondition(line.children[1:])
		if t_cond == P_FAIL {
			return P_FAIL
		}

		// WISH: handle TRUE and FALSE literals

		t_jump.negate = negate
		t_jump.Add(t_cond)
	}

	fu.body.Add(t_jump)
	return nil
}

func (fu *FuncDef) ParseContinue(line *Node) *Node {
	// find the most recent loop block
	blk := fu.cur_block

	for blk != nil {
		if blk.kind == "loop" {
			break // ok
		}
		blk = blk.parent
	}

	if blk == nil {
		PostError("continue used outside of a loop")
		return P_FAIL
	}

	t_jump := NewNode(NH_Jump, blk.loop_lab, line.pos)

	// support a condition after the `break` keyword
	if line.Len() > 1 {
		t_cond, negate := fu.ParseJumpCondition(line.children[1:])
		if t_cond == P_FAIL {
			return P_FAIL
		}

		// WISH: handle TRUE and FALSE literals

		t_jump.negate = negate
		t_jump.Add(t_cond)
	}

	fu.body.Add(t_jump)
	return nil
}

func (fu *FuncDef) ParseJumpCondition(children []*Node) (*Node, bool) {
	negate := false

	t_word  := children[0]
	children = children[1:]

	if t_word.Match("if") {
		// ok
	} else if t_word.Match("unless") {
		negate = true
	} else {
		PostError("expected if/unless, got: %s", t_word.String())
		return P_FAIL, false
	}

	comp := fu.ParseComputation(children, "if/jump", bool_type)
	if comp == P_FAIL {
		return P_FAIL, false
	}

	// TODO: if comp is a NC_OpSeq whose last operation is "bnot",
	//       remove that operation and flip `negate` here

	return comp, negate
}

func (fu *FuncDef) ParseMemWrite(line *Node) *Node {
	if line.Len() < 2 || line.children[1].kind != NG_Data {
		PostError("missing access in [] for mem-write statement")
		return P_FAIL
	}

	t_acc := line.children[1]
	if t_acc.kind != NG_Data {
		PostError("need access in [] for mem-write, got: %s", t_acc.String())
		return P_FAIL
	}

	dest := fu.OperandAccess(t_acc, false)
	if dest == P_FAIL {
		return P_FAIL
	}

	op_val := fu.ParseComputation(line.children[2:], "mem-write", dest.ty)
	if op_val == P_FAIL {
		return P_FAIL
	}

	t_write := NewNode(NH_MemWrite, "", line.pos)
	t_write.Add(dest)
	t_write.Add(op_val)

	fu.body.Add(t_write)
	return nil
}

func (fu *FuncDef) ParseSwap(line *Node) *Node {
	if line.Len() < 3 {
		PostError("missing operands for swap statement")
		return P_FAIL
	}
	if line.Len() > 3 {
		PostError("extra rubbish at end of swap statement")
		return P_FAIL
	}

	left  := fu.ParseOperand(line.children[1], false, true)
	right := fu.ParseOperand(line.children[2], false, true)

	if left == P_FAIL || right == P_FAIL {
		return P_FAIL
	}

	if left.IsLiteral() || right.IsLiteral() ||
		left.kind == NP_CSize || right.kind == NP_CSize {
		PostError("cannot use a literal value in a swap statement")
		return P_FAIL
	}

	left_ok  :=  left.IsAssignable()
	right_ok := right.IsAssignable()

	if !left_ok || !right_ok {
		PostError("operands in a swap statement must be assignable")
		return P_FAIL
	}

	if left.kind == NP_Local && right.kind == NP_Local {
		if left.local == right.local {
			PostError("cannot swap a local with itself")
			return P_FAIL
		}
	}

	if !TypeCompare(left.ty, right.ty) {
		PostError("type mismatch in swap: got %s and %s",
			left.ty.String(), right.ty.String())
		return P_FAIL
	}

	t_swap := NewNode(NH_Swap, "", line.pos)
	t_swap.Add(left)
	t_swap.Add(right)

	fu.body.Add(t_swap)
	return nil
}

func (fu *FuncDef) ParseTailCall(line *Node, is_last bool) *Node {
	PostError("tail-call statement not usable with C target")
	return P_FAIL
/*
	if line.Len() < 2 {
		PostError("missing function for tail-call statement")
		return P_FAIL
	}

	var ctx EvalContext
	ctx.fu = fu

	t_call := ctx.Term_ExplicitCall(line.children[1:])
	if t_call == P_FAIL {
		return P_FAIL
	}

	// t_call should always be an NC_Call here

	called_func := fu.GetCalledFunc(t_call)
	if called_func != nil && called_func.inline {
		PostError("cannot tail call an inline function")
		return P_FAIL
	}

	fun_type := t_call.children[0].ty

	// this limitation stems from the Win64 ABI, since tail-calls can
	// only pass parameters via registers (NEVER via the stack).
	if len(fun_type.param) > 4 {
		PostError("too many parameters for a tail call (limit is 4)")
		return P_FAIL
	}

	// if this function (the caller) returns something or is `no-return`,
	// require the called function to return the SAME type.
	if fu.ty.sub.kind != TYP_Void {
		if !TypeCompare(fun_type.sub, fu.ty.sub) {
			PostError("type mismatch on tail-call return: wanted %s, got %s",
				fu.ty.sub.String(), fun_type.sub.String())
			return P_FAIL
		}
	}

	t_call.kind = NH_TailCall

	fu.body.Add(t_call)
	return nil
*/
}

func (fu *FuncDef) ParseComputation(children []*Node, what string, dest_type *Type) *Node {
	if len(children) == 0 {
		PostError("missing value for %s", what)
		return P_FAIL
	}

	var ctx EvalContext
	ctx.fu = fu

	comp := ctx.ProcessExpression(children)
	if comp == P_FAIL {
		return P_FAIL
	}

	if dest_type == nil {
		// anything goes
		return comp
	}

	// assign a type if possible
	// [ caller must check as well! ]

	return fu.DoTypeStuff(comp, what, dest_type)
}

//----------------------------------------------------------------------

func (fu *FuncDef) ParseOperand(t *Node, allow_func bool, auto_access bool) *Node {
	if t.IsLiteral() || t.kind == NP_CSize {
		// note that this node usually has no type (ty = NIL).
		// everything else returned here will have a type.
		return t
	}

	switch t.kind {
	case NL_Name:
		return fu.OperandName(t, allow_func, auto_access)

	case NG_Expr:
		if len(t.children) == 0 {
			PostError("missing computation in ()")
			return P_FAIL
		}
		var ctx EvalContext
		ctx.fu = fu
		return ctx.ProcessExpression(t.children)

	case NG_Data:
		return fu.OperandAccess(t, true)

	case NG_Block:
		Error_SetPos(t.pos)
		PostError("block used in value context")
		return P_FAIL

	case NP_GlobPtr:
		return t

	default:
		PostError("expected operand, got: %s", t.String())
		return P_FAIL
	}
}

func (fu *FuncDef) OperandName(t *Node, allow_func bool, auto_access bool) *Node {
	name := t.str

	if fu.method && t.IsField() {
		return fu.MemberAccess(t)
	}

	local := fu.cur_block.LookupLocal(name)
	if local != nil {
		op := NewNode(NP_Local, "", t.pos)
		op.local = local
		op.ty = local.ty
		return op
	}

	if !ValidateModUsage(t.module) {
		return P_FAIL
	}

	ReplaceAlias(t)
	name = t.str

	def, mod := LookupDef(name, t.module)

	if def != nil && mod != cur_mod && !def.IsPublic() {
		PostError("%s is private to module %s", name, mod.name)
		return P_FAIL
	}
	if def != nil {
		switch def.kind {
		case DEF_Const:
			// note that the result node will have no type
			return def.d_const.MakeLiteral(t.pos)

		case DEF_Var:
			return fu.OperandVar(t, def, auto_access)

		case DEF_Func:
			if !allow_func {
				PostError("invalid use of function name: %s", name)
				return P_FAIL
			}
			return fu.OperandFunc(t, def)

		case DEF_Type:
			PostError("cannot use type '%s' in that context", t.str)
			return P_FAIL
		}
	}

	if operators[t.str] != nil {
		PostError("cannot use operator '%s' in that context", t.str)
		return P_FAIL
	}

	if fu.error_num > 0 && fu.cur_block.SeenLocal(t.str) {
		// don't show errors for a local that was known to be defined
	} else {
		PostError("unknown identifier: %s", t.str)

		// inhibit future errors about this identifier
		fu.cur_block.seen[t.str] = true
	}
	return P_FAIL
}

func (fu *FuncDef) OperandVar(t *Node, def *Definition, auto_access bool) *Node {
	vdef := def.d_var

	t_glob := NewNode(NP_GlobPtr, t.str, t.pos)
	t_glob.ty  = vdef.ptr
	t_glob.def = def

	// need a fully-qualified name here, unless extern
	if vdef.extern {
		t_glob.module = ""
	} else if t.module != "" {
		t_glob.module = t.module
	} else {
		t_glob.module = cur_mod.name
	}

	if auto_access && def.kind == DEF_Var && !def.d_var.ty.IsStructural() {
		t_mem := NewNode(NM_Deref, "", t.pos)
		t_mem.ty = vdef.ty
		t_mem.Add(t_glob)
		return t_mem
	}

	return t_glob
}

func (fu *FuncDef) OperandFunc(t *Node, def *Definition) *Node {
	fdef := def.d_func

	t_func := NewNode(NP_FuncRef, t.str, t.pos)
	t_func.ty  = fdef.ty
	t_func.def = def

	// need a fully-qualified name here, unless extern
	if fdef.extern {
		t_func.module = ""
	} else if t.module != "" {
		t_func.module = t.module
	} else {
		t_func.module = cur_mod.name
	}

	return t_func
}

type AccessState struct {
	// type of the object being accessed.  this will be TYP_Array,
	// TYP_Struct or TYP_Union.
	obj_type *Type

	// the node computing the current address.  the type of this node is
	// a POINTER to the above type.
	node  *Node

	// an offset added to the address in the above node.
	offset  int64

	// true when we only want the final address
	addr_of  bool
}

func (fu *FuncDef) OperandAccess(t *Node, allow_ref bool) *Node {
	// this takes an access in `[]` and produces a NM_XXX node
	// which represents the final piece of memory (to read or write).
	//
	// the `[ref ...]` variant produces a node which computes the
	// final address.

	children := t.children

	addr_of := false
	if len(children) > 0 && children[0].Match("ref") {
		if !allow_ref {
			PostError("ref cannot be used in that context")
			return P_FAIL
		}
		addr_of = true
		children = children[1:]
	}

	if len(children) == 0 {
		PostError("missing elements in []")
		return P_FAIL
	}

	base := fu.ParseOperand(children[0], true /* allow_func */, false /* auto_access */)
	children = children[1:]

	if base == P_FAIL {
		return P_FAIL
	}
	if base.IsLiteral() || base.kind == NP_CSize {
		PostError("cannot access a literal value")
		return P_FAIL
	}
	if base.ty == nil {
		panic("operand in [] without a type")
	}

	// merely a function pointer?
	if base.kind == NP_FuncRef {
		return fu.FunctionAddress(base, children, addr_of)
	}

	// handle the case of no indexors -- deref a pointer
	if len(children) == 0 {
		return fu.AccessDeref(base, addr_of)
	}

	// if global variable is a pointer, deref it now
	if base.kind == NP_GlobPtr && base.def.kind == DEF_Var {
		vdef := base.def.d_var
		if vdef.ty.kind == TYP_Pointer && vdef.ty.sub.IsStructural() {
			t_mem := NewNode(NM_Deref, "", base.pos)
			t_mem.ty = vdef.ty
			t_mem.Add(base)

			base = fu.ShiftAccess(t_mem)
		}
	}

	if base.ty.kind != TYP_Pointer {
		PostError("expected pointer in [], got a %s", base.ty.SimpleName())
		return P_FAIL
	}
	if base.ty.sub == rawmem_type {
		PostError("cannot access a raw-mem pointer")
		return P_FAIL
	}
	if base.ty.sub.kind == TYP_Extern {
		PostError("cannot access an external type in []")
		return P_FAIL
	}

	if base.kind != NP_Local && base.kind != NP_GlobPtr {
		base = fu.ShiftToTemporary(base, "__base")
	}

	// see if we must dereference a pointer to get to the array/struct/etc.
	// when so, it is only allowed at beginning of the `[]` form, not later.

	if base.kind != NP_GlobPtr {
		fu.DoCheckNull(base)
	}

	var acst AccessState

	acst.obj_type = base.ty.sub
	acst.node     = base
	acst.offset   = 0
	acst.addr_of  = addr_of

	// after a deref (if any), everything else in `[]` is merely indexing
	// into a fixed region of memory (adding offsets to a base pointer).

	for _, t_index := range children {
		if fu.AccessIndexor(&acst, t_index) == P_FAIL {
			return P_FAIL
		}
	}

	if addr_of {
		return fu.MemoryRef(acst.node, acst.obj_type, acst.offset)
	} else {
		return acst.node
	}
}

func (fu *FuncDef) AccessIndexor(acst *AccessState, t_index *Node) *Node {
	switch acst.obj_type.kind {
	case TYP_Array:
		return fu.AccessArray(acst, t_index)

	case TYP_Struct, TYP_Union:
		return fu.AccessStruct(acst, t_index)

	case TYP_Pointer:
		PostError("cannot deref a new pointer in []")
		return P_FAIL

	default:
		PostError("cannot index a %s in []", acst.obj_type.SimpleName())
		return P_FAIL
	}
}

func (fu *FuncDef) AccessStruct(acst *AccessState, t_index *Node) *Node {
	if !t_index.IsField() {
		PostError("expected struct/union field in [], got: %s", t_index.String())
		return P_FAIL
	}

	field_idx := acst.obj_type.FindParam(t_index.str)

	if field_idx < 0 {
		PostError("no such field %s in type %s", t_index.str, acst.obj_type.String())
		return P_FAIL
	}

	field := acst.obj_type.param[field_idx]

	t_mem := NewNode(NM_Field, "", t_index.pos)
	t_mem.str = t_index.str
	t_mem.ty = field.ty
	t_mem.Add(acst.node)

	acst.node      = t_mem
	acst.obj_type  = field.ty

	return acst.node  // OK
}

func (fu *FuncDef) AccessArray(acst *AccessState, t_index *Node) *Node {
	arr_type  := acst.obj_type
	elem_type := arr_type.sub

	if t_index.IsField() {
		PostError("expected array index in [], got: %s", t_index.str)
		return P_FAIL
	}

	op_index := fu.ParseOperand(t_index, false, true)
	if op_index == P_FAIL {
		return P_FAIL
	}

	// handle a literal value...
	if op_index.IsLiteral() {
		switch op_index.kind {
		case NL_Integer, NL_Char:
			// parse the index value
			lit_index, err := strconv.ParseInt(op_index.str, 0, 64)

			if err != nil || lit_index < 0 || lit_index > 0x7FFFFFFF {
				PostError("illegal index value: %s", op_index.str)
				return P_FAIL
			}

			// check index is within bounds
			if arr_type.elems > 0 {
				if lit_index >= int64(arr_type.elems) {
					PostError("index is out of bounds (%d > %d)", lit_index, arr_type.elems - 1);
					return P_FAIL
				}
			}

			op_index.ty = usize_type

		default:
			PostError("array index must be integer, got: %s", t_index.String())
			return P_FAIL
		}

	} else {
		if op_index.ty.kind != TYP_Int {
			PostError("array index must be integer, got a %s", op_index.ty.SimpleName())
			return P_FAIL
		}

		// ensure index is in a local var
		if op_index.kind != NP_Local {
			op_index = fu.ShiftToTemporary(op_index, "__index")
		}

		// insert code to check that index is within bounds
		if acst.obj_type.elems > 0 {
			fu.DoCheckBounds(op_index, acst.obj_type.elems)
		}
	}

	node := fu.ShiftComputation(acst.node)
	node  = fu.ShiftAccess(node)

	t_mem := NewNode(NM_Index, "", t_index.pos)
	t_mem.str = t_index.str
	t_mem.ty = elem_type
	t_mem.Add(node)
	t_mem.Add(op_index)

	acst.node      = t_mem
	acst.obj_type  = t_mem.ty

	return acst.node  // OK
}

func (fu *FuncDef) AccessDeref(base *Node, addr_of bool) *Node {
	if addr_of {
		if base.kind == NP_GlobPtr {
			return base
		}
		PostError("invalid use of ref in []")
		return P_FAIL
	}

	// for a global var, do what `auto_access` normally would
	if base.kind == NP_GlobPtr && base.def.kind == DEF_Var {
		t_mem := NewNode(NM_Deref, "", base.pos)
		t_mem.ty = base.def.d_var.ty
		t_mem.Add(base)

		base = t_mem
	}

	if base.ty.kind != TYP_Pointer {
		PostError("expected pointer in [], got a %s", base.ty.SimpleName())
		return P_FAIL
	}
	if base.ty.sub == rawmem_type {
		PostError("cannot access a raw-mem pointer")
		return P_FAIL
	}
	if base.ty.sub.kind == TYP_Extern {
		PostError("cannot access an external type in []")
		return P_FAIL
	}

	mem_type := base.ty.sub

	switch mem_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok

	case TYP_Array:
		PostError("missing array index in []")
		return P_FAIL

	case TYP_Struct, TYP_Union:
		PostError("missing field name in []")
		return P_FAIL

	default:
		PostError("cannot use [] to access a %s", mem_type.SimpleName())
		return P_FAIL
	}

	if base.kind != NP_Local {
		base = fu.ShiftToTemporary(base, "__base")
	}

	// insert code to check for NULL
	fu.DoCheckNull(base)

	t_mem := NewNode(NM_Deref, "", base.pos)
	t_mem.ty = mem_type
	t_mem.Add(base)

	return t_mem
}

func (fu *FuncDef) MemoryRef(base *Node, mem_type *Type, offset int64) *Node {
	ptr_type := NewPointerType(mem_type)

	if offset > 0 {
		base = fu.ShiftComputation(base)

		calc := NewNode(NC_OpSeq, "", base.pos)
		calc.ty = ptr_type
		calc.Add(base)

		add_str := strconv.FormatInt(offset, 10)
		t_add   := NewNode(NL_Integer, add_str, calc.pos)
		t_add.ty = ssize_type

		op := NewNode(NX_Operator, "+", base.pos)
		op.ty = calc.ty
		op.Add(t_add)

		calc.Add(op)
		return calc
	}

	// FIXME this seems too simplistic, may need a "raw-cast" operation

	base.ty = ptr_type
	return base
}

func (fu *FuncDef) MemberAccess(t_elem *Node) *Node {
	// get the self object
	self := fu.params[0]

	struct_type := self.ty.sub
	field_idx   := struct_type.FindParam(t_elem.str)

	if field_idx < 0 {
		PostError("no such field %s in type %s", t_elem.str, struct_type.String())
		return P_FAIL
	}

	field := struct_type.param[field_idx]

	t_local := NewNode(NP_Local, "", t_elem.pos)
	t_local.local = self
	t_local.ty = self.ty

	t_mem := NewNode(NM_Field, "", t_elem.pos)
	t_mem.str = t_elem.str
	t_mem.ty = field.ty
	t_mem.Add(t_local)

	return t_mem
}

func (fu *FuncDef) FunctionAddress(base *Node, children []*Node, addr_of bool) *Node {
	if len(children) > 0 || !addr_of {
		PostError("cannot dereference a function address")
		return P_FAIL
	}

	if base.def.d_func.inline {
		PostError("cannot take the address of an inline function")
		return P_FAIL
	}

	base.kind = NP_GlobPtr
	base.ty   = base.def.d_func.ptr
	return base
}

//----------------------------------------------------------------------

func (fu *FuncDef) DoTypeStuff(t *Node, what string, dest_type *Type) *Node {
	// (a) handle untyped literals, give wanted type if possible
	// (b) handle auto conversion of subtypes
	// (c) handle auto conversion of a global array/struct/union to a pointer
	// (d) for everything else, just check type compatibility

	if t.ty == nil {
		if t.IsLiteral() || t.kind == NP_CSize {
			// ok
		} else {
			panic("non-literal operand with no type")
		}

		if dest_type.kind == TYP_Float {
			switch t.kind {
			case NL_Integer, NL_Char:
				if IntegerToFloat(t) != OK {
					return P_FAIL
				}
			}
		}

		t.ty = dest_type

		if fu.CheckLiteral(t) != OK {
			return P_FAIL
		}

	} else if TypeIsSubtype(t.ty, dest_type) {
		if t.IsLiteral() {
			t.ty = dest_type
		} else {
			t = fu.DoConvert(t, dest_type)
		}

	} else {
		if !TypeCompare(t.ty, dest_type) {
			PostError("type mismatch in %s: wanted %s, got %s",
				what, dest_type.String(), t.ty.String())
			return P_FAIL
		}
	}

	return t
}

func (fu *FuncDef) CheckLiteral(t *Node) error {
	ty := t.ty

	if !(t.IsLiteral() || t.kind == NP_CSize) {
		panic("CheckLiteral with non literal")
	}
	if ty == nil {
		panic("CheckLiteral with no type")
	}

	if ty.kind == TYP_Int && t.kind == NL_Integer {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Int && t.kind == NL_Char {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Int && t.kind == NP_CSize {
		return OK
	}
	if ty.kind == TYP_Float && t.kind == NL_Float {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Float && t.kind == NL_FltSpec {
		return OK
	}
	if ty.kind == TYP_Bool && t.kind == NL_Bool {
		return OK
	}
	if ty.kind == TYP_Pointer && t.kind == NL_Null {
		return OK
	}
	if ty.kind == TYP_Pointer && t.kind == NL_String {
		if ty.sub.kind == TYP_Int && ty.sub.unsigned && ty.sub.bits < 64 {
			return OK
		}
		PostError("cannot use string in that context (type mismatch)")
		return FAILED
	}

	PostError("literal has wrong type, wanted a %s", ty.SimpleName())
	return FAILED
}

func (fu *FuncDef) DoConvert(L *Node, dest_type *Type) *Node {
	if L.kind != NC_OpSeq {
		L = fu.ShiftComputation(L)
		seq := NewNode(NC_OpSeq, "", L.pos)
		seq.ty = dest_type
		seq.Add(L)
		L = seq
	}

	apply := NewNode(NX_Operator, "conv", L.pos)
	apply.ty = dest_type

	L.Add(apply)
	L.ty = apply.ty

	return L
}

func (fu *FuncDef) DoCheckNull(op *Node) {
	if op.kind == NP_Local && op.local.self {
		// not needed, self is checked at method call time
		return
	}
	t_check := NewNode(NH_ChkPtr, "", op.pos)
	t_check.Add(op)

	fu.body.Add(t_check)
}

func (fu *FuncDef) DoCheckBounds(local *Node, elems int) {
	// we don't need to check cases where range of type (as unsigned)
	// is always <= number of elements.
	if local.ty.bits < 64 {
		limit := uint64(1) << local.ty.bits
		if limit <= uint64(elems) {
			return
		}
	}

	t_check := NewNode(NH_ChkIndex, "", local.pos)
	t_check.offset = int64(elems)
	t_check.Add(local)

	fu.body.Add(t_check)
}

func (fu *FuncDef) DropComputation(comp *Node) *Node {
	t_drop := NewNode(NH_Drop, "", comp.pos)
	t_drop.Add(comp)

	fu.body.Add(t_drop)
	return nil
}

func (fu *FuncDef) ShiftComputation(op *Node) *Node {
	switch op.kind {
	case NC_Call:
		return fu.ShiftToTemporary(op, "__call")

	case NC_OpSeq:
		return fu.ShiftToTemporary(op, "__expr")

	case NC_MemRead, NC_MultiEq, NC_StackVar:
		return fu.ShiftToTemporary(op, "__temp")
	}

	return op
}

func (fu *FuncDef) ShiftNeeded(op *Node) {
	switch op.kind {
	case NC_Call:
		fu.ShiftToTemporary(op, "__call")

	case NC_MemRead:
		fu.ShiftToTemporary(op, "__temp")
	}
}

func (fu *FuncDef) ShiftAccess(op *Node) *Node {
	switch op.kind {
	case NM_Deref, NM_Field, NM_Index:
		return fu.ShiftToTemporary(op, "__access")
	}

	return op
}

func (fu *FuncDef) ShiftToTemporary(op *Node, prefix string) *Node {
	if op.ty == nil {
		panic("node with no type")
	}

	// create the temporary local
	temp  := fu.NewTemporary(prefix)
	local := fu.NewLocal(temp, op.ty, fu.cur_block)

	var_dest := NewNode(NP_Local, "", op.pos)
	var_dest.local = local
	var_dest.ty = local.ty

	// insert new assignment before the current line
	t_move := NewNode(NH_Move, "", op.pos)
	t_move.Add(var_dest)
	t_move.Add(op)

	fu.body.Add(t_move)

	op = NewNode(NP_Local, "", op.pos)
	op.local = local
	op.ty = local.ty

	return op
}

func (fu *FuncDef) CommentLine(line *Node) {
	text := fu.LineAsText(line)
	comment := NewNode(NH_Comment, text, line.pos)

	fu.body.Add(comment)
}

func (fu *FuncDef) LineAsText(line *Node) string {
	s := ""

	for i, t := range line.children {
		token := t.str

		switch t.kind {
		case NL_String: token = "\"...\""
		case NL_Null:   token = "NULL"

		case NL_Bool:
			if t.str == "0" {
				token = "FALSE"
			} else if t.str == "1" {
				token = "TRUE"
			} else {
				token = t.str
			}

		case NL_Char:
			ch, _ := strconv.Atoi(t.str)
			if 32 <= ch && ch <= 126 {
				token = fmt.Sprintf("'%c'", rune(ch))
			} else if ch <= 0xFFFF {
				token = fmt.Sprintf("'\\u%04X'", ch)
			} else {
				token = fmt.Sprintf("'\\U%08X'", ch)
			}

		case NG_Expr:   token = "(" + fu.LineAsText(t) + ")"
		case NG_Data:   token = "[" + fu.LineAsText(t) + "]"
		case NG_Block:  token = "{...}"
		}

		if token == "" {
			token = "????"
		}
		if i > 0 {
			s += " "
		}
		s += token
	}

	return s
}

func (fu *FuncDef) NewTemporary(prefix string) string {
	fu.num_temps += 1
	return prefix + strconv.Itoa(fu.num_temps)
}

func (fu *FuncDef) NewLocal(name string, ty *Type, scope *BlockInfo) *LocalInfo {
	local := new(LocalInfo)
	local.name = name
	local.ty   = ty
	local.param_idx = -1

	fu.locals = append(fu.locals, local)

	if scope != nil {
		scope.locals[name] = local
	}
	return local
}

func (fu *FuncDef) EmitLabel(name string, pos Position) {
	info := new(LabelInfo)
	info.name  = name
	info.pos   = pos
	info.block = fu.cur_block

	fu.labels[name] = info

	t_label := NewNode(NH_Label, name, pos)
	fu.body.Add(t_label)
}

func (fu *FuncDef) GetCalledFunc(t *Node) *FuncDef {
	// input node must be a NH_TailCall or NC_Call, or a high level
	// node which has a NC_Call as its computation.  result is NIL
	// when the function is not a global (or a method).

	comp := t.GetComputation()
	if comp != nil {
		t = comp
	}

	switch t.kind {
	case NC_Call, NH_TailCall:
		// ok
	default:
		return nil
	}

	t_func := t.children[0]

	switch t_func.kind {
	case NP_FuncRef:
		def, _ := LookupDef(t_func.str, t_func.module)
		if def == nil {
			panic("failed to re-find: " + t_func.str)
		}
		return def.d_func

	case NP_Method:
		belong_type := t_func.ty.param[0].ty.sub
		return belong_type.FindMethod(t_func.str)
	}

	return nil
}
