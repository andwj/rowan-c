// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "bufio"
import "fmt"
import "strings"
import "strconv"
import "unicode"
import "unicode/utf8"

// Node represents an element of a parsed line or file.
type Node struct {
	kind     NodeKind
	children []*Node
	str      string
	pos      Position
	ty       *Type

	module   string
	offset   int64
	loop     int
	dead     bool
	negate   bool

	def      *Definition
	local    *LocalInfo
}

type NodeKind int

const (
	/* low level nodes */

	NL_ERROR NodeKind = iota    // str is the message
	NL_EOF
	NL_EOLN

	NL_Integer  // an int:   [-]DDDDDDD          or [-]0xHHHHHH
	NL_Float    // a float:  [-]DDD.DDD[e[+-]DD] or [-]0xHH.HHHp[+-]DDD
	NL_FltSpec  // a special floating-point constant e.g. "+INF"
	NL_Char     // a char literal in '', encoded as integer: DDDDD
	NL_Bool     // a boolean literal, encoded as integer 0 or 1
	NL_String   // a string in "", encoded as UTF-8
	NL_Null     // the 'NULL' pointer constant
	NL_Zeroes   // used for elided arrays and structs in data

	NL_Name     // an identifier
	NL_Symbol   // a special symbol like `:` or `[`

	/* grouping nodes */

	NG_Line     // a parsed line of tokens
	NG_Expr     // a group of elements in `()` parentheses
	NG_Data     // a group of elements in `[]` square brackets
	NG_Block    // a group of elements in `{}` curly brackets

	/* data nodes */

	ND_Union   // a union field value.  str is field name

	/* high level code nodes */

	NH_Label    // identifier at beginning of line followed by `:`
	NH_Comment  // a comment for the generated assembly

	NH_Drop     // child is computation.  discards the result.
	NH_Move     // dest | computation.  dest is NP_Local or NM_XXX.
	NH_MemWrite // dest | computation.  dest is always an NM_XXX operand.
	NH_Swap     // op1  | op2.  operands always NP_Local or NN_XXX.

	NH_Jump     // child is condition (optional).  str is label.
	NH_Return   // child is computation (optional).
	NH_TailCall // func | parameter operands...

	NH_Abort    // str is the message
	NH_ChkPtr   // child is operand (a pointer)
	NH_ChkIndex // child is operand (the index), offset is array size

	/* computations */

	NC_Call     // func | parameter operands...
	NC_OpSeq    // operand | operations... (at least one)
	NC_MemRead  // operand (always an NM_XXX).
	NC_MultiEq  // match operand | value operands...
	NC_StackVar // ty is a *pointer* to specified type.

	/* operands */

	NP_Local    // `local` field is the LocalInfo struct
	NP_GlobPtr  // str is global   name.  produces ptr to var/func.
	NP_FuncRef  // str is function name.  only usable in func calls.
	NP_Method   // str is method name.  ty is method signature.
	NP_CSize    // C back-end: becomes `sizeof(T)`, def is type def.

	/* memory access */

	NM_Deref    // base ptr (to a basic type)
	NM_Field    // base ptr (to a struct or union).  str is field.
	NM_Index    // base ptr (to array) | index operand.

	/* misc */

	NX_Operator  // child is operand (for binary ops).  str is name.
)

type Position struct {
	line int  // 0 if unknown
	file int  // 0 if unknown, index into all_filenames (+ 1)
}

type Lexer struct {
	reader *bufio.Reader
	pos    Position

	tokens  *Node   // tokens for current line, NIL if none yet
	pending *Node   // a pending error from SkipAhead

	hit_eof  bool   // we have reached the end-of-file
	comment  int    // inside a multi-line comment (2 or 1)
	mlc_line int    // line at beginning of a multi-line comment
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(r io.Reader, file int) *Lexer {
	lexer := new(Lexer)

	lexer.reader   = bufio.NewReader(r)
	lexer.pos.file = file
	lexer.hit_eof  = false
	lexer.comment  = 0

	return lexer
}

// Scan reads and parses the current file and returns the next high-level
// token, usually a NG_Line.  It returns a NL_ERROR if there was a parsing
// error, or NL_EOF when the end of the file is reached.
func (lex *Lexer) Scan() *Node {
	if lex.pending != nil {
		res := lex.pending
		lex.pending = nil
		return res
	}
	if lex.hit_eof  {
		return NewNode(NL_EOF, "", lex.pos)
	}

	return lex.scanGroup(NG_Line, "", "", false)
}

// SkipAhead may be called after an error is returned from Scan(), and
// will reposition the token stream at the next top-level directive.
// If any error occurs, it will be the returned at next call to Scan().
func (lex *Lexer) SkipAhead() {
	lex.tokens  = nil
	lex.pending = nil

	for {
		t := lex.nextToken()

		if t.kind == NL_ERROR {
			lex.pending = t
			return
		}
		if t.kind == NL_EOF {
			return
		}

		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			if err_tok.kind == NL_ERROR {
				lex.pending = err_tok
			}
			return
		}

		tok_list := lex.scanLine(s)
		if tok_list.kind == NL_ERROR {
			lex.pending = tok_list
			return
		}

		if tok_list.Len() > 0 {
			head := tok_list.children[0]

			if head.IsCommonDirective() {
				// ensure nextToken will read this line
				lex.tokens = tok_list
				return
			}
		}
	}
}

func (lex *Lexer) scanGroup(kind NodeKind, opener, closer string, in_block bool) *Node {
	group := NewNode(kind, "", lex.pos)

	for {
		tok := lex.nextToken()

		// handle an opening parenthesis or bracket
		switch {
		case tok.Match("("): tok = lex.scanGroup(NG_Expr, "(", ")", false)
		case tok.Match("["): tok = lex.scanGroup(NG_Data, "[", "]", false)
		case tok.Match("{"): tok = lex.scanBlock()
		}

		if tok.kind == NL_ERROR {
			return tok
		}

		if tok.kind == NL_EOF {
			if opener != "" {
				// use the starting line for this error message
				msg := "unterminated " + opener + closer + " group"
				return NewNode(NL_ERROR, msg, group.pos)
			}
			return tok
		}

		if tok.kind == NL_EOLN {
			// ignore it inside () and []
			if kind != NG_Line {
				continue
			}

			// skip empty lines
			if group.Len() == 0 {
				continue
			}

			// handle '\' symbol to extend lines
			last := group.children[group.Len() - 1]
			if last.Match("\\") {
				group.PopTail()
				continue
			}

			return group
		}

		// found terminator of a group?
		if closer != "" {
			if tok.Match(closer) {
				return group
			}
		}
		if in_block && tok.Match("}") {
			lex.pending = group
			return tok
		}

		// detect a stray closing parenthesis or bracket
		if tok.Match(")") || tok.Match("]") || tok.Match("}") {
			msg := "stray " + tok.str + " found"
			return NewNode(NL_ERROR, msg, group.pos)
		}

		// a colon splits a physical line into two logical lines
		if tok.Match(":") {
			if kind != NG_Line || group.Len() == 0 {
				return NewNode(NL_ERROR, "cannot use ':' in that context", group.pos)
			}
			return group
		}

		// check for misuse of the '\' symbol
		if kind == NG_Line {
			if group.Len() > 0 {
				last := group.children[group.Len() - 1]
				if last.Match("\\") {
					return NewNode(NL_ERROR, "can only use \\ at end of a line", last.pos)
				}
			}
		} else {
			if tok.Match("\\") {
				return NewNode(NL_ERROR, "cannot use \\ in that context", tok.pos)
			}
		}

		// ensure non-empty lines begin at first token
		if kind == NG_Line {
			if group.Len() == 0 {
				group.pos = tok.pos
			}
		}

		group.Add(tok)
	}
}

func (lex *Lexer) scanBlock() *Node {
	group := NewNode(NG_Block, "", lex.pos)

	for {
		line := lex.scanGroup(NG_Line, "", "", true /* in_block */)

		if line.kind == NL_EOF {
			// use the starting line for this error message
			return NewNode(NL_ERROR, "unterminated {} block", group.pos)
		}
		if line.kind == NL_ERROR {
			return line
		}

		is_end := false

		if line.Match("}") {
			is_end = true
			line   = lex.pending
			lex.pending = nil
		}

		// do not add empty lines to a block
		if line.Len() > 0 {
			group.Add(line)
		}

		if is_end {
			return group
		}
	}
}


// nextToken parses the next low-level token from the file.
// Result can be NL_ERROR for a parsing problem, or NL_EOF at the
// end of the file, or NL_EOLN for the end of a line.
func (lex *Lexer) nextToken() *Node {
	for {
		if lex.tokens != nil {
			if lex.tokens.Len() > 0 {
				return lex.tokens.PopHead()
			}
			lex.tokens = nil
		}

		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			if err_tok.kind == NL_EOF {
				if lex.comment > 0 {
					lex.comment = 0
					pos := lex.pos
					pos.line = lex.mlc_line
					return NewNode(NL_ERROR, "unterminated comments", pos)
				}
			}
			return err_tok
		}

		tok_list := lex.scanLine(s)
		if tok_list.kind == NL_ERROR {
			return tok_list
		}
		lex.tokens = tok_list
	}
}

func (lex *Lexer) fetchRawLine() (string, *Node) {
	// once stream is finished, keep returning EOF
	// [ this simplifies some logic in the calling code ]
	if lex.hit_eof  {
		return "", NewNode(NL_EOF, "", lex.pos)
	}

	lex.pos.line += 1

	// NOTE: this can return some data + io.EOF
	s, err := lex.reader.ReadString('\n')

	if err == io.EOF {
		lex.hit_eof = true
	} else if err != nil {
		lex.hit_eof = true
		return "", NewNode(NL_ERROR, err.Error(), lex.pos)
	}

	// strip CR and LF from the end
	if len(s) > 0 && s[len(s)-1] == '\n' {
		s = s[0:len(s)-1]
	}
	if len(s) > 0 && s[len(s)-1] == '\r' {
		s = s[0:len(s)-1]
	}

	return s, nil
}

//----------------------------------------------------------------------

// scanLine parses a single a line and produces a sequence of low-level
// tokens, returning them in a NG_Line token.  If there was a problem,
// like an unterminated string, then an NL_ERROR token is returned.
// Multi-line comments are handled here, via some state in the Lexer
// struct, but parentheses and brackets are not.
func (lex *Lexer) scanLine(s string) *Node {
	group := NewNode(NG_Line, "", lex.pos)

	// convert to runes
	r := []rune(s)

	// check for UTF-8 problems
	for _, ch := range r {
		if ch == utf8.RuneError {
			return NewNode(NL_ERROR, "bad UTF-8 found", group.pos)
		}
	}

	// skip initial whitespace
	for len(r) > 0 {
		ch := r[0]
		if unicode.Is(unicode.White_Space, ch) || unicode.IsControl(ch) {
			r = r[1:]
		} else {
			break
		}
	}

	// handle multi-line comments
	if len(r) >= 3 {
		if r[0] == '#' && r[1] == '[' && r[2] == '[' {
			if lex.comment > 0 {
				return NewNode(NL_ERROR, "cannot nest #[[ comments", group.pos)
			}
			// remember start line
			lex.comment  = 2
			lex.mlc_line = group.pos.line
			return group
		}
		if r[0] == '#' && r[1] == ']' && r[2] == ']' {
			if lex.comment != 2 {
				return NewNode(NL_ERROR, "stray #]] found", group.pos)
			}
			lex.comment = 0
			return group
		}
	}
	if lex.comment == 2 {
		return group
	}

	if len(r) >= 2 {
		if r[0] == '#' && r[1] == '[' {
			if lex.comment == 1 {
				return NewNode(NL_ERROR, "cannot nest #[ comments", group.pos)
			}
			// remember start line
			lex.comment  = 1
			lex.mlc_line = group.pos.line
			return group
		}
		if r[0] == '#' && r[1] == ']' {
			if lex.comment != 1 {
				return NewNode(NL_ERROR, "stray #] found", group.pos)
			}
			lex.comment = 0
			return group
		}
	}
	if lex.comment == 1 {
		return group
	}

	// handle normal tokens
	for len(r) > 0 {
		ch := r[0]

		// whitespace ?
		if unicode.Is(unicode.White_Space, ch) || unicode.IsControl(ch) {
			r = r[1:]
			continue
		}

		// line comment ?
		if ch == ';' {
			break
		}

		// string ?
		if r[0] == '"' {
			size, tok := lex.scanString(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		// character literal ?
		if r[0] == '\'' {
			size, tok := lex.scanCharacter(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		// number ?
		if unicode.IsDigit(r[0]) ||
			(len(r) >= 2 && (r[0] == '-' || r[0] == '+') && unicode.IsDigit(r[1])) {

			size, tok := lex.scanNumber(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		// anything else MUST be a name or symbol
		size, tok := lex.scanIdentifier(r)
		tok.pos = lex.pos
		if tok.kind == NL_ERROR {
			return tok
		}
		r = r[size:]
		group.Add(tok)
	}

	group.Add(NewNode(NL_EOLN, "", lex.pos))

	return group
}

// returns # of runes consumed, or negative on error
func (lex *Lexer) scanIdentifier(r []rune) (int, *Node) {
	pos := 0

	// handle '::' symbol
	if len(r) >= 2 && r[0] == ':' && r[1] == ':' {
		return 2, NewNode(NL_Symbol, string(r[0:2]), lex.pos)
	}

	// handle non-ident symbols
	if strings.ContainsRune("()[]{}:,^", r[0]) {
		return 1, NewNode(NL_Symbol, string(r[0:1]), lex.pos)
	}

	for pos < len(r) {
		if ! LEX_IdentifierChar(r[pos]) {
			break
		}
		pos += 1
	}

	// tell caller we could not parse an identifier
	if pos == 0 {
		msg := fmt.Sprintf("stray U+%04X in code", int(r[0]))
		return -1, NewNode(NL_ERROR, msg, lex.pos)
	}

	s := string(r[0:pos])

	// a special constant?
	switch s {
	case "NULL":
		nd := NewNode(NL_Null, "", lex.pos)
		nd.ty = NewPointerType(rawmem_type)
		return pos, nd

	case "FALSE":
		nd := NewNode(NL_Bool, "0", lex.pos)
		nd.ty = bool_type
		return pos, nd

	case "TRUE":
		nd := NewNode(NL_Bool, "1", lex.pos)
		nd.ty = bool_type
		return pos, nd

	case "+INF", "-INF", "SNAN", "QNAN":
		return pos, NewNode(NL_FltSpec, s, lex.pos)

	case "CPU_BITS":
		val := "32"
		if Options.arch_bits > 32 { val = "64" }
		return pos, NewNode(NL_Integer, val, lex.pos)

	case "PTR_SIZE":
		val := "4"
		if Options.arch_bits > 32 { val = "8" }
		return pos, NewNode(NL_Integer, val, lex.pos)

	case "CPU_ENDIAN":
		val := "0"
		if Options.big_endian { val = "1" }
		return pos, NewNode(NL_Integer, val, lex.pos)
	}

	nd := NewNode(NL_Name, s, lex.pos)

	// decompose a module prefix
	div := strings.IndexRune(s, '/')
	if div > 0 && div+1 < len(s) {
		nd.module = s[0:div]
		nd.str    = s[div+1:]
	}

	return pos, nd
}

func (lex *Lexer) scanString(r []rune) (int, *Node) {
	// skip leading quote
	pos := 1

	s := ""

	for {
		if pos >= len(r) {
			return -1, NewNode(NL_ERROR, "unterminated string", lex.pos)
		}

		if r[pos] == '"' {
			break
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) {
			pos += 1

			esc, step := lex.scanEscape(r[pos:])
			if step == -2 {
				return -1, NewNode(NL_ERROR, "unknown escape in string", lex.pos)
			} else if step < 0 {
				return -1, NewNode(NL_ERROR, "malformed escape in string", lex.pos)
			}

			s = s + esc
			pos += step
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}

	return pos + 1, NewNode(NL_String, s, lex.pos)
}

func (lex *Lexer) scanCharacter(r []rune) (int, *Node) {
	// skip leading quote
	pos := 1

	if len(r) < 3 || r[1] == '\'' {
		return -1, NewNode(NL_ERROR, "bad character literal", lex.pos)
	}

	if r[2] == '\'' && r[1] != '\\' {
		int_val := strconv.Itoa(int(r[1]))
		return 3, NewNode(NL_Char, int_val, lex.pos)
	}

	// handle escapes
	if r[pos] != '\\' {
		return -1, NewNode(NL_ERROR, "bad character literal", lex.pos)
	}

	pos += 1
	r = r[pos:]

	esc, step := lex.scanEscape(r)

	// need to convert decoded escape to a single rune
	var rch []rune
	if step >= 0 {
		rch = []rune(esc)
	}

	if step == -2 {
		return -1, NewNode(NL_ERROR, "unknown escape in char literal", lex.pos)
	} else if step < 0 {
		return -1, NewNode(NL_ERROR, "malformed escape in char literal", lex.pos)
	} else if !utf8.ValidString(esc) || len(rch) != 1 {
		return -1, NewNode(NL_ERROR, "char literal is not a valid unicode char", lex.pos)
	}

	r = r[step:]

	if len(r) < 1 || r[0] != '\'' {
		return -1, NewNode(NL_ERROR, "unterminated char literal", lex.pos)
	}

	step += pos + 1

	int_val := strconv.Itoa(int(rch[0]))

	return step, NewNode(NL_Char, int_val, lex.pos)
}

func (lex *Lexer) scanEscape(r []rune) (string, int) {
	switch r[0] {
	case '"', '`', '\'', '\\', '?':
		return string(r[0]), 1
	case 'a':
		return "\u0007", 1   // bell
	case 'b':
		return "\u0008", 1   // backspace
	case 't':
		return "\u0009", 1   // horizontal tab
	case 'n':
		return "\u000A", 1  // linefeed
	case 'v':
		return "\u000B", 1  // vertical tab
	case 'f':
		return "\u000C", 1  // formfeed
	case 'r':
		return "\u000D", 1  // carriage return
	case 'e':
		return "\u001B", 1  // escape
	}

	// octal?
	// (requires one to three octal digits, as per C99 and NASM)
	if '0' <= r[0] && r[0] <= '7' {
		return lex.scanOctalEscape(r)
	}

	// hexadecimal?
	// (requires one or two hexadecimal digits, as per C99 and NASM)
	if r[0] == 'x' {
		return lex.scanHexEscape(r)
	}

	// unicode?
	// (these follow conventions of C11, Go and NASM)
	if r[0] == 'u' {
		return lex.scanUnicodeEscape(r, 4)
	}
	if r[0] == 'U' {
		return lex.scanUnicodeEscape(r, 8)
	}

	return "", -2
}

func (lex *Lexer) scanOctalEscape(r []rune) (string, int) {
	value := int(r[0] - '0')
	size  := 1

	if len(r) >= 2 && '0' <= r[1] && r[1] <= '7' {
		value = (value << 3) + int(r[1] - '0')
		size += 1
	}
	if len(r) >= 3 && '0' <= r[2] && r[2] <= '7' {
		value = (value << 3) + int(r[2] - '0')
		size += 1
	}

	if value > 255 {
		return "", -1
	}

	// convert to a string.
	// [ it may be invalid UTF-8 -- that is checked later ]
	var s [1]byte
	s[0] = byte(value)
	return string(s[:]), size
}

func (lex *Lexer) scanHexEscape(r []rune) (string, int) {
	value := 0
	size  := 1  // the 'x'

	for size < 3 && size < len(r) {
		ch := unicode.ToUpper(r[size])
		if '0' <= ch && ch <= '9' {
			value = value << 4
			value |= int(ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			value = value << 4
			value |= 10 + int(ch - 'A')
		} else {
			break
		}
		size += 1
	}

	if size < 2 {
		return "", -1  // no digits!
	}

	// convert to a string.
	// [ it may be invalid UTF-8 -- that is checked later ]
	var s [1]byte
	s[0] = byte(value)
	return string(s[:]), size
}

func (lex *Lexer) scanUnicodeEscape(r []rune, size int) (string, int) {
	// we assume here the first character is 'u' or 'U'

	if len(r) < size+1 {
		return "", -1
	}

	var value rune = 0

	for i := 1; i <= size; i++ {
		value = value << 4
		ch := unicode.ToUpper(r[i])
		if '0' <= ch && ch <= '9' {
			value |= (ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			value |= 10 + (ch - 'A')
		} else {
			return "", -1
		}
	}

	if value > 0x10FFFF {
		return "", -1
	}

	// convert to a string
	return string(value), size+1
}

// returns a token and # of runes consumed
func (lex *Lexer) scanNumber(r []rune) (int, *Node) {
	if len(r) >= 2 && r[0] == '0' && r[1] == 'b' {
		return lex.scanBinaryNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'b' {
		return lex.scanBinaryNumber(r, 3)
	}

	if len(r) >= 2 && r[0] == '0' && r[1] == 'x' {
		return lex.scanHexNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'x' {
		return lex.scanHexNumber(r, 3)
	}

	return lex.scanDecimalNumber(r, 0)
}

func (lex *Lexer) scanBinaryNumber(r []rune, pos int) (int, *Node) {
	is_neg := (r[0] == '-')

	s := ""

	for pos < len(r) {
		ch := r[pos]
		if ch == '0' || ch == '1' {
			s += string(ch)
			pos += 1
			continue
		}
		if unicode.IsDigit(ch) || unicode.IsLetter(ch) ||
			ch == '-' || ch == '+' || ch == '.' {

			msg := "illegal binary digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}
		// all other symbols terminate the number
		break
	}

	if s == "" {
		return -1, NewNode(NL_ERROR, "bad binary number (no digits)", lex.pos)
	}
	val, err := strconv.ParseUint(s, 2, 64)
	if err != nil {
		msg := fmt.Sprintf("bad binary number (too large)")
		return -1, NewNode(NL_ERROR, msg, lex.pos)
	}

	// convert to hexadecimal
	hex := "0x" + strconv.FormatUint(val, 16)
	if is_neg {
		hex = "-" + hex
	}
	return pos, NewNode(NL_Integer, hex, lex.pos)
}

func (lex *Lexer) scanDecimalNumber(r []rune, pos int) (int, *Node) {
	is_neg := (r[0] == '-')

	if (r[0] == '-' || r[0] == '+') {
		pos = 1
	}

	has_digit := false
	has_dot   := false
	has_E     := false
	has_exp   := false

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if unicode.IsDigit(ch) {
			if has_E {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			pos += 1
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_E {
				return -1, NewNode(NL_ERROR, "bad float (misplaced '.')", lex.pos)
			}
			has_dot = true
			s += string(ch)
			pos += 1
			continue
		}

		if ch == 'e' || ch == 'E' {
			if !has_digit || has_E {
				return -1, NewNode(NL_ERROR, "bad float (misplaced 'e')", lex.pos)
			}
			has_E = true
			s += string(ch)
			pos += 1

			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				s += string(r[pos])
				pos += 1
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			msg := "illegal decimal digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return -1, NewNode(NL_ERROR, "bad number (no digits)", lex.pos)
	}
	if has_E && !has_exp {
		return -1, NewNode(NL_ERROR, "bad float (missing exponent)", lex.pos)
	}

	if is_neg {
		s = "-" + s
	}

	kind := NL_Integer
	if has_dot || has_exp {
		kind = NL_Float
	}
	return pos, NewNode(kind, s, lex.pos)
}

func (lex *Lexer) scanHexNumber(r []rune, pos int) (int, *Node) {
	is_neg := (r[0] == '-')

	has_digit := false
	has_dot   := false
	has_P     := false
	has_exp   := false

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if has_P && unicode.IsLetter(ch) {
			msg := "illegal hex exponent digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}

		if unicode.Is(unicode.Hex_Digit, ch) {
			if has_P {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			pos += 1
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_P {
				return -1, NewNode(NL_ERROR, "bad hex float (misplaced '.')", lex.pos)
			}
			has_dot = true
			s += string(ch)
			pos += 1
			continue
		}

		if ch == 'p' || ch == 'P' {
			if !has_digit || has_P {
				return -1, NewNode(NL_ERROR, "bad hex float (misplaced 'p')", lex.pos)
			}
			has_P = true
			s += string(ch)
			pos += 1

			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				s += string(r[pos])
				pos += 1
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			msg := "illegal hex digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return -1, NewNode(NL_ERROR, "bad hex number (no digits)", lex.pos)
	}
	if (has_dot && !has_P) || (has_P && !has_exp) {
		return -1, NewNode(NL_ERROR, "bad hex float (missing exponent)", lex.pos)
	}

	s = "0x" + s
	if is_neg {
		s = "-" + s
	}

	kind := NL_Integer
	if has_dot || has_exp {
		kind = NL_Float
	}
	return pos, NewNode(kind, s, lex.pos)
}

func LEX_IdentifierChar(ch rune) bool {
	const ID_CHARS = "@_.-+*/%<=>!?#$&|'~\\"

	return false ||
		unicode.IsLetter(ch) ||
		unicode.IsDigit(ch) ||
		strings.ContainsRune(ID_CHARS, ch)
}

//----------------------------------------------------------------------

func NewNode(kind NodeKind, str string, pos Position) *Node {
	t := new(Node)
	t.kind = kind
	t.str  = str
	t.pos  = pos
	t.children = make([]*Node, 0)
	return t
}

func (t *Node) Len() int {
	return len(t.children)
}

func (t *Node) Last() *Node {
	if len(t.children) == 0 {
		return nil
	}
	return t.children[len(t.children)-1]
}

func (t *Node) Add(child *Node) {
	if child == nil {
		panic("Add with nil node")
	}
	t.children = append(t.children, child)
}

func (t *Node) AddFront(child *Node) {
	// resize the array
	t.children = append(t.children, nil)

	// shift elements up
	copy(t.children[1:], t.children[0:])

	t.children[0] = child
}

func (t *Node) PopHead() *Node {
	if t.Len() == 0 {
		panic("PopHead with no tokens")
	}
	head := t.children[0]
	t.children = t.children[1:]
	return head
}

func (t *Node) PopTail() *Node {
	if t.Len() == 0 {
		panic("PopTail with no tokens")
	}
	num  := len(t.children)
	tail := t.children[num-1]
	t.children = t.children[0:num-1]
	return tail
}

func (t *Node) Remove(idx int) {
	if idx >= t.Len() {
		panic("Remove with bad index")
	}
	for k := idx+1; k < t.Len(); k++ {
		t.children[k-1] = t.children[k]
	}
	t.children = t.children[0:t.Len()-1]
}

func (t *Node) Match(name string) bool {
	if t.module != "" {
		return false
	}
	if t.kind == NL_Name || t.kind == NL_Symbol {
		if t.Len() == 0 {
			return t.str == name
		}
	}
	return false
}

func (t *Node) Find(name string) int {
	for i, elem := range t.children {
		if elem.Match(name) {
			return i
		}
	}
	return -1
}

func (t *Node) IsField() bool {
	if t.module != "" {
		return false
	}
	if t.Match("...") || t.Match("..") {
		return false
	}
	return t.kind == NL_Name && len(t.str) >= 2 && t.str[0] == '.'
}

func (t *Node) IsMethod() bool {
	if t.module != "" {
		return false
	}
	if t.kind == NL_Name {
		r := []rune(t.str)

		if r[0] == '.' {
			return false
		}

		// require last char (only) to be a hash
		if len(r) >= 2 && r[len(r)-1] == '#' {
			for i := len(r)-2; i >= 0; i-- {
				if r[i] == '#' { return false }
			}
			return true
		}
	}
	return false
}

func (t *Node) IsLiteral() bool {
	switch t.kind {
	case NL_Integer, NL_Float, NL_FltSpec, NL_String, NL_Char, NL_Bool, NL_Null:
		return true
	}
	return false
}

func (t *Node) IsCommonDirective() bool {
	if t.module != "" {
		return false
	}
	if t.kind == NL_Name {
		switch t.str {
		case "const", "alias",
			"type", "extern-type",
			"fun", "extern-fun", "inline-fun",
			"var", "extern-var", "zero-var", "rom-var",
			"#public", "#private":

			return true
		}
	}
	return false
}

func (t *Node) IsAssignable() bool {
	switch t.kind {
	case NP_Local, NM_Deref, NM_Field, NM_Index:
		return true
	}
	return false
}

func (t *Node) GetComputation() *Node {
	switch t.kind {
	case NH_Drop:
		return t.children[0]

	case NH_Move:
		return t.children[1]

	case NH_Jump, NH_Return:
		if t.Len() > 0 {
			return t.children[0]
		}
	}

	return nil
}

func (t *Node) ReplaceComputation(comp *Node) {
	switch t.kind {
	case NH_Drop, NH_Jump, NH_Return:
		t.children[0] = comp

	case NH_Move:
		t.children[1] = comp

	default:
		panic("ReplaceComputation in odd node")
	}
}

// String returns something usable in error messages,
// especially ones of the form "expected xxx, got: yyy".
func (t *Node) String() string {
	switch t.kind {
	case NL_Integer, NL_Float, NL_FltSpec, NL_Name, NL_Symbol:
		return t.str

	case NL_Char:   return "a char literal"
	case NL_Bool:   return "a boolean literal"
	case NL_String: return "a string literal"
	case NL_Null:   return "NULL"

	case NG_Line:   return "a raw line"
	case NG_Expr:   return "stuff in ()"
	case NG_Data:   return "stuff in []"
	case NG_Block:  return "stuff in {}"

	case NH_Label:  return "a label"
	default:        return "something odd"
	}
}

func (t *Node) FullString() string {
	if t == nil {
		return "nil"
	}

	s := t.str
	if len(s) > 50 {
		s = s[0:50] + "..."
	}
	s = fmt.Sprintf("%q", s)

	len_str := "(" + strconv.Itoa(t.Len()) + " elem)"

	neg_str := ""
	if t.negate {
		neg_str = "NOT "
	}

	switch t.kind {
	/* low level nodes */

	case NL_ERROR:   return "ERROR " + s
	case NL_EOF:     return "EOF"
	case NL_EOLN:    return "EOLN"

	case NL_Integer: return "NL_Integer " + s
	case NL_Float:   return "NL_Float " + s
	case NL_FltSpec: return "NL_FltSpec " + s

	case NL_Char:    return "NL_Char " + s
	case NL_Bool:    return "NL_Bool " + s
	case NL_String:  return "NL_String " + s
	case NL_Null:    return "NL_Null"
	case NL_Zeroes:  return "NL_Zeroes"

	case NL_Name:    return "NL_Name " + s
	case NL_Symbol:  return "NL_Symbol " + s

	/* grouping nodes */

	case NG_Line:    return "NG_Line " + len_str
	case NG_Expr:    return "NG_Expr " + len_str
	case NG_Data:    return "NG_Data " + len_str
	case NG_Block:   return "NG_Block " + len_str

	/* data nodes */

	case ND_Union:   return "ND_Union " + s

	/* high level nodes */

	case NH_Label:    return "NH_Label " + s
	case NH_Comment:  return "NH_Comment"

	case NH_Drop:     return "NH_Drop"
	case NH_Move:     return "NH_Move"
	case NH_MemWrite: return "NH_MemWrite"
	case NH_Swap:     return "NH_Swap"

	case NH_Jump:     return "NH_Jump " + neg_str + s
	case NH_Return:   return "NH_Return"
	case NH_TailCall: return "NH_TailCall"

	case NH_Abort:    return "NH_Abort " + s
	case NH_ChkPtr:   return "NH_ChkPtr "
	case NH_ChkIndex: return "NH_ChkIndex "

	/* computations */

	case NC_Call:     return "NC_Call"
	case NC_OpSeq:    return "NC_OpSeq"
	case NC_MemRead:  return "NC_MemRead"
	case NC_MultiEq:  return "NC_MultiEq " + neg_str
	case NC_StackVar: return "NC_StackVar"

	/* operands */

	case NP_Local:    return "NP_Local " + t.local.name
	case NP_GlobPtr:  return "NP_GlobPtr " + s
	case NP_FuncRef:  return "NP_FuncRef " + s
	case NP_Method:   return "NP_Method " + s
	case NP_CSize:    return "NP_CSize " + s

	/* memory access */

	case NM_Deref:    return "NM_Deref"
	case NM_Field:    return "NM_Field"
	case NM_Index:    return "NM_Index"

	/* misc */

	case NX_Operator: return "NX_Operator " + s

	default:
		return "??UNKNOWN??"
	}
}

func Dump(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}

	s := fmt.Sprintf("%*s%s", level, "", t.FullString())
	println(s)

	if t.children != nil {
		for _, child := range t.children {
			if child.kind != NH_Comment {
				Dump("", child, level + 3)
			}
		}
	}
}

func Dumpty(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}

	ty_str := "---"
	if t.ty != nil {
		ty_str = t.ty.String()
	}

	if t.offset != 0 {
		ty_str += fmt.Sprintf(" offset=%d", t.offset)
	}

	s := fmt.Sprintf("%*s%s : %s", level, "", t.FullString(), ty_str)
	println(s)

	if t.children != nil {
		for _, child := range t.children {
			if child.kind == NH_Comment {
				s = fmt.Sprintf("%*s; %s", level + 3, "", child.str)
				println(s)
			} else {
				Dumpty("", child, level + 3)
			}
		}
	}
}

func DumpLoops(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}
	for i, t := range t.children {
		info := "       "
		if level == 0 {
			info = fmt.Sprintf("%3d (%d)", i, t.loop)
		}

		s := fmt.Sprintf("%s %*s%s", info, level, "", t.FullString())

		if t.kind == NH_Comment {
			s = fmt.Sprintf("%s %*s; %s", info, level, "", t.str)
		}

		println(s)

		DumpLoops("", t, level + 3)
	}
}
