// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

void __rw_abort(char *msg) {
	fprintf(stderr, "%s", (char *)msg);
	fflush(stderr);

	abort();
}

void __rw_null_abort(char *file, int line) {
	fprintf(stderr, "NULL pointer dereferenced in %s:%d", (char *) file, line);
	fflush(stderr);

	abort();
}

void __rw_index_abort(char *file, int line) {
	fprintf(stderr, "out-of-bounds array index in %s:%d", (char *) file, line);
	fflush(stderr);

	abort();
}

void tinyrt__exit(int32_t code) {
	exit(code);
}

int16_t tinyrt__in__byte(void) {
	int ch = fgetc(stdin);

	if (ch == EOF || ferror(stdin)) {
		return -1;
	}
	return (int16_t)(ch & 255);
}

void tinyrt__out__byte(uint8_t c) {
	fputc((int)c, stdout);
	fflush(stdout);
}

void tinyrt__out__str(uint8_t *s) {
	fputs((char *)s, stdout);
	fflush(stdout);
}

void tinyrt__out__hex(uint64_t n) {
	uint64_t high = n >> 32;

	n = n & 0xFFFFFFFFUL;

	fprintf(stderr, "%08X%08X", (uint32_t)high, (uint32_t)n);
}
