;;
;; A simple adventure game
;;
;; by Andrew Apted, 2018-2021.
;;
;; this code is licensed as CC0 (i.e. public domain)
;;

#public

fun main () {
	welcome-message
	describe-room player-loc
	println ""
	loop until game-over {
		read-and-process-command
	}
	quit-message
}

;----------------------------------------------------------------------

#private

type uchar u8

; locations
type Loc s32

const LOC_Mountain = 1
const LOC_Forest   = 2
const LOC_Lake     = 3
const LOC_Outside  = 4
const LOC_Castle   = 5
const LOC_Treasury = 6

; directions
type Dir s32

const DIR_N = 1
const DIR_S = 2
const DIR_E = 3
const DIR_W = 4

type Room struct {
	.name    ^uchar
	.objects [20]Object
	.exits   [20]Exit
	.desc    [20]^uchar
}

type Exit struct {
	.dir  Dir
	.dest Loc
	.obstacle Object
}

type Object s32

const OBJ_Sword    = 1
const OBJ_Key      = 2
const OBJ_Steak    = 3
const OBJ_Carrot   = 4
const OBJ_Treasure = 5

const OBJ_Crocodile = 6
const OBJ_Parrot    = 7
const OBJ_Guard     = 8
const OBJ_Door      = 9

type ValueWord struct {
	.value s32   ; -1 terminates a list
	.name  ^uchar
}

type CmdFunc fun ()

type Command struct {
	.func ^CmdFunc
	.name ^uchar
}

;----------------------------------------------------------------------

const PASSWORD = "pinecone"

var game-over bool = FALSE

; --- player information ---

var player-loc Loc = LOC_Mountain

var player-inventory [20]Object = { OBJ_Sword ... }

var player-found-key bool = FALSE

; --- room definitions ---

var ROOMS [7]Room = {
	; the [0] element is just a place-holder
	{ NULL {...} {...} {...} }

	; LOC_Mountain
	{
		.name "Mountain"
		.objects {...}
		.exits { {DIR_N LOC_Forest 0} ... }
		.desc {
			"You are standing on a large grassy mountain."
			"To the north you see a thick forest."
			"Other directions are blocked by steep cliffs."
			...
		}
	}

	; LOC_Forest
	{
		.name "Forest"
		.objects { OBJ_Crocodile OBJ_Parrot ... }
		.exits {
			{ DIR_S LOC_Mountain 0 }
			{ DIR_W LOC_Lake 0 }
			{ DIR_E LOC_Outside OBJ_Crocodile }
			...
		}
		.desc {
			"You are in a forest, surrounded by dense trees and shrubs."
			"A wide path slopes gently upwards to the south, and"
			"narrow paths lead east and west."
			...
		}
	}

	; LOC_Lake
	{
		.name "Lake"
		.objects { OBJ_Steak ... }
		.exits { {DIR_E LOC_Forest 0} ... }
		.desc {
			"You stand on the shore of a beautiful lake, soft sand under"
			"your feet.  The clear water looks warm and inviting."
			...
		}
	}

	; LOC_Outside
	{
		.name "Outside"
		.objects {...}
		.exits {
			{ DIR_W LOC_Forest 0 }
			{ DIR_E LOC_Castle OBJ_Door }
			...
		}
		.desc {
			"The forest is thinning off here.  To the east you can see a"
			"large castle made of dark brown stone.  A narrow path leads"
			"back into the forest to the west."
			...
		}
	}

	; LOC_Castle
	{
		.name "Castle"
		.objects { OBJ_Guard OBJ_Carrot ... }
		.exits {
			{ DIR_W LOC_Outside  0 }
			{ DIR_S LOC_Treasury OBJ_Guard }
			...
		}
		.desc {
			"You are standing inside a magnificant, opulent castle."
			"A staircase leads to the upper levels, but unfortunately"
			"it is currently blocked off by delivery crates.  A large"
			"wooden door leads outside to the west, and a small door"
			"leads south."
			...
		}
	}

	; LOC_Treasury
	{
		.name "Treasury"
		.objects { OBJ_Treasure ... }
		.exits { {DIR_N LOC_Castle 0} ... }
		.desc {
			"Wow!  This room is full of valuable treasures.  Gold, jewels,"
			"valuable antiques sit on sturdy shelves against the walls."
			"However...... perhaps money isn't everything??"
			...
		}
	}
}

; --- various tables ---

rom-var OBJECTS [11]ValueWord = {
	{ 0 NULL }  ; unused

	{ OBJ_Sword     "sword"     }
	{ OBJ_Key       "key"       }
	{ OBJ_Steak     "steak"     }
	{ OBJ_Carrot    "carrot"    }
	{ OBJ_Treasure  "treasure"  }

	{ OBJ_Crocodile "crocodile" }
	{ OBJ_Parrot    "parrot"    }
	{ OBJ_Guard     "guard"     }
	{ OBJ_Door      "door"      }

	{ -1 NULL }  ; terminator
}

rom-var DIRS [10]ValueWord = {
	{ 0 NULL }  ; unused

	{ DIR_N "north" }
	{ DIR_S "south" }
	{ DIR_E "east"  }
	{ DIR_W "west"  }

	{ DIR_N "n" }
	{ DIR_S "s" }
	{ DIR_E "e" }
	{ DIR_W "w" }

	{ -1 NULL }  ; terminator
}

rom-var COMMANDS [31]Command = {
	{ cmd-quit   "quit"    }
	{ cmd-quit   "q"       }
	{ cmd-quit   "exit"    }
	{ cmd-invent "invent"  }
	{ cmd-invent "inventory" }
	{ cmd-invent "inv"     }
	{ cmd-invent "i"       }
	{ cmd-help   "help"    }
	{ cmd-look   "look"    }
	{ cmd-look   "l"       }
	{ cmd-go     "go"      }
	{ cmd-swim   "swim"    }
	{ cmd-swim   "dive"    }
	{ cmd-drop   "drop"    }
	{ cmd-get    "get"     }
	{ cmd-get    "take"    }
	{ cmd-give   "give"    }
	{ cmd-give   "offer"   }
	{ cmd-feed   "feed"    }
	{ cmd-open   "open"    }
	{ cmd-open   "unlock"  }
	{ cmd-attack "attack"  }
	{ cmd-attack "fight"   }
	{ cmd-attack "kill"    }
	{ cmd-attack "hit"     }
	{ cmd-use    "use"     }
	{ cmd-use    "apply"   }
	{ cmd-say    "say"     }
	{ cmd-say    "speak"   }
	{ cmd-say    "tell"    }

	{ NULL NULL }  ; terminator
}

rom-var DUD_WORDS [6]ValueWord = {
	{  1 "a"    }
	{  1 "an"   }
	{  1 "the"  }
	{  1 "to"   }
	{  1 "with" }

	{ -1  NULL  }  ; terminator
}

;----------------------------------------------------------------------

fun welcome-message () {
	println ""
	println "Welcome to a simple adventure game!"
	println ""
}

fun quit-message () {
	println "Goodbye!"
}

fun solved-message () {
	println "You help yourself to the treasure."
	println "With your good health and new-found wealth,"
	println "you live happily ever after...."
	println ""
	println "Congratulations, you solved the game!"
}

fun read-and-process-command () {
	let len = input-line

	; just quit on error or EOF [ nothing else we can do! ]
	if neg? len {
		game-over = TRUE
	} else {
		parse-user-command
	}
}

fun parse-user-command () {
	lowercase-words
	split-words

	if zero? word-count {
		if some? dud-words {
			println "Huh?"
			println ""
		}

		return
	}

	; convert a plain direction into a go command
	let verb = [word-buf 0]
	let dir  = lookup-word DIRS verb

	if pos? dir {
		[word-buf 1] = verb
		word-count = 2
		verb = "go"
	}

	word-count = word-count - 1

	let cmd = lookup-command verb

	if ref? cmd {
		call [cmd .func]
	} else {
		print "I don't understand \""
		print verb
		println "\""
	}

	println ""
}

;----------------------------------------------------------------------

fun cmd-quit () {
	game-over = TRUE
}

fun cmd-help () {
	println "Use text commands to walk around and do things."
	println ""
	println "Some examples:"
	println "   go north"
	println "   get the rope"
	println "   drop the lantern"
	println "   inventory"
	println "   unlock door"
	println "   kill the serpent"
	println "   quit"
}

fun cmd-invent () {
	println "You are carrying:"

	let i     = u32 0
	let count = u32 0

	loop while i < 20 {
		let obj = [player-inventory i]
		if pos? obj {
			print "    a "
			println [OBJECTS obj .name]
			count = count + 1
		}
		i = i + 1
	}

	if zero? count {
		println "    nothing"
	}
}

fun cmd-look () {
	describe-room player-loc
}

fun cmd-go () {
	if zero? word-count {
		println "Go where?"
		return
	}

	let dir = lookup-word DIRS [word-buf 1]

	if neg? dir {
		println "I don't understand that direction."
		return
	}

	let room = [ref ROOMS player-loc]
	let exit = ^Exit NULL

	let i = u32 0
	loop {
		exit = [ref room .exits i]
		if [exit .dir] == 0 {
			println "You cannot go that way."
			return
		}
		break if dir == [exit .dir]
		i = i + 1
	}

	let obstacle = [exit .obstacle]

	if obstacle == OBJ_Door {
		println "The castle door is locked!"

	} else if obstacle == OBJ_Crocodile {
		println "A huge, scary crocodile blocks your path!"

	} else if obstacle == OBJ_Guard {
		println "The guard stops you and says \"Hey, you cannot go in there"
		println "unless you tell me the password!\"."

	} else {
		player-loc = [exit .dest]
		println ""
		describe-room player-loc
	}
}

fun cmd-swim () {
	let loc = player-loc

	if loc == LOC_Outside {
		println "But the moat is full of crocodiles!"

	} else if loc != LOC_Lake {
		println "There is nowhere to swim here."

	} else if player-found-key {
		println "You enjoy a nice swim in the lake."

	} else {
		player-add-obj OBJ_Key
		player-found-key = TRUE

		println "You dive into the lake, enjoy paddling around for a while."
		println "Diving a bit deeper, you discover a rusty old key!"
	}
}

fun cmd-drop () {
	if zero? word-count {
		println "Drop what?"
		return
	}

	let room = [ref ROOMS player-loc]

	let obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj {
		; use a dummy obj number
		obj = 127
	}

	if player-has-obj obj {
		player-remove-obj obj
		room-add-obj room obj

		print "You drop the "
		print [word-buf 1]
		println "."
	} else {
		print "You are not carrying a "
		print [word-buf 1]
		println "."
	}
}

fun cmd-get () {
	if zero? word-count {
		println "Get what?"
		return
	}

	let room = [ref ROOMS player-loc]

	let obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj {
		; use a dummy obj number
		obj = 127
	}

	if not room-has-obj room obj {
		print "You do not see any "
		print [word-buf 1]
		println " here."

	} else if obj == OBJ_Crocodile {
		println "Are you serious?"
		println "The only thing you would get is eaten!"

	} else if obj == OBJ_Parrot {
		println "The parrot nimbly evades your grasp."

	} else if obj == OBJ_Guard {
		println "A momentary blush suggests the guard was flattered."

	} else if obj == OBJ_Treasure {
		game-over = TRUE
		solved-message

	} else {
		room-remove-obj room obj
		player-add-obj obj

		print "You pick up the "
		print [word-buf 1]
		println "."
	}
}

fun cmd-give () {
	; need two extra words (object and recipient)
	if word-count < 2 {
		println "Give what to whom?"
		return
	}

	let room = [ref ROOMS player-loc]

	let obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj {
		; use a dummy obj number
		obj = 127
	}

	let whom = lookup-word OBJECTS [word-buf 2]
	if neg? whom {
		; use a dummy obj number
		whom = 127
	}

	if not player-has-obj obj {
		print "You can't give a "
		print [word-buf 1]
		println ", since you don't have one!"

	} else if not room-has-obj room whom {
		print "There is no "
		print [word-buf 2]
		println " here."

	} else if (obj == OBJ_Carrot) and (whom == OBJ_Parrot) {
		player-remove-obj obj

		println "The parrot happily starts munching on the carrot.  Every now"
		print   "and then you hear it say \""
		print   PASSWORD
		println "\" as it nibbles away on that"
		println "orange stick.  I wonder who this parrot belonged to?"

	} else if (obj == OBJ_Steak) and (whom == OBJ_Crocodile) {
		player-remove-obj obj
		room-remove-obj room whom
		free-exit room DIR_E

		println "You hurl the steak towards the crocodile, which suddenly"
		println "snaps into action, grabbing the steak in its steely jaws"
		println "and slithering off to devour its meal in private."

	} else if whom >= OBJ_Crocodile {
		print "The "
		print [word-buf 2]
		println " is not interested."

	} else {
		println "Don't be ridiculous!"
	}
}

fun cmd-feed () {
	if word-count < 2 {
		println "Feed what to whom?"
		return
	}

	cmd-give
}

fun cmd-open () {
	if zero? word-count {
		println "Open what?"
		return
	}

	let room = [ref ROOMS player-loc]

	let obj = lookup-word OBJECTS [word-buf 1]

	if obj != OBJ_Door {
		println "You cannot open that."

	} else if player-loc != LOC_Outside {
		println "There is no door here."

	} else if not player-has-obj OBJ_Key {
		println "The door is locked!"

	} else {
		player-remove-obj OBJ_Key
		free-exit room DIR_E

		println "Carefully you insert the rusty old key in the lock, and turn it."
		println "Yes!!  The door unlocks!  However the key breaks into several"
		println "pieces and is useless now."
	}
}

fun cmd-attack () {
	if zero? word-count {
		println "Attack what?"
		return
	}

	let target = lookup-word OBJECTS [word-buf 1]

	if target == OBJ_Crocodile {
		println "The mere thought of wrestling with that savage beast"
		println "paralyses you with fear!"

	} else if target == OBJ_Guard {
		if player-has-obj OBJ_Sword {
			println "You and the guard begin a dangerous sword fight!"
			println "But after ten minutes or so, you are both exhausted and"
			println "decide to call it a draw."
		} else {
			println "You raise your hands to fight, then notice that the guard"
			println "is carrying a sword, so you shadow box for a while instead."
		}

	} else if player-has-obj OBJ_Sword {
		println "You swing your sword, but miss!"

	} else {
		println "You bruise your hand in the attempt."
	}
}

fun cmd-use () {
	if zero? word-count {
		println "Use what?"
		return
	}

	let obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj {
		; use a dummy obj number
		obj = 127
	}

	if not player-has-obj obj {
		println "You cannot use something you don't have."

	} else if obj == OBJ_Key {
		[word-buf 1] = "door"
		cmd-open

	} else if obj == OBJ_Sword {
		println "You practise your parry skills."

	} else {
		println "Its lack of utility leads to futility."
	}
}

fun cmd-say () {
	if zero? word-count {
		println "Say what?"
		return
	}

	let room = [ref ROOMS player-loc]

	if player-loc == LOC_Castle {
		if streq? [word-buf 1] PASSWORD {
			free-exit room DIR_S

			println "The guard says \"Welcome Sire!\" and beckons you to enter"
			println "the treasury."
			return
		}
	}

	println "Nothing happens."
}

;----------------------------------------------------------------------

; --- player utilities ---

fun player-has-obj (obj Object -> bool) {
	let i = u32 0
	loop {
		if i >= 20 {
			return FALSE
		}
		if [player-inventory i] == obj {
			return TRUE
		}
		i = i + 1
	}
}

fun player-add-obj (obj Object) {
	let i = u32 0
	loop {
		if zero? [player-inventory i] {
			[player-inventory i] = obj
			return
		}
		i = i + 1
	}
}

fun player-remove-obj (obj Object) {
	let i = u32 0
	loop while i < 20 {
		if [player-inventory i] == obj {
			[player-inventory i] = 0
		}
		i = i + 1
	}
}

; --- room utilities ---

fun free-exit (room ^Room dir Dir) {
	let i = u32 0
	loop while i < 20 {
		let exit = [ref room .exits i]
		if [exit .dir] == dir {
			[exit .obstacle] = 0
		}
		i = i + 1
	}
}

fun room-has-obj (room ^Room obj Object -> bool) {
	let i = u32 0
	loop {
		if i >= 20 {
			return FALSE
		}
		if [room .objects i] == obj {
			return TRUE
		}
		i = i + 1
	}
}

fun room-add-obj (room ^Room obj Object) {
	let i = u32 0
	loop {
		if zero? [room .objects i] {
			[room .objects i] = obj
			return
		}
		i = i + 1
	}
}

fun room-remove-obj (room ^Room obj Object) {
	let i = u32 0
	loop while i < 20 {
		if [room .objects i] == obj {
			[room .objects i] = 0
		}
		i = i + 1
	}
}

fun describe-room (loc Loc) {
	let room = [ref ROOMS loc]

	; show description lines
	let i = u32 0
	loop {
		let desc = [room .desc i]
		break if null? desc

		println desc
		i = i + 1
	}

	; show objects which are here
	i = u32 0
	loop while i < 20 {
		let obj = [room .objects i]
		if pos? obj {
			print "There is a "
			print [OBJECTS obj .name]
			println " here."
		}
		i = i + 1
	}
}

; --- parsing utilities ---

const MAX_LINE  = 200
const MAX_WORDS = 20

zero-var line-buf [MAX_LINE]uchar

; each pointer here will point into line-buf
var word-buf [MAX_WORDS]^uchar = {...}
var word-count u32 = 0
var dud-words  u32 = 0

fun lowercase-words () {
	let i = u32 0
	loop while i < MAX_LINE {
		[line-buf i] = to-lower [line-buf i]
		i = i + 1
	}
}

fun split-words () {
	word-count = 0
	dud-words  = 0

	let i     = s32 0
	let start = s32 0
	let len   = s32 0

	loop {
		let ch = [line-buf i]
		break if zero? ch

		if is-space? ch {
			; ensure previous word is NUL-terminated
			[line-buf i] = 0

			if pos? len {
				add-word [ref line-buf start]
				len = 0
			}
		} else {
			if zero? len {
				start = i
				len   = 1
			} else {
				len = len + 1
			}
		}

		i = i + 1
	}

	; ensure we have the last word
	if pos? len {
		add-word [ref line-buf start]
	}
}

fun is-space? (ch uchar -> bool) {
	ch <= 32
}

fun add-word (word ^uchar) {
	; reached limit?
	if word-count >= MAX_WORDS {
		return
	}

	; skip dud words like "a" and "the"
	let dud = lookup-word DUD_WORDS word
	if pos? dud {
		dud-words = dud-words + 1
		return
	}

	; handle some aliases
	if streq? word "croc" {
		word = "crocodile"
	}

	[word-buf word-count] = word
	word-count = word-count + 1
}

fun lookup-command (s ^uchar -> ^Command) {
	let i = u32 0
	loop {
		let cmd = [ref COMMANDS i]
		break if null? [cmd .func]

		if streq? s [cmd .name] {
			return cmd
		}

		i = i + 1
	}

	NULL ; not found
}

fun lookup-word (table ^[100]ValueWord s ^uchar -> s32) {
	let i = u32 0
	loop {
		let entry = [ref table i]
		break if neg? [entry .value]

		let name = [entry .name]
		if null? name {
			; ignore a placeholder (name is "")
		} else if streq? s name {
			return [entry .value]
		}

		i = i + 1
	}

	-1 ; not found
}

fun to-lower (ch u8 -> u8) {
	if (ch >= 'A') and (ch <= 'Z') {
		ch + 32
	} else {
		ch
	}
}

fun streq? (s1 ^uchar s2 ^uchar -> bool) {
	loop {
		let a = [s1]
		let b = [s2]

		if a != b {
			return FALSE
		}
		if a == 0 {
			return TRUE
		}

		s1 = s1 + 1
		s2 = s2 + 1
	}
}

; --- i/o utilities ---

fun println (s ^uchar) {
	tinyrt-out-str s
	tinyrt-out-byte '\r'
	tinyrt-out-byte '\n'
}

fun print (s ^uchar) {
	tinyrt-out-str s
}

fun input-line (-> s32) {
	; returns length of line, or -1 on error/eof

	tinyrt-out-str "> "

	let len = s32 0
	[line-buf len] = 0

	loop {
		let ch = tinyrt-in-byte

		; error or eof?
		if neg? ch {
			break if len > 0
			return -1
		}

		; end of the line?
		break if ch == 10

		; ignore the CR character
		continue if ch == 13

		; ignore chars when buffer is full
		let len2 = len + 4
		continue if len2 >= MAX_LINE

		[line-buf len] = uchar ch
		len = len + 1

		; keep buffer NUL-terminated at all times
		[line-buf len] = 0
	}

	len
}

;----------------------------------------------------------------------

extern-fun tinyrt-out-byte (c u8)
extern-fun tinyrt-out-str  (s ^u8)
extern-fun tinyrt-in-byte  (-> s16)

;--- editor settings ---
; vi:ts=4:sw=4:noexpandtab
