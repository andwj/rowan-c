// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/*
   this code contains optimisations which do not depend on the
   target architecture, plus some helper stuff for the back-end
   (architecture-specific) code.
*/

package main

// import "fmt"
import "strconv"

func (fu *FuncDef) MissingLabels() error {
	problems := 0

	// check that jumps go into their own block, or a parent of it
	for _, jump_info := range fu.jumps {
		blk  := jump_info.block
		name := jump_info.name

		for blk != nil {
			if blk.labels[name] {
				break  // ok
			}
			blk = blk.parent
		}

		if blk == nil {
			Error_SetPos(jump_info.pos)
			if fu.labels[name] == nil {
				PostError("missing label: %s", name)
			} else {
				PostError("jump into foreign block")
			}
			problems += 1
		}
	}

	if problems > 0 {
		return FAILED
	}

	// remove any labels which are unused
	used_labels := make(map[string]bool)

	for _, t := range fu.body.children {
		if t.kind == NH_Jump {
			used_labels[t.str] = true
		}
	}

	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	for _, t := range old_body.children {
		if t.kind == NH_Label && ! used_labels[t.str] {
			delete(fu.labels, t.str)
			continue
		}
		fu.body.Add(t)
	}

	return OK
}

//----------------------------------------------------------------------

type InlineAnalysis struct {
	input   []*FuncDef
	output  []*FuncDef
	seen    map[*FuncDef]bool
	example *FuncDef
}

type InlineResult int
const (
	INRES_OK       InlineResult = 0  // finished, all okay
	INRES_Progress InlineResult = 1  // progress was made
	INRES_Failed   InlineResult = 2  // cycle detected
)

func InlineAllFunctions() {
	lan := new(InlineAnalysis)

	lan.input  = make([]*FuncDef, 0)
	lan.output = make([]*FuncDef, 0)
	lan.seen   = make(map[*FuncDef]bool)

	// collect all function defs
	for _, mod := range ordered_mods {
		for _, def := range mod.ord_defs {
			if def.kind == DEF_Func || def.kind == DEF_Method {
				lan.input = append(lan.input, def.d_func)
			}
		}
	}

	// determine an ordering so that any inlined function is handled
	// before the function which inlines it.  also detect situations
	// which are impossible to satisfy, for example: A inlines B and
	// B inlines A.

	for {
		res := lan.DoPass()
		if res == INRES_OK {
			break
		} else if res == INRES_Failed {
			Error_SetPos(lan.example.body.pos)
			PostError("cyclic inlining of functions (such as %s)", lan.example.name)
			return
		}
	}

	/* we now have a usable ordering */

	for _, fu := range lan.output {
		fu.PerformInlining()
	}
	for _, fu := range lan.input {
		if !fu.inline && !fu.extern {
			fu.PerformInlining()
		}
	}
}

func (lan *InlineAnalysis) DoPass() InlineResult {
	count    := 0
	progress := false

	for _, fu := range lan.input {
		if fu.inline && !lan.seen[fu] {
			count += 1
			lan.example = fu

			if lan.Visit(fu) {
				lan.output = append(lan.output, fu)
				lan.seen[fu] = true
				progress = true
			}
		}
	}

	if count == 0 {
		return INRES_OK
	} else if progress {
		return INRES_Progress
	} else {
		return INRES_Failed
	}
}

func (lan *InlineAnalysis) Visit(fu *FuncDef) bool {
	for _, t := range fu.body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_Call {
			called_func := fu.GetCalledFunc(comp)

			if called_func == nil {
				continue
			}
			if !called_func.inline {
				continue
			}
			if lan.seen[called_func] {
				continue
			}

			// this inline func is not yet "leafy"
			return false
		}
	}

	// this inline func is "leafy"
	return true
}

func (fu *FuncDef) PerformInlining() {
	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	for _, t := range old_body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_Call {
			called_func := fu.GetCalledFunc(comp)

			if called_func != nil && called_func.inline {
				t_new := fu.PerformInlineCall(t, called_func)
				if t_new != nil {
					fu.body.Add(t_new)
				}
				fu.num_inlines += 1
				continue
			}
		}

		fu.body.Add(t)
	}
}

func (fu *FuncDef) PerformInlineCall(t_call *Node, other *FuncDef) *Node {
	comp := t_call.GetComputation()

	suffix := ":" + strconv.Itoa(fu.num_inlines)

	// copy the locals, including parameters
	local_map := make(map[*LocalInfo]*LocalInfo)

	for _, local := range other.locals {
		local_map[local] = fu.CopyInlineLocal(local, suffix)
	}

	// NOTE: no need to update fu.labels

	// create assignments for the parameter locals
	for i, par_info := range other.params {
		call_par := comp.children[1 + i]

		new_local := local_map[par_info]
		if new_local == nil {
			panic("unmapped local")
		}

		var_name := par_info.name + suffix
		var_dest := NewNode(NP_Local, var_name, t_call.pos)
		var_dest.local = new_local
		var_dest.ty = par_info.ty

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(var_dest)
		t_move.Add(call_par)

		fu.body.Add(t_move)
	}

	// return values may need a local to store it
	var ret_local *LocalInfo
	var ret_local_node *Node
	var ret_local_exist bool = false

	switch other.ty.sub.kind {
	case TYP_Void, TYP_NoReturn:
		// no local needed
	default:
		if t_call.kind == NH_Drop {
			// no local needed
		} else if t_call.kind == NH_Move && t_call.children[0].kind == NP_Local {
			// use the existing local
			ret_local_node = t_call.children[0]
			ret_local = ret_local_node.local
			ret_local_exist = true
		} else {
			ret_local = fu.NewLocal("__inline" + suffix, other.ty.sub, nil)
			ret_local_node = NewNode(NP_Local, "", t_call.pos)
			ret_local_node.local = ret_local
			ret_local_node.ty = ret_local.ty
		}
	}

	need_end := false

	// copy the statements
	for _, t2 := range other.body.children {
		if t2.kind == NH_Return {
			need_end = true
		}

		fu.CopyInlineStatement(t2, suffix, ret_local, t_call.pos, local_map)
	}

	if need_end {
		lab_end := NewNode(NH_Label, "__end" + suffix, t_call.pos)
		fu.body.Add(lab_end)
	}

	// copy the original node (NH_Move, NH_Drop, etc...)

	switch other.ty.sub.kind {
	case TYP_Void, TYP_NoReturn:
		// nothing needed
		return nil
	}

	switch t_call.kind {
	case NH_Drop:
		// nothing needed
		return nil

	case NH_Move:
		if ret_local_exist {
			// nothing needed, inlined code writes result to an existing local
			return nil
		}

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(t_call.children[0])
		t_move.Add(ret_local_node)
		return t_move

	case NH_Jump:
		t_jump := NewNode(NH_Jump, t_call.str, t_call.pos)
		t_jump.negate = t_call.negate
		t_jump.Add(ret_local_node)
		return t_jump

	case NH_Return:
		t_ret := NewNode(NH_Return, "", t_call.pos)
		t_ret.Add(ret_local_node)
		return t_ret

	default:
		panic("strange node kind in PerformInlineCall")
	}
}

func (fu *FuncDef) CopyInlineLocal(local *LocalInfo, suffix string) *LocalInfo {
	new_loc := fu.NewLocal(local.name + suffix, local.ty, nil)
	return new_loc
}

func (fu *FuncDef) CopyInlineStatement(t *Node, suffix string, ret_local *LocalInfo, pos Position,
	local_map map[*LocalInfo]*LocalInfo) {

	if t.kind == NH_Return {
		// special logic for return statements: transfer a needed value
		// to the return local, then jump to the end label.

		if ret_local != nil {
			comp := fu.CopyInlineNode(t.children[0], suffix, pos, local_map)

			dest := NewNode(NP_Local, "", pos)
			dest.local = ret_local
			dest.ty = ret_local.ty

			t_move := NewNode(NH_Move, "", pos)
			t_move.Add(dest)
			t_move.Add(comp)

			fu.body.Add(t_move)
		}

		t_jump := NewNode(NH_Jump, "__end" + suffix, pos)
		fu.body.Add(t_jump)
		return
	}

	new_node := fu.CopyInlineNode(t, suffix, pos, local_map)
	fu.body.Add(new_node)
}

func (fu *FuncDef) CopyInlineNode(t1 *Node, suffix string, pos Position,
	local_map map[*LocalInfo]*LocalInfo) *Node {

	t2 := NewNode(t1.kind, t1.str, pos)

	t2.ty     = t1.ty
	t2.module = t1.module
	t2.offset = t1.offset
	t2.negate = t1.negate
	t2.def    = t1.def

	// adjust labels and locals with a suffix
	switch t1.kind {
	case NH_Label, NH_Jump:
		t2.str += suffix

	case NP_Local:
		t2.local = local_map[t1.local]
		if t2.local == nil {
			panic("unmapped local")
		}
	}

	for _, child := range t1.children {
		t2.Add(fu.CopyInlineNode(child, suffix, pos, local_map))
	}
	return t2
}

//----------------------------------------------------------------------

// CodePath represents a path of code statements in a function.
// The path may be empty, it represents a function which has not yet
// started execution.
type CodePath struct {
	// the next node to be visited, -1 if finished
	next int

	// nodes in the path (indexes into body)
	visited map[int]bool

	// the locals which have been created along this path.
	locals map[*LocalInfo]bool

	// this path produced an error
	failed bool
}

func NewCodePath() *CodePath {
	pt := new(CodePath)
	pt.next    = 0
	pt.visited = make(map[int]bool, 0)
	pt.locals  = make(map[*LocalInfo]bool)
	return pt
}

func (pt *CodePath) Copy() *CodePath {
	pt2 := new(CodePath)
	pt2.next = pt.next

	pt2.visited = make(map[int]bool)
	for idx,_ := range pt.visited {
		pt2.visited[idx] = true
	}
	pt2.locals = make(map[*LocalInfo]bool)
	for loc,_ := range pt.locals {
		pt2.locals[loc] = true
	}
	return pt2
}

func (pt1 *CodePath) CanMerge(pt2 *CodePath) bool {
	return pt1.next == pt2.next
}

func (pt *CodePath) Merge(old *CodePath) {
	for idx,_ := range old.visited {
		pt.visited[idx] = true
	}

	// only keep locals that are common to both paths
	new_locals := make(map[*LocalInfo]bool)
	for loc, _ := range old.locals {
		if pt.locals[loc] {
			new_locals[loc] = true
		}
	}

	pt.locals = new_locals
	pt.failed = pt.failed || old.failed

	// mark old path as dead
	old.next = -1
}

func (fu *FuncDef) FlowAnalysis() error {
	for _, t := range fu.body.children {
		t.dead = true
	}

	// create initial path
	pt := NewCodePath()

	fu.paths = make([]*CodePath, 0)
	fu.paths = append(fu.paths, pt)

	error_count := 0

	/* process active paths until done */

	for len(fu.paths) > 0 {
		// remove dead paths from the end
		last := len(fu.paths) - 1
		pt   := fu.paths[last]

		if pt.next < 0 {
			if pt.failed {
				error_count += 1
			}
			fu.paths = fu.paths[0:last]
			continue
		}

		// choose the path at earliest node
		for i := 0; i < last; i++ {
			other := fu.paths[i]
			if other.next >= 0 && other.next < pt.next {
				pt = other
			}
		}

		// attempt to merge other paths into this one
		for i := 0; i <= last; i++ {
			other := fu.paths[i]
			if other != pt {
				if pt.CanMerge(other) {
					pt.Merge(other)
				}
			}
		}

		fu.FlowStep(pt)
	}

	if error_count > 0 {
		return FAILED
	}
	return OK
}

func (fu *FuncDef) FlowStep(pt *CodePath) {
	// reached end?
	if pt.next >= fu.body.Len() {
		ret_type := fu.ty.sub

		if ret_type.kind != TYP_Void && !fu.missing_ret {
			fu.missing_ret = true
			last := fu.body.Last()
			Error_SetPos(last.pos)
			PostError("control reaches end of non-void function")
		}
		pt.next = -1
		return
	}

	// already visited? (i.e. looping back)
	if pt.visited[pt.next] {
		pt.next = -1
		return
	}

	pt.visited[pt.next] = true

	t := fu.body.children[pt.next]
	t.dead = false

	// check for use of uninitialized locals
	err := fu.FlowCheckLocals(t, pt)
	if err != nil {
		pt.failed = true
	}

	// check for creation of locals
	if t.kind == NH_Move {
		if t.children[0].kind == NP_Local {
			local := t.children[0].local
			pt.locals[local] = true
		}
	}

	// handle return statements, or equivalent
	switch t.kind {
	case NH_Return, NH_TailCall, NH_Abort:
		pt.next = -1
		return

	case NH_Drop:
		// calling a `no-return` function?
		comp := t.children[0]

		if comp.kind == NC_Call {
			t_func   := comp.children[0]
			ret_type := t_func.ty.sub

			if ret_type.kind == TYP_NoReturn {
				pt.next = -1
				return
			}
		}
	}

	// handle jumps.
	// conditional jumps split the path into two.

	if t.kind == NH_Jump {
		dest_idx := fu.FindLabel(t.str)

		// WISH: detect when a jump is not really conditional, especially in
		//       loops where a local begins at zero and stops at a constant.

		if t.Len() > 0 {
			branch := pt.Copy()
			branch.next = pt.next + 1
			fu.paths = append(fu.paths, branch)
		}

		pt.next = dest_idx
		return
	}

	pt.next += 1
}

func (fu *FuncDef) FlowCheckLocals(t *Node, pt *CodePath) error {
	if t.kind == NP_Local {
		local := t.local

		// has been created in this path?
		if pt.locals[local] {
			return OK
		}
		// parameters don't need a previous write
		if local.param_idx >= 0 {
			return OK
		}
		// prevent multiple errors
		if !local.errored {
			local.errored = true

			Error_SetPos(t.pos)
			PostError("local '%s' may be used uninitialized", t.local.name)
		}

		pt.failed = true
		return FAILED
	}

	start := 0
	if t.kind == NH_Move && t.children[0].kind == NP_Local {
		start = 1
	}
	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		if fu.FlowCheckLocals(child, pt) != OK {
			return FAILED
		}
	}

	return OK
}

func (fu *FuncDef) DeadCodeRemoval() {
	if Options.no_optim {
		return
	}

	// WISH: detect a jump statement followed by its label
	//       (with nothing but comments in between), remove it.

	for _, t := range fu.body.children {
		if t.dead {
			if t.kind != NH_Comment && t.kind != NH_Label {
				t.kind = NH_Comment
				t.str  = "[ DEAD CODE ]"
			}
		}
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) UnusedLocals() {
	if Options.no_optim {
		return
	}

	// keep trying until no more changes
	for fu.UnusedLocalPass() {
	}
}

func (fu *FuncDef) UnusedLocalPass() bool {
	changed := false

	// determine which locals are actually read
	fu.LocalUsages()

	// we remove nodes writing to an unused local
	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	removals := make(map[*LocalInfo]bool)

	for _, t := range old_body.children {
		if t.kind != NH_Move {
			fu.body.Add(t)
			continue
		}

		dest := t.children[0]
		comp := t.children[1]

		kill_node := false

		if dest.kind == NP_Local {
			local := dest.local

			if local.param_idx >= 0 {
				// ignore parameters here

			} else if local.reads == 0 {
				kill_node = true
			}

			// leave a tombstone
			if kill_node {
				if !removals[dest.local] {
					removals[dest.local] = true

					msg   := "[ " + dest.str + " OPTIMISED AWAY ]"
					cmt   := NewNode(NH_Comment, msg, t.pos)
					blank := NewNode(NH_Comment, "",  t.pos)

					fu.body.Add(cmt)
					fu.body.Add(blank)
				}
			}
		}

		// certain nodes MUST be kept, these become NH_Drop
		if kill_node {
			changed = true

			switch comp.kind {
			case NC_MemRead, NC_Call:
				t2 := NewNode(NH_Drop, "", t.pos)
				t2.Add(comp)
				fu.body.Add(t2)

			default:
				// remove the node (by doing nothing)
			}
		} else {
			fu.body.Add(t)
		}
	}

	// remove the LocalInfo data (rebuild the list)
	old_locals := fu.locals
	fu.locals = make([]*LocalInfo, 0, len(old_locals))

	for _, local := range old_locals {
		if local.reads == 0 && local.param_idx < 0 {
			changed = true
		} else {
			fu.locals = append(fu.locals, local)
		}
	}

	return changed
}

func (fu *FuncDef) LocalUsages() {
	for _, local := range fu.locals {
		local.reads = 0
	}

	for _, t := range fu.body.children {
		start := 0
		var skip_local *LocalInfo

		if t.kind == NH_Move {
			dest := t.children[0]

			if dest.kind == NP_Local {
				local := dest.local

				// skip this destination when marking reads
				start = 1

				// when determining unused locals, a statement like `x = iadd x 1`
				// should not be considered as reading from ("using") the local x.
				// however function calls (etc) must not be removed.
				{
					comp := t.children[1]
					if !(comp.kind == NC_Call || comp.kind == NC_MemRead) {
						skip_local = local
					}
				}
			}
		}

		if t.kind == NH_Swap {
			for _, child := range t.children {
				if child.kind == NP_Local {
					// local := child.local
				}
			}
		}

		fu.LocalUsagesInNode(t, start, skip_local)
	}
}

func (fu *FuncDef) LocalUsagesInNode(t *Node, start int, skip_local *LocalInfo) {
	if t.kind == NP_Local {
		local := t.local
		if local != skip_local {
			local.reads += 1
		}
	}

	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		fu.LocalUsagesInNode(child, 0, skip_local)
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) FindLabel(label string) int {
	for idx, line := range fu.body.children {
		if line.kind == NH_Label && line.str == label {
			return idx
		}
	}

	panic("no such label: " + label)
}
