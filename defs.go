// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strings"

type Definition struct {
	kind    DefKind

	d_alias *AliasDef
	d_const *ConstDef
	d_type  *TypeDef
	d_var   *VarDef
	d_func  *FuncDef
}

type DefKind int
const (
	DEF_Alias DefKind = iota
	DEF_Const
	DEF_Type
	DEF_Func
	DEF_Method
	DEF_Var
)

type AliasDef struct {
	name   string
	target string
	module string
}

type ConstDef struct {
	name   string
	module string
	kind   ConstKind
	value  string
	public bool
	expr   *Node
}

type ConstKind int
const (
	CONST_Int   = iota
	CONST_Float
	CONST_Char
	CONST_Bool
	CONST_String
	CONST_CSize   // for C back-end
)

type TypeDef struct {
	name   string
	module string
	extern bool
	alias  bool
	public bool
	kind   string
	body   *Node
	ty     *Type
	ref_ty *Type  // for aliases
}

type VarDef struct {
	name     string
	module   string
	extern   bool
	zeroed   bool
	rom      bool
	public   bool

	expr     *Node
	raw_type *Node
	ty       *Type
	ptr      *Type

	// stuff for the C generation back-end
	_out     *OutThing
}

type FuncDef struct {
	name     string
	module   string
	extern   bool
	inline   bool
	method   bool
	public   bool

	raw_pars *Node
	body     *Node
	ty       *Type
	ptr      *Type

	// parsing info....
	labels      map[string]*LabelInfo
	jumps       []*LabelInfo
	if_labels   int
	loop_labels int
	error_num   int
	cur_block   *BlockInfo

	locals      []*LocalInfo
	params      []*LocalInfo
	num_temps   int
	num_inlines int

	// optimisation info....
	paths  []*CodePath
	missing_ret bool

	// stuff for the C generation back-end
	_out        *OutThing
	stack_vars  []string
}

type LocalInfo struct {
	name string
	ty   *Type

	// this is >= 0 for parameters, -1 for locals
	param_idx int

	// this local is read-only
	immutable bool

	// this local is the receiver of a method
	self bool

	// true if we have detected an error with this local
	errored bool

	// these keep track of usages of a local var.
	// the `writes` field includes the creation of the var.
	reads  int

	// stuff for the C generation back-end
	out_name string
}

type LabelInfo struct {
	name   string
	block *BlockInfo
	pos    Position
}

type BlockInfo struct {
	kind      string
	loop_lab  string
	break_lab string
	tail      bool
	parent    *BlockInfo
	labels    map[string]bool
	locals    map[string]*LocalInfo
	seen      map[string]bool
}

func NewBlockInfo(kind string) *BlockInfo {
	blk := new(BlockInfo)
	blk.kind = kind
	blk.labels = make(map[string]bool)
	blk.locals = make(map[string]*LocalInfo)
	blk.seen   = make(map[string]bool)
	return blk
}

func (def *Definition) Name() string {
	switch def.kind {
	case DEF_Alias:  return def.d_alias.name
	case DEF_Const:  return def.d_const.name
	case DEF_Type:   return def.d_type.name
	case DEF_Func:   return def.d_func.name
	case DEF_Method: return def.d_func.name
	case DEF_Var:    return def.d_var.name
	default:         return ""
	}
}

func (def *Definition) IsPublic() bool {
	switch def.kind {
	case DEF_Alias:  return false
	case DEF_Const:  return def.d_const.public
	case DEF_Type:   return def.d_type.public
	case DEF_Func:   return def.d_func.public
	case DEF_Method: return def.d_func.public
	case DEF_Var:    return def.d_var.public
	default:         return false
	}
}

func ReplaceAlias (t *Node) {
	if t.kind == NL_Name {
		if cur_mod != nil {
			def := cur_mod.defs[t.str]

			if def != nil && def.kind == DEF_Alias {
				t.str    = def.d_alias.target
				t.module = def.d_alias.module
			}
		}
	}
}

func (blk *BlockInfo) LookupLocal(name string) *LocalInfo {
	for blk != nil {
		lvar := blk.locals[name]
		if lvar != nil {
			return lvar
		}
		blk = blk.parent
	}
	return nil
}

func (blk *BlockInfo) SeenLocal(name string) bool {
	for blk != nil {
		if blk.seen[name] {
			return true
		}
		blk = blk.parent
	}
	return false
}

//----------------------------------------------------------------------

const (
	// allow the name to shadow a global definition
	ALLOW_SHADOW = (1 << iota)
	ALLOW_UNDERSCORE
	ALLOW_MODULE
)

func ValidateName(t_name *Node, what string, flags int) error {
	if t_name.kind != NL_Name {
		PostError("bad %s name: not an identifier", what)
		return FAILED
	}

	if t_name.module != "" {
		if (flags & ALLOW_MODULE) == 0 {
			PostError("bad %s name: cannot contain a '/'", what)
			return FAILED
		}
	}

	name := t_name.str

	if name == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OK
	}

	// disallow field names
	if len(name) >= 1 && name[0] == '.' {
		PostError("bad %s name: cannot begin with a dot", what)
		return FAILED
	}

	// disallow attributes
	if len(name) >= 1 && name[0] == '@' {
		PostError("bad %s name: cannot begin with a '@'", what)
		return FAILED
	}

	// disallow special keywords
	if len(name) >= 1 && name[0] == '#' {
		PostError("bad %s name: cannot begin with a '#'", what)
		return FAILED
	}

	// already defined?
	if (flags & ALLOW_SHADOW) == 0 {
		if cur_mod != nil {
			if cur_mod.defs[name] != nil {
				PostError("bad %s name: '%s' already defined", what, name)
				return FAILED
			}
		}
	}

	// disallow language keywords
	if IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow operators
	if IsOperatorName(name) {
		PostError("bad %s name: '%s' is a math operator", what, name)
		return FAILED
	}

	// type names cannot contain a dot
	if what == "type" {
		for _, ch := range name {
			if ch == '.' {
				PostError("bad %s name: cannot contain a dot", what)
				return FAILED
			}
		}
	}

	// disallow names with certain suffixes
	if len(name) >= 6 && strings.HasSuffix(name, ".size") {
		PostError("bad %s name: '.size' is a reserved suffix", what)
		return FAILED
	}

	return OK
}

func IsLanguageKeyword(name string) bool {
	switch name {
	case
		// directives
		"#public", "#private",
		"const", "type", "extern-type",
		"var", "extern-var", "zero-var", "rom-var", "stack-var",
		"fun", "extern-fun", "inline-fun",

		// statements
		"if", "else", "unless", "do", "let",
		"loop", "while", "until", "break", "continue",
		"jump", "return", "call", "tail-call",
		"cast", "ref",
		"mem-read", "mem-write", "swap",
		"sel-if", "match", "matches?",
		"min", "max", "assert",

		// types
		"u8", "u16", "u32", "u64",
		"s8", "s16", "s32", "s64",
		"^", "bool", "void", "no-return", "raw-mem",
		"array", "struct", "union", "raw-struct", "raw-union",

		// constants
		"NULL", "FALSE", "TRUE",
		"+INF", "-INF", "SNAN", "QNAN",
		"CPU_BITS", "CPU_ENDIAN", "PTR_SIZE",

		// miscellaneous
		":", "::", "_", "\\", "->", "=>", ".", "..", "...":

		return true
	}

	return false
}

//----------------------------------------------------------------------

func CompileAllConstants() {
	// visit all constant definitions which need to be evaluated.
	// we process them repeatedly until the count shrinks to zero.
	// if it fails to shrink, it means there is a cyclic dependency.

	last_count := -1  // first time through

	for {
		count     := 0
		got_error := false
		example   := ""

		for _, mod := range ordered_mods {
			cur_mod = mod

			for _, def := range mod.ord_defs {
				if def.kind == DEF_Const {
					con := def.d_const

					if con.expr != nil {
						count += 1

						if con.TryEvaluate() != OK {
							got_error = true
						}
						if con.expr != nil {
							example = con.name
						}
					}
				}
			}
		}

		if count == 0 || got_error {
			return
		}
		if count == last_count {
			PostError("cyclic dependencies with constants (such as %s)", example)
			return
		}

		last_count = count
	}
}

func (con *ConstDef) TryEvaluate() error {
	Error_SetPos(con.expr.pos)

	var ectx EvalContext
	t_value := ectx.EvalTerm(con.expr)

	if t_value == P_FAIL {
		return FAILED
	}
	if t_value == P_UNKNOWN {
		// try again next round
		return OK
	}
	if t_value.kind == NL_FltSpec {
		PostError("bad const def: expected value, got: %s", t_value.String())
		return FAILED
	}

	switch t_value.kind {
	case NL_Char:   con.kind = CONST_Char
	case NL_Float:  con.kind = CONST_Float
	case NL_String: con.kind = CONST_String
	case NL_Bool:   con.kind = CONST_Bool
	default:        con.kind = CONST_Int
	}

	con.value = t_value.str
	con.expr  = nil

	return OK
}

func (con *ConstDef) MakeLiteral(pos Position) *Node {
	var kind NodeKind

	if con.kind == CONST_CSize {
		op := NewNode(NP_CSize, con.value, pos)
		op.def, _ = LookupDef(con.value, con.module)
		return op
	}

	switch con.kind {
	case CONST_Int:    kind = NL_Integer
	case CONST_Float:  kind = NL_Float
	case CONST_Char:   kind = NL_Char
	case CONST_Bool:   kind = NL_Bool
	case CONST_String: kind = NL_String
	default: panic("bad constant")
	}

	t_lit := NewNode(kind, con.value, pos)
	return t_lit
}
