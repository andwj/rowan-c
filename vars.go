// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "math"
import "strconv"
import "unicode/utf8"

func DeduceAllVars() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var {
			def.d_var.Deduce()
		}
	}
	Error_SetWhat("")
}

func ParseAllVars() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var && !(def.d_var.extern || def.d_var.zeroed) {
			def.d_var.Parse()
		}
	}
	Error_SetWhat("")
}

func CompileAllVars() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var {
			cur_mod.gen.CompileVar(def.d_var)
		}
	}
	Error_SetWhat("")
}

//----------------------------------------------------------------------

// Deduce merely determines the overall type of the variable.
func (vdef *VarDef) Deduce() error {
	Error_SetWhat(vdef.name)
	Error_SetPos(vdef.raw_type.pos)

	ty, count := ParseTypeSpec(vdef.raw_type.children, "a variable")
	if ty == nil {
		return FAILED
	}
	if count < vdef.raw_type.Len() {
		PostError("extra rubbish after type in var def")
		return FAILED
	}

	if ty.kind == TYP_Void || ty.kind == TYP_NoReturn {
		PostError("global vars cannot be void or no-return")
		return FAILED
	}
	if ty.kind == TYP_Function {
		PostError("global vars cannot be a raw function (use a pointer)")
		return FAILED
	}

	vdef.ty  = ty
	vdef.ptr = NewPointerType(ty)

	return OK
}

//----------------------------------------------------------------------

// Parse looks at the expression/body of the variable and produces
// higher-level nodes for the CompileVar code to use.
func (vdef *VarDef) Parse() error {
	Error_SetPos(vdef.raw_type.pos)
	Error_SetWhat(vdef.name)

	new_expr := vdef.ParseValue(vdef.expr, vdef.ty)
	if new_expr == P_FAIL {
		return FAILED
	}

	vdef.expr = new_expr
	return OK
}

func (vdef *VarDef) ParseValue(t *Node, ty *Type) *Node {
	Error_SetPos(t.pos)

	switch ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		t = vdef.ParseSimpleValue(t, ty)

		if t != P_FAIL {
			// check if value fits into type
			if CheckNumericValue(t, ty) != OK {
				return P_FAIL
			}
		}
		return t

	case TYP_Array:
		return vdef.ParseArray(t, ty)

	case TYP_Struct:
		return vdef.ParseStruct(t, ty)

	case TYP_Union:
		return vdef.ParseUnion(t, ty)

	default:
		PostError("strange type in data: %s", ty.SimpleName())
		return P_FAIL
	}
}

func (vdef *VarDef) ParseSimpleValue(t *Node, ty *Type) *Node {
	if t.kind == NG_Data || t.kind == NG_Block {
		PostError("expected value, got: %s", t.String())
		return P_FAIL
	}

	if t.kind == NG_Expr {
		var ectx EvalContext

		t = ectx.EvalTerm(t)
		if t == P_FAIL {
			return P_FAIL
		}
	}

	if t.kind == NL_Name {
		if !ValidateModUsage(t.module) {
			return P_FAIL
		}
		ReplaceAlias(t)

		def, mod := LookupDef(t.str, t.module)

		if def == nil {
			PostError("unknown identifier: %s", t.str)
			return P_FAIL
		}
		if mod != cur_mod && !def.IsPublic() {
			PostError("%s is private to module %s", t.str, mod.name)
			return P_FAIL
		}

		if def.kind == DEF_Const {
			t = def.d_const.MakeLiteral(t.pos)

		} else if ty.kind == TYP_Pointer {
			return vdef.ParsePointerValue(t, ty, def)

		} else {
			// leave `t` unchanged, trigger an error below
		}
	}

	switch ty.kind {
	case TYP_Int:
		switch t.kind {
		case NL_Integer, NL_Char, NP_CSize:
			// ok
		default:
			PostError("expected integer value, got: %s", t.String())
			return P_FAIL
		}

	case TYP_Float:
		switch t.kind {
		case NL_Float, NL_FltSpec:
			// ok
		case NL_Integer, NL_Char:
			// ok, convert integer to a float
			if IntegerToFloat(t) != nil {
				return P_FAIL
			}
		default:
			PostError("expected float value, got: %s", t.String())
			return P_FAIL
		}

	case TYP_Bool:
		switch t.kind {
		case NL_Bool:
			// ok
		default:
			PostError("expected boolean value, got: %s", t.String())
			return P_FAIL
		}

	case TYP_Pointer:
		switch t.kind {
		case NL_String:
			if ! (ty.kind == TYP_Pointer && ty.sub.kind == TYP_Int &&
				ty.sub.unsigned && ty.sub.bits < 64) {

				PostError("cannot use string in that context (type mismatch)")
				return P_FAIL
			}
			t.ty = ty

		case NL_Null:
			// ok

		default:
			PostError("expected a pointer value, got: %s", t.String())
			return P_FAIL
		}
	}

	return t
}

func (vdef *VarDef) ParsePointerValue(t *Node, ty *Type, def *Definition) *Node {
	// this should be a global var or function name
	if !(def.kind == DEF_Var || def.kind == DEF_Func) {
		PostError("expected a pointer value, got: %s", t.str)
		return P_FAIL
	}

	// NOTE: we only perform limited type checking here, as there
	// is no way to explicitly cast to a different pointer type.
	// so we just check that functions are used appropriately.

	ty_funky  := (ty.sub.kind == TYP_Function)
	def_funky := (def.kind == DEF_Func)

	if ty_funky && !def_funky {
		PostError("expected a function pointer, got: %s", t.str)
		return P_FAIL
	}
	if !ty_funky && def_funky {
		PostError("cannot use function in that context (type mismatch)")
		return P_FAIL
	}
	if def_funky && def.d_func.inline {
		PostError("cannot take the address of an inline function")
		return P_FAIL
	}

	is_extern := false
	if def.kind == DEF_Func { is_extern = def.d_func.extern }
	if def.kind == DEF_Var  { is_extern = def.d_var .extern }

	mod_name := ""
	if def.kind == DEF_Func { mod_name = def.d_func.module }
	if def.kind == DEF_Var  { mod_name = def.d_var .module }

	name := def.Name()

	t_glob := NewNode(NP_GlobPtr, name, t.pos)
	t_glob.ty = vdef.ptr
	t_glob.def = def

	// need a fully-qualified name for NP_GlobPtr, unless extern
	if is_extern {
		t_glob.module = ""
	} else if mod_name != "" {
		t_glob.module = mod_name
	} else {
		t_glob.module = cur_mod.name
	}

	return t_glob
}

func (vdef *VarDef) ParseArray(t *Node, ty *Type) *Node {
	if t.kind != NG_Block {
		PostError("expected array data, got: %s", t.String())
		return P_FAIL
	}

	if ty.size == 0 {
		if t.Len() > 0 {
			PostError("expected empty {}")
			return P_FAIL
		}
		return t
	}

	new_array := NewNode(NG_Block, "", t.pos)

	// visit all elements to check type
	t = t.CompressBlock()
	children := t.children

	for len(children) > 0 {
		elem := children[0]
		children = children[1:]

		Error_SetPos(elem.pos)

		// handle `...` which fills remaining values with a default
		if elem.Match("...") {
			if vdef.FillArray(new_array, ty) != OK {
				return P_FAIL
			}
			break
		}

		// handle strings where int values are expected
		if ty.sub.kind != TYP_Pointer {
			switch elem.kind {
			case NL_String:
				if vdef.StringToArray(new_array, ty, elem.str) != OK {
					return P_FAIL
				}
				continue

			case NL_Name:
				// no need to replace an alias here
				def, mod := LookupDef(elem.str, elem.module)

				if def != nil && def.kind == DEF_Const && def.d_const.kind == CONST_String {
					if mod != cur_mod && !def.IsPublic() {
						PostError("%s is private to module %s", elem.str, mod.name)
						return P_FAIL
					}
					if vdef.StringToArray(new_array, ty, def.d_const.value) != OK {
						return P_FAIL
					}
					continue
				}
			}
		}

		elem2 := vdef.ParseValue(elem, ty.sub)
		if elem2 == P_FAIL {
			return P_FAIL
		}
		new_array.Add(elem2)
	}

	if len(children) > 0 {
		PostError("extra rubbish after '...' in array")
		return P_FAIL
	}
	if new_array.Len() != ty.elems {
		PostError("wrong number of array elements: wanted %d, got %d",
			ty.elems, new_array.Len())
		return P_FAIL
	}

	return new_array
}

func (vdef *VarDef) StringToArray(new_array *Node, ty *Type, s string) error {
	char_type := ty.sub

	// Note: we disallow the signed types (s8, s16, s32) here.
	// I don't think it makes any sense to allow them.

	if char_type.kind != TYP_Int || !char_type.unsigned {
		PostError("cannot use strings in an array of %s", char_type.String())
		return FAILED
	}

	switch char_type.bits {
	case 8:
		return vdef.StringToArray_UTF8(new_array, s)

	case 16:
		return vdef.StringToArray_UTF16(new_array, s)

	case 32:
		return vdef.StringToArray_UTF32(new_array, s)

	default:
		PostError("cannot use strings in an array of %s", char_type.String())
		return FAILED
	}
}

func (vdef *VarDef) StringToArray_UTF8(new_array *Node, s string) error {
	// this is easy since in Go strings are already UTF-8
	for i := 0; i < len(s); i++ {
		var ch uint8 = s[i]
		char_str := strconv.Itoa(int(ch))
		t_lit := NewNode(NL_Integer, char_str, new_array.pos)
		new_array.Add(t_lit)
	}
	return OK
}

func (vdef *VarDef) StringToArray_UTF16(new_array *Node, s string) error {
	// produce error if string is not valid UTF-8
	// [ which can occur when using octal/hex escapes ]
	if !utf8.ValidString(s) {
		PostError("string is not valid UTF-8")
		return FAILED
	}

	r := []rune(s)

	// the only tricky part here is to convert a large character
	// value to the surrogate pairs.

	for _, ch := range r {
		var word int64 = int64(ch)

		if word > 0xFFFF {
			word -= 0x10000

			high := 0xD800 + ((word >> 10) & 0x03FF)
			word  = 0xDC00 + ( word        & 0x03FF)

			char_str := "0x" + strconv.FormatInt(high, 16)
			t_lit := NewNode(NL_Integer, char_str, new_array.pos)
			new_array.Add(t_lit)
		}

		char_str := "0x" + strconv.FormatInt(word, 16)
		t_lit := NewNode(NL_Integer, char_str, new_array.pos)
		new_array.Add(t_lit)
	}
	return OK
}

func (vdef *VarDef) StringToArray_UTF32(new_array *Node, s string) error {
	// produce error if string is not valid UTF-8
	// [ which can occur when using octal/hex escapes ]
	if !utf8.ValidString(s) {
		PostError("string is not valid UTF-8")
		return FAILED
	}

	r := []rune(s)

	for _, ch := range r {
		char_str := "0x" + strconv.FormatInt(int64(ch), 16)
		t_lit := NewNode(NL_Integer, char_str, new_array.pos)
		new_array.Add(t_lit)
	}
	return OK
}

func (vdef *VarDef) FillArray(new_array *Node, ty *Type) error {
	want_len := ty.elems

	for new_array.Len() < want_len {
		// for compound types, insert a NL_Zeroes token and the back-end
		// will take care to fill with zeros.
		switch ty.sub.kind {
		case TYP_Array, TYP_Struct, TYP_Union:
			t := NewNode(NL_Zeroes, "", new_array.pos)
			new_array.Add(t)

		default:
			t := vdef.DefaultValue(ty.sub, new_array.pos)
			if t == P_FAIL {
				PostError("cannot use '...' in array of %s", ty.sub.SimpleName())
				return FAILED
			}
			new_array.Add(t)
		}
	}

	return OK
}

func (vdef *VarDef) ParseStruct(t *Node, ty *Type) *Node {
	if t.kind != NG_Block {
		PostError("expected struct data, got: %s", t.String())
		return P_FAIL
	}

	new_struct := NewNode(NG_Block, "", t.pos)

	t = t.CompressBlock()
	children := t.children

	for _, field := range ty.param {
		// when a field name is present, it must match the struct
		if len(children) > 0 && children[0].IsField() {
			t_name := children[0]
			if t_name.str != field.name {
				PostError("expected struct field %s, got %s", field.name, t_name.str)
				return P_FAIL
			}
			children = children[1:]
		}

		if len(children) == 0 {
			PostError("missing element for %s in struct", field.name)
			return P_FAIL
		}

		elem := children[0]
		children = children[1:]

		// handle `...` which fills remaining values with a default
		if elem.Match("...") {
			if len(children) > 0 {
				PostError("extra rubbish after '...' in struct")
				return nil
			}

			if vdef.FillStruct(new_struct, ty) != OK {
				return P_FAIL
			}
			break
		}

		elem2 := vdef.ParseValue(elem, field.ty)
		if elem2 == P_FAIL {
			return P_FAIL
		}
		new_struct.Add(elem2)
	}

	if len(children) > 0 {
		PostError("too many elements for struct, got: %s", children[0].String())
		return P_FAIL
	}

	return new_struct
}

func (vdef *VarDef) FillStruct(new_struct *Node, ty *Type) error {
	want_len := len(ty.param)

	for new_struct.Len() < want_len {
		field_idx  := new_struct.Len()
		field_name := ty.param[field_idx].name
		field_type := ty.param[field_idx].ty

		// for compound types, insert a NL_Zeroes token and the back-end
		// will take care to fill with zeros.
		switch field_type.kind {
		case TYP_Array, TYP_Struct, TYP_Union:
			t := NewNode(NL_Zeroes, "", new_struct.pos)
			new_struct.Add(t)

		default:
			t := vdef.DefaultValue(field_type, new_struct.pos)
			if t == P_FAIL {
				PostError("cannot use '...' for struct field %s", field_name)
				return FAILED
			}
			new_struct.Add(t)
		}
	}

	return OK
}

func (vdef *VarDef) ParseUnion(t *Node, ty *Type) *Node {
	if t.kind != NG_Block {
		PostError("expected union data, got: %s", t.String())
		return P_FAIL
	}

	t = t.CompressBlock()

	if t.Len() < 2 || !t.children[0].IsField() {
		PostError("union data is missing field name or value")
		return P_FAIL
	}
	if t.Len() > 2 {
		PostError("extra rubbish after union value")
		return P_FAIL
	}

	t_name := t.children[0]

	new_union := NewNode(ND_Union, "", t.pos)

	for _, field := range ty.param {
		if field.name == t_name.str {
			t_value := vdef.ParseValue(t.children[1], field.ty)
			if t_value == P_FAIL {
				return P_FAIL
			}

			new_union.str = field.name
			new_union.Add(t_value)
			return new_union
		}
	}

	PostError("no such field %s in union type", t_name.str)
	return P_FAIL
}

func (vdef *VarDef) DefaultValue(ty *Type, pos Position) *Node {
	switch ty.kind {
	case TYP_Int:
		t_lit := NewNode(NL_Integer, "0", pos)
		return t_lit

	case TYP_Float:
		t_lit := NewNode(NL_Float, "0.0", pos)
		return t_lit

	case TYP_Pointer:
		t_lit := NewNode(NL_Null, "", pos)
		t_lit.ty = NewPointerType(rawmem_type)
		return t_lit

	case TYP_Bool:
		t_lit := NewNode(NL_Bool, "0", pos)
		t_lit.ty = bool_type
		return t_lit

	default:
		// parent must show an appropriate error!
		return P_FAIL
	}
}

func (t *Node) CompressBlock() *Node {
	res := NewNode(NG_Block, "", t.pos)
	for _, line := range t.children {
		for _, child := range line.children {
			res.Add(child)
		}
	}
	return res
}

//----------------------------------------------------------------------

func CheckNumericValue(t *Node, ty *Type) error {
	// see if the literal fits into the target type

	switch t.kind {
	case NL_Integer, NL_Char:
		// NOTE WELL: we allow some leeway here, the value needs to
		// fit into either a signed or unsigned version of the type.
		// for example, values between -128 to 255 are accepted for
		// both s8 and u8, but not anything outside of that range.

		v1, err1 := strconv.ParseInt (t.str, 0, ty.bits)
		v2, err2 := strconv.ParseUint(t.str, 0, ty.bits)

		if err1 != nil && err2 != nil {
			sign := 's'
			if ty.unsigned { sign = 'u' }
			PostError("value is too large for a %c%d", sign, ty.bits)
			return FAILED
		}

		// if value is outside the natural range (e.g. 255 for s8),
		// then adjust the literal to make it natural.
		// [ even though NASM does not care, other backends might ]

		// WISH: if literal was hexadecimal, make new version hex too

		if err1 != nil && !ty.unsigned {
			var new_val int64
			switch ty.bits {
			case 8:  new_val = int64(int8 (v2))
			case 16: new_val = int64(int16(v2))
			case 32: new_val = int64(int32(v2))
			case 64: new_val = int64(      v2 )
			}
			t.str = strconv.FormatInt(new_val, 10)

		} else if err2 != nil && ty.unsigned {
			var new_val uint64
			switch ty.bits {
			case 8:  new_val = uint64(uint8 (v1))
			case 16: new_val = uint64(uint16(v1))
			case 32: new_val = uint64(uint32(v1))
			case 64: new_val = uint64(       v1 )
			}
			t.str = strconv.FormatUint(new_val, 10)
		}

	case NL_Float:
		var err error
		if ty.bits == 32 {
			_, err = strconv.ParseFloat(t.str, 32)
		} else {
			_, err = strconv.ParseFloat(t.str, 64)
		}
		if err != nil {
			PostError("float value is too large for a f%d", ty.bits)
			return FAILED
		}
	}

	return OK
}

// IntegerFitting returns the smallest bit-size that the given
// literal value requires.  result will be 8, 16, 32 or 64.
// the result depends on the signed-ness, e.g. `255` only needs
// 8 bits for unsigned but 16-bits for signed.
func IntegerFitting(t *Node, unsigned bool) int {
	var err error

	if unsigned {
		_, err = strconv.ParseUint(t.str, 0, 8)
		if err == nil { return 8 }

		_, err = strconv.ParseUint(t.str, 0, 16)
		if err == nil { return 16 }

		_, err = strconv.ParseUint(t.str, 0, 32)
		if err == nil { return 32 }

	} else {
		_, err = strconv.ParseInt(t.str, 0, 8)
		if err == nil { return 8 }

		_, err = strconv.ParseInt(t.str, 0, 16)
		if err == nil { return 16 }

		_, err = strconv.ParseInt(t.str, 0, 32)
		if err == nil { return 32 }
	}

	return 64
}

func IntegerForceSize(t *Node, bits int) error {
	// we need to handle full range of s64 *and* u64, so parse twice
	v1, err1 := strconv.ParseInt (t.str, 0, 64)
	v2, err2 := strconv.ParseUint(t.str, 0, 64)

	if err1 != nil && err2 != nil {
		PostError("integer literal is too large")
		return FAILED
	}

	// WISH: if literal was hexadecimal, make new version hex too

	var new_val int64

	switch bits {
	case 8:
		if err1 != nil {
			new_val = int64(uint8(v2))
		} else if v1 < -128 || v1 > 255 {
			new_val = int64(uint8(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 16:
		if err1 != nil {
			new_val = int64(uint16(v2))
		} else if v1 < -0x8000 || v1 > 0xFFFF {
			new_val = int64(uint16(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 32:
		if err1 != nil {
			new_val = int64(uint32(v2))
		} else if v1 < -0x80000000 || v1 > 0xFFFFFFFF {
			new_val = int64(uint32(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 64:
		// leave it as-is
	}

	return OK
}

func IntegerToBool(t *Node) error {
	// negative or zero becomes FALSE.
	// everything else becomes TRUE.

	t.kind = NL_Bool
	t.ty   = bool_type

	if t.str[0] != '-' {
		v, err := strconv.ParseUint(t.str, 0, 64)
		if err != nil || v > 0 {
			t.str = "1"
			return OK
		}
	}

	t.str = "0"
	return OK
}

func IntegerToFloat(t *Node) error {
	// convert hex to decimal
	if ( len(t.str) >= 2 && t.str[1] == 'x') ||
		(len(t.str) >= 3 && t.str[2] == 'x') {

		if t.str[0] == '-' {
			v, err := strconv.ParseInt(t.str, 0, 64)
			if err != nil {
				PostError("integer literal is too large")
				return FAILED
			}
			t.str = strconv.FormatInt(v, 10)

		} else {
			v, err := strconv.ParseUint(t.str, 0, 64)
			if err != nil {
				PostError("integer literal is too large")
				return FAILED
			}
			t.str = strconv.FormatUint(v, 10)
		}
	}

	t.kind = NL_Float
	t.str  = t.str + ".0"

	return OK
}

func FloatToInteger(t *Node) error {
	val, err := strconv.ParseFloat(t.str, 64)
	if err != nil {
		PostError("float value is too large")
		return FAILED
	}

	// check the value is in range for a s64.  this may exclude a few
	// values at the limits, but that is better than allowing values
	// *past* the limit and getting a bogus result.
	const limit = 9.223372036854775e+18

	if val < -limit || val > limit {
		PostError("float value is too large")
		return FAILED
	}

	new_val := int64(val)

	t.kind = NL_Integer
	t.str  = strconv.FormatInt(new_val, 10)

	return OK
}

func IntegerScaleToShift(scale int) int {
	// returns -1 when scale is not a power of two.

	if scale <= 0 {
		panic("bad scale for IntegerScaleToShift")
	}

	res := 0

	for {
		if scale == 1 {
			return res
		}
		if (scale & 1) == 1 {
			return -1
		}
		scale = scale >> 1
		res   = res + 1
	}
}

func InfinityOrNan(n float64) bool {
	return math.IsInf(n, 0) || math.IsNaN(n)
}

func IsLiteralZero(t *Node) bool {
	switch t.kind {
	case NL_Integer, NL_Char:
		return t.str == "0" || t.str == "0x0"
	case NL_Float:
		return t.str == "0.0"
	}
	return false
}

func IsLiteralOne(t *Node) bool {
	switch t.kind {
	case NL_Integer, NL_Char:
		return t.str == "1" || t.str == "0x1"
	}
	return false
}

func IsLiteralFalse(t *Node) bool {
	return t.kind == NL_Bool && t.str == "0"
}

func IsLiteralTrue(t *Node) bool {
	return t.kind == NL_Bool && t.str == "1"
}
