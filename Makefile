#
#  --- ROWAN ---
#
#  Requires GNU make.
#

PROGRAM=rowan-c

all: $(PROGRAM)

clean:
	rm -f $(PROGRAM)

$(PROGRAM): *.go
	go build

.PHONY: all clean

# ------ Tiny Runtime --------------------------

CFLAGS=-O0 -g1 -Wall -Wextra --std=c99

TINYRT_OBJ=_tinyrt/tinyrt.o

runtime: $(TINYRT_OBJ)

runtime-clean:
	rm -f $(TINYRT_OBJ)

$(TINYRT_OBJ): _tinyrt/*.c
	$(CC) $(CFLAGS) -c _tinyrt/tinyrt.c -o $@

.PHONY: runtime runtime-clean

# ------ Sample Programs  ----------------------

samples: \
	samples/hello  \
	samples/sum    \
	samples/input  \
	samples/fibonacci  \
	samples/mandelbrot

# TODO samples/adventure samples/life

samples/adventure: samples/adventure.rx runtime
	./rowan-c samples/adventure.rx
	$(CC) $(CFLAGS) samples/adventure.c $(TINYRT_OBJ) -o $@

samples/fibonacci: samples/fibonacci.rx runtime
	./rowan-c samples/fibonacci.rx
	$(CC) $(CFLAGS) samples/fibonacci.c $(TINYRT_OBJ) -o $@

samples/hello: samples/hello.rx runtime
	./rowan-c samples/hello.rx
	$(CC) $(CFLAGS) samples/hello.c $(TINYRT_OBJ) -o $@

samples/input: samples/input.rx runtime
	./rowan-c samples/input.rx
	$(CC) $(CFLAGS) samples/input.c $(TINYRT_OBJ) -o $@

samples/life: samples/life.rx runtime
	./rowan-c samples/life.rx
	$(CC) $(CFLAGS) samples/life.c $(TINYRT_OBJ) -o $@

samples/mandelbrot: samples/mandelbrot.rx runtime
	./rowan-c samples/mandelbrot.rx
	$(CC) $(CFLAGS) samples/mandelbrot.c $(TINYRT_OBJ) -o $@

samples/sum: samples/sum.rx runtime
	./rowan-c samples/sum.rx
	$(CC) $(CFLAGS) samples/sum.c $(TINYRT_OBJ) -o $@

samples-clean:
	rm -f samples/*.[co]
	rm -f samples/adventure
	rm -f samples/fibonacci
	rm -f samples/hello
	rm -f samples/input
	rm -f samples/life
	rm -f samples/mandelbrot
	rm -f samples/sum

.PHONY: samples samples-clean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
