// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"
import "strings"
// import "fmt"
// import "unicode"

func (gen *Generator) BuildAllOutVars() {
	for _, def := range cur_mod.ord_defs {
		if def.kind == DEF_Var {
			out_name := gen.EncodeName(def.d_var.name)
			def.d_var._out = NewOutThing(ODEF_Var, out_name)
		}
	}
}

func (gen *Generator) WriteFwdFuncs() {
	// writes the forward declarations of functions used in
	// variable initializers.

	fwds := make(map[*OutThing]bool)

	for out, _ := range gen.things {
		if out.kind == ODEF_Var {
			for dep, _ := range out.depends {
				if dep.kind == ODEF_Func {
					fwds[dep] = true
				}
			}
		}
	}

	if len(fwds) == 0 {
		return
	}

	OutLine("")
	OutLine("// ---- forward funcs ----")
	OutLine("")

	for len(fwds) > 0 {
		out := gen.PickNextThing(fwds, nil)
		out.WriteForward()
		delete(fwds, out)
	}

	OutLine("")
}

//----------------------------------------------------------------------

const DATA_MAX_LINE   = 64
const DATA_MAX_INLINE = 96
const DATA_MAX_STRING = 96

func (gen *Generator) CompileVar(vdef *VarDef) {
	out := vdef._out

	type_name := gen.ConvertType(vdef.ty, true, nil)
	full_def  := gen.FormVarDefinition(type_name, out.name)

	gen.AddThing(out)

	// create a forward decl
	full_def = gen.VarQualifier(vdef) + full_def
	out.forward = full_def + ";"

	if vdef.extern || vdef.zeroed {
		// the forward decl is enough
		out.required = true
		return
	}

	// convert the data / value

	ty := vdef.ty

	switch ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		val_str := gen.SimpleDataValue(vdef.expr, ty, out)
		out.Line("%s = %s;", full_def, val_str)
		return
	}

	out.Token(full_def)
	out.Token(" = ")

	gen.CompileData(vdef.expr, ty, len(full_def) + 4, out)

	out.Token(";")
	out.FlushLine(0)
}

func (gen *Generator) VarQualifier(vdef *VarDef) string {
	if vdef.extern {
		return "extern"
	}
	qual := ""

	if !vdef.public {
		qual = "static "
	}
	if vdef.zeroed {
		// nothing needed
	} else if vdef.rom {
		qual = qual + "const "
	}

	return qual
}

func (gen *Generator) CompileData(data *Node, ty *Type, left_len int, out *OutThing) {
	est := gen.EstimateInline(data, ty, 0)
	if est > 0 {
		est = est + left_len
	}
	if est > 0 && est < DATA_MAX_INLINE {
		s := gen.InlineData(data, ty, out)
		out.Token(s)
		return
	}

	switch ty.kind {
	case TYP_Array:
		gen.Data_Array(data, ty, out)

	case TYP_Struct:
		gen.Data_Struct(data, ty, out)

	case TYP_Union:
		gen.Data_Union(data, ty, out)

	default:
		panic("weird data: " + ty.SimpleName())
	}
}

func (gen *Generator) InlineData(data *Node, ty *Type, out *OutThing) string {
	switch ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		return gen.SimpleDataValue(data, ty, out)
	}

	switch ty.kind {
	case TYP_Array:
		return gen.Inline_Array(data, ty, out)

	case TYP_Struct:
		return gen.Inline_Struct(data, ty, out)

	case TYP_Union:
		return gen.Inline_Union(data, ty, out)

	default:
		panic("weird data: " + ty.SimpleName())
	}
}

func (gen *Generator) Data_Array(data *Node, ty *Type, out *OutThing) {
	out.BeginBlock()

	elem_type := ty.sub

	if data.kind == NL_Zeroes {
		for i := 0; i < ty.elems; i++ {
			if i > 0 {
				out.Token(",")
				out.FlushLine(DATA_MAX_LINE)
			}
			gen.CompileData(data, elem_type, 0, out)
		}
	} else {
		for i, elem := range data.children {
			if i > 0 {
				out.Token(",")

				// force newlines for complex elements (looks nicer)
				if elem_type.IsStructural() {
					out.FlushLine(0)
				} else {
					out.FlushLine(DATA_MAX_LINE)
				}
			}
			gen.CompileData(elem, elem_type, 0, out)
		}
	}

	out.EndBlock()
}

func (gen *Generator) Inline_Array(data *Node, ty *Type, out *OutThing) string {
	var sb strings.Builder

	sb.WriteString("{ ")

	elem_type := ty.sub

	if data.kind == NL_Zeroes {
		s := gen.InlineData(data, elem_type, out)

		for i := 0; i < ty.elems; i++ {
			if i > 0 {
				sb.WriteString(",")
			}
			sb.WriteString(s)
		}
	} else {
		for i, elem := range data.children {
			if i > 0 {
				sb.WriteString(",")
			}
			s := gen.InlineData(elem, elem_type, out)
			sb.WriteString(s)
		}
	}

	sb.WriteString(" }")

	return sb.String()
}

func (gen *Generator) Data_Struct(data *Node, ty *Type, out *OutThing) {
	out.BeginBlock()

	if data.kind == NL_Zeroes {
		for i, field := range ty.param {
			if i > 0 {
				out.Token(",")
				out.FlushLine(0)
			}
			cmt := "/* " + field.name + " */ "
			out.Token(cmt)

			gen.CompileData(data, field.ty, 0, out)
		}
	} else {
		for i, elem := range data.children {
			if i > 0 {
				out.Token(",")
				out.FlushLine(0)
			}
			field_name := ty.param[i].name
			field_type := ty.param[i].ty

			cmt := "/* " + field_name + " */ "
			out.Token(cmt)

			gen.CompileData(elem, field_type, 0, out)
		}
	}

	out.EndBlock()
}

func (gen *Generator) Inline_Struct(data *Node, ty *Type, out *OutThing) string {
	var sb strings.Builder

	sb.WriteString("{ ")

	if data.kind == NL_Zeroes {
		for i, field := range ty.param {
			if i > 0 {
				sb.WriteString(",")
			}
			s := gen.InlineData(data, field.ty, out)
			sb.WriteString(s)
		}
	} else {
		for i, elem := range data.children {
			if i > 0 {
				sb.WriteString(",")
			}
			field_type := ty.param[i].ty
			s := gen.InlineData(elem, field_type, out)
			sb.WriteString(s)
		}
	}

	sb.WriteString(" }")

	return sb.String()
}

func (gen *Generator) Data_Union(data *Node, ty *Type, out *OutThing) {
	var field_idx int

	if data.kind == NL_Zeroes {
		field_idx = ty.LargestField()
	} else {
		// node kind is a ND_Union with a single data element
		field_idx := ty.FindParam(data.str)

		if field_idx < 0 {
			panic("union field not found")
		}
		data = data.children[0]
	}

	field_name := ty.param[field_idx].name
	field_type := ty.param[field_idx].ty

	out.BeginBlock()

	out.Token(field_name)
	out.Token(" = ")

	gen.CompileData(data, field_type, len(field_name) + 3, out)

	out.EndBlock()
}

func (gen *Generator) Inline_Union(data *Node, ty *Type, out *OutThing) string {
	var field_idx int

	if data.kind == NL_Zeroes {
		field_idx = ty.LargestField()
	} else {
		// node kind is a ND_Union with a single data element
		field_idx := ty.FindParam(data.str)

		if field_idx < 0 {
			panic("union field not found")
		}
		data = data.children[0]
	}

	field_name := ty.param[field_idx].name
	field_type := ty.param[field_idx].ty

	s := gen.InlineData(data, field_type, out)

	return "{ " + field_name + " = " + s + " }"
}

func (gen *Generator) EstimateInline(data *Node, ty *Type, depth int) int {
	// esimates how many chars would be required for the data,
	// especially a struct with all elements placed "in line".
	// returns -1 if nesting is too deep, or type is not handled.

	switch data.kind {
	case NL_Null, NL_FltSpec, NL_Char, NL_Bool:
		return 7

	case NL_Integer, NL_Float:
		return len(data.str)

	case NL_String:
		if data.ty == nil {
			panic("string with no type")
		}

		if data.ty.sub.bits == 8 {
			if len(data.str) <= DATA_MAX_STRING {
				return 10 + len(data.str)
			}
		}
		return 15

	case NL_Zeroes:
		switch ty.kind {
		case TYP_Int:     return 1
		case TYP_Float:   return 3
		case TYP_Pointer: return 7
		case TYP_Bool:    return 8
		}

	case NP_GlobPtr:
		return 20
	}

	depth = depth + 1
	if depth > 4 {
		return -1
	}

	switch ty.kind {
	case TYP_Array:
		return gen.Estimate_Array(data, ty, depth)

	case TYP_Struct:
		return gen.Estimate_Struct(data, ty, depth)

	case TYP_Union:
		return gen.Estimate_Union(data, ty, depth)
	}

	// unknown type??
	return -1
}

func (gen *Generator) Estimate_Array(data *Node, ty *Type, depth int) int {
	// if element type is complex, force a line-by-line output.
	// [ this just looks nicer, in my opinion ]
	elem_type := ty.sub

	if elem_type.IsStructural() {
		return -1
	}

	length := 4  // curly brackets + spaces

	if data.kind == NL_Zeroes {
		est := gen.EstimateInline(data, elem_type, depth)
		if est < 0 { return -1 }
		// add one to account for commas
		length += ty.elems * (est + 1)

	} else {
		for _, elem := range data.children {
			est := gen.EstimateInline(elem, elem_type, depth)
			if est < 0 { return -1 }
			// add one to account for commas
			length += est + 1
		}
	}
	return length
}

func (gen *Generator) Estimate_Struct(data *Node, ty *Type, depth int) int {
	length := 4  // curly brackets + spaces

	if data.kind == NL_Zeroes {
		for i, field := range ty.param {
			if i > 0 {
				length += 1
			}
			est := gen.EstimateInline(data, field.ty, depth)
			if est < 0 { return -1 }
			length = length + est
		}
	} else {
		for i, child := range data.children {
			if i > 0 {
				length += 1
			}
			child_type := ty.param[i].ty
			est := gen.EstimateInline(child, child_type, depth)
			if est < 0 { return -1 }
			length = length + est
		}
	}
	return length
}

func (gen *Generator) Estimate_Union(data *Node, ty *Type, depth int) int {
	var field_idx int

	if data.kind == NL_Zeroes {
		field_idx = ty.LargestField()
	} else {
		// node kind is a ND_Union with a single data element
		field_idx := ty.FindParam(data.str)

		if field_idx < 0 {
			panic("union field not found")
		}
		data = data.children[0]
	}

	field_name := ty.param[field_idx].name
	field_type := ty.param[field_idx].ty

	est := gen.EstimateInline(data, field_type, depth + 1)
	if est < 0 { return - 1 }

	return 2 + len(field_name) + 3 + est + 2
}

func (gen *Generator) SimpleDataValue(elem *Node, ty *Type, parent *OutThing) string {
	switch elem.kind {
	case NL_Zeroes:
		return gen.ZeroDataValue(ty)

	case NL_Null:
		return "_rwNULL"

	case NL_Integer, NL_Float:
		return elem.str

	case NL_Char:
		return gen.CharDataValue(elem, ty)

	case NL_Bool:
		if elem.str == "1" {
			return "_rwTRUE"
		} else {
			return "_rwFALSE"
		}

	case NL_FltSpec:
		gen.NeedHeader("<math.h>")

		switch elem.str {
		case "+INF": return "INFINITY"
		case "-INF": return "-INFINITY"
		case "QNAN": return "NAN"
		case "SNAN": return "NAN"  // best we can do?
		default:
			panic("unknown NL_FltSpec literal")
		}

	case NL_String:
		if elem.ty == nil {
			panic("string with no type")
		}

		// use a standard C string if short enough
		if elem.ty.sub.bits == 8 {
			if len(elem.str) <= 128 {
				return "(uint8_t *)" + gen.ConvertStringUTF8(elem.str)
			}
		}

		switch elem.ty.sub.bits {
		case 8:  return gen.AddStringUTF8 (elem.str)
		case 16: return gen.AddStringUTF16(elem.str)
		case 32: return gen.AddStringUTF32(elem.str)
		default:
			panic("string with odd type (expected ^u8 or so)")
		}

	case NP_CSize:
		type_name := gen.ConvertType(elem.def.d_type.ty, false, nil)
		return "sizeof(" + type_name + ")"

	case NP_GlobPtr:
		if parent != nil {
			def := elem.def
			switch def.kind {
			case DEF_Var:  parent.AddDependency(def.d_var ._out)
			case DEF_Func: parent.AddDependency(def.d_func._out)
			}
		}
		out_name := gen.EncodeFullName(elem.str, elem.module)
		return "&" + out_name

	default:
		panic("odd node to SimpleDataValue: " + elem.String())
	}
}

func (gen *Generator) ZeroDataValue(ty *Type) string {
	switch ty.kind {
	case TYP_Int:     return "0"
	case TYP_Float:   return "0.0"
	case TYP_Pointer: return "_rwNULL"
	case TYP_Bool:    return "_rwFALSE"

	default:
		panic("odd type to ZeroDataValue: " + ty.SimpleName())
	}
}

func (gen *Generator) CharDataValue(elem *Node, ty *Type) string {
	value, err := strconv.ParseInt(elem.str, 0, 64)

	if err != nil || value < 0 {
		return elem.str
	}
	if value == '\'' {
		return elem.str
	}
	return gen.EncodeStringRune(rune(value), false)
}
