// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"

type Operator struct {
	name   string
	prec   int
	unary  map[BaseType]string
	binary map[BaseType]string

	comparison bool
	reversible bool
}

var operators map[string]*Operator

func InitOperators() {
	operators = make(map[string]*Operator)

	/* binary ops (must be first) */

	RegisterOperator("or",  1, TYP_Bool, "|", false)
	RegisterOperator("and", 2, TYP_Bool, "&", false)

	RegisterOperator("==", 3, TYP_Int, "", true)
	RegisterOperator("!=", 3, TYP_Int, "", true)
	RegisterOperator("<",  3, TYP_Int, "", true)
	RegisterOperator("<=", 3, TYP_Int, "", true)
	RegisterOperator(">",  3, TYP_Int, "", true)
	RegisterOperator(">=", 3, TYP_Int, "", true)

	RegisterOperator("==", 3, TYP_Pointer, "", true)
	RegisterOperator("!=", 3, TYP_Pointer, "", true)
	RegisterOperator("<",  3, TYP_Pointer, "", true)
	RegisterOperator("<=", 3, TYP_Pointer, "", true)
	RegisterOperator(">",  3, TYP_Pointer, "", true)
	RegisterOperator(">=", 3, TYP_Pointer, "", true)

	RegisterOperator("==", 3, TYP_Float, "", true)
	RegisterOperator("!=", 3, TYP_Float, "", true)
	RegisterOperator("<",  3, TYP_Float, "", true)
	RegisterOperator("<=", 3, TYP_Float, "", true)
	RegisterOperator(">",  3, TYP_Float, "", true)
	RegisterOperator(">=", 3, TYP_Float, "", true)

	RegisterOperator("==", 3, TYP_Bool, "", true)
	RegisterOperator("!=", 3, TYP_Bool, "", true)
	RegisterOperator("<",  3, TYP_Bool, "", true)
	RegisterOperator("<=", 3, TYP_Bool, "", true)
	RegisterOperator(">",  3, TYP_Bool, "", true)
	RegisterOperator(">=", 3, TYP_Bool, "", true)

	RegisterOperator("+", 4, TYP_Int, "", false)
	RegisterOperator("-", 4, TYP_Int, "", false)
	RegisterOperator("*", 5, TYP_Int, "", false)
	RegisterOperator("/", 5, TYP_Int, "", false)
	RegisterOperator("%", 5, TYP_Int, "", false)

	RegisterOperator("|",  4, TYP_Int, "",  false)
	RegisterOperator("~",  4, TYP_Int, "^", false)
	RegisterOperator("&",  5, TYP_Int, "",  false)
	RegisterOperator("<<", 6, TYP_Int, "",  false)
	RegisterOperator(">>", 6, TYP_Int, "",  false)

	RegisterOperator("+", 4, TYP_Float, "",  false)
	RegisterOperator("-", 4, TYP_Float, "",  false)
	RegisterOperator("*", 5, TYP_Float, "",  false)
	RegisterOperator("/", 5, TYP_Float, "",  false)
	RegisterOperator("%", 5, TYP_Float, "fremt", false)

	RegisterOperator("+", 4, TYP_Pointer, "", false)
	RegisterOperator("-", 4, TYP_Pointer, "", false)

	/* unary ops */

	RegisterOperator("~", 0, TYP_Int,    "",  false)
	RegisterOperator("-", 0, TYP_Int,    "",  false)
	RegisterOperator("-", 0, TYP_Float,  "",  false)
	RegisterOperator("not", 0, TYP_Bool, "!", false)

	RegisterOperator("abs",   0, TYP_Int,   "iabs",  false)
	RegisterOperator("abs",   0, TYP_Float, "fabs",  false)
	RegisterOperator("sqrt",  0, TYP_Float, "fsqrt", false)

	RegisterOperator("round", 0, TYP_Float, "fround", false)
	RegisterOperator("trunc", 0, TYP_Float, "ftrunc", false)
	RegisterOperator("floor", 0, TYP_Float, "ffloor", false)
	RegisterOperator("ceil",  0, TYP_Float, "fceil",  false)

	RegisterOperator("zero?", 0, TYP_Int, "0 ==", true)
	RegisterOperator("some?", 0, TYP_Int, "0 !=", true)
	RegisterOperator("neg?",  0, TYP_Int, "0 >",  true)
	RegisterOperator("pos?",  0, TYP_Int, "0 <",  true)

	RegisterOperator("zero?", 0, TYP_Float, "0 ==", true)
	RegisterOperator("some?", 0, TYP_Float, "0 !=", true)
	RegisterOperator("neg?",  0, TYP_Float, "0 >",  true)
	RegisterOperator("pos?",  0, TYP_Float, "0 <",  true)

	RegisterOperator("inf?", 0, TYP_Float, "finf?", true)
	RegisterOperator("nan?", 0, TYP_Float, "fnan?", true)

	RegisterOperator("null?", 0, TYP_Pointer, "_rwNULL ==", true)
	RegisterOperator("ref?",  0, TYP_Pointer, "_rwNULL !=", true)

	/* mark the commutitive ops */

	operators["+"].reversible = true
	operators["*"].reversible = true
	operators["&"].reversible = true
	operators["|"].reversible = true
	operators["~"].reversible = true

	operators["=="].reversible = true
	operators["!="].reversible = true
}

func RegisterOperator(op string, prec int, kind BaseType, bu string, cmp bool) {
	if bu == "" {
		bu = op
	}

	info := operators[op]

	if info == nil {
		info = new(Operator)

		info.name   = op
		info.prec   = prec
		info.unary  = make(map[BaseType]string)
		info.binary = make(map[BaseType]string)
		info.comparison = cmp

		operators[op] = info
	} else {
		if prec != 0 && prec != info.prec {
			panic("different precedences for " + op)
		}
	}

	if prec == 0 {
		info.unary[kind] = bu
	} else {
		info.binary[kind] = bu
	}
}

func IsBinaryOperator(t *Node) bool {
	if t.kind != NL_Name {
		return false
	}
	op := operators[t.str]
	if op == nil {
		return false
	}
	return len(op.binary) > 0
}

func IsOperator(t *Node) bool {
	if t.kind == NL_Name {
		return IsOperatorName(t.str)
	}
	return false
}

func IsOperatorName(op string) bool {
	_, ok := operators[op]
	return ok
}
