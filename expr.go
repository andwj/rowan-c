// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "math"
import "strconv"
import "strings"

type EvalContext struct {
	// this is nil if outside a function (i.e. a constant expression)
	fu *FuncDef
}

func (ctx *EvalContext) ProcessExpression(children []*Node) *Node {
	// handle "terms" which consist of multiple tokens.
	// terms are separated by *binary* operators (and will never
	// begin with a binary operator).  a function name which is just
	// an identifier (no parameters) must be considered a term too.

	// this will become: TERM { bin-op TERM }.
	expr := make([]*Node, 0)
	num_ops := 0

	for len(children) > 0 {
		// find the next binary operator.
		// [ first element should never be one ]
		pos := 1

		for pos < len(children) {
			if IsBinaryOperator(children[pos]) {
				break
			}
			pos += 1
		}

		term := ctx.ProcessTerm(children[0:pos])
		children = children[pos:]

		if term == P_FAIL || term == P_UNKNOWN {
			return term
		}

		expr = append(expr, term)

		// do we have an operator?
		if len(children) > 0 {
			expr = append(expr, children[0])
			children = children[1:]
			num_ops += 1
		}
	}

	if num_ops == 0 {
		return expr[0]
	}

	node := ctx.EvaluateExpression(expr)
	return node
}

func (ctx *EvalContext) ProcessTerm(children []*Node) *Node {
	term := children[0]

	if term.kind == NL_Name || term.kind == NL_Symbol {
		if ctx.fu != nil && ctx.fu.cur_block.SeenLocal(term.str) {
			// locals always shadow a global
		} else {
			if !ValidateModUsage(term.module) {
				return P_FAIL
			}
			ReplaceAlias(term)

			// a unary operator?
			op := operators[term.str]

			if op != nil {
				if len(op.unary) == 0 {
					PostError("bad expression: unexpected %s operator", term.str)
					return P_FAIL
				}
				if len(children) < 2 {
					PostError("bad expression: missing term after %s operator", term.str)
					return P_FAIL
				}
				term = ctx.ProcessTerm(children[1:])
				if term == P_FAIL || term == P_UNKNOWN {
					return term
				}
				return ctx.EvaluateUnary(op, term)
			}

			// a function name?
			def, mod := LookupDef(term.str, term.module)

			if def != nil && mod != cur_mod && !def.IsPublic() {
				PostError("%s is private to module %s", term.str, mod.name)
				return P_FAIL
			}
			if def != nil && def.kind == DEF_Func {
				if ctx.fu == nil {
					PostError("cannot call functions in const exprs")
					return P_FAIL
				}

				term = ctx.fu.OperandFunc(term, def)
				return ctx.Term_FunCall(term, children[1:])
			}

			// a type name?
			var ty *Type

			if def != nil && def.kind == DEF_Type {
				ty = def.d_type.ty
				children = children[1:]

			} else if term.Match("^") {
				var count int
				ty, count = ParseTypeSpec(children, "a literal value")
				if ty == nil {
					return P_FAIL
				}
				children = children[count:]

			} else {
				ty = LookupBuiltinType(term.str)
				if ty != nil {
					children = children[1:]
				}
			}

			if ty != nil {
				if ctx.fu == nil {
					PostError("cannot use types in const exprs")
					return P_FAIL
				}
				return ctx.Term_Conv(ty, children)
			}
		}
	}

	if ctx.fu != nil {
		/* in a function body */

		if term.IsMethod() {
			return ctx.Term_MethodCall(children)
		}

		args := children[1:]

		switch {
		case term.Match("call"):
			return ctx.Term_ExplicitCall(args)

		case term.Match("cast"):
			return ctx.Term_RawCast(args)

		case term.Match("mem-read"):
			return ctx.Term_MemRead(args)

		case term.Match("stack-var"):
			return ctx.Term_StackVar(args)

		case term.Match("matches?"):
			return ctx.Term_Matches(args)

		case term.Match("max"):
			return ctx.Term_MinMax(args, true)

		case term.Match("min"):
			return ctx.Term_MinMax(args, false)

		case term.Match("big-endian"):
			return ctx.Term_Endian(args, true)

		case term.Match("little-endian"):
			return ctx.Term_Endian(args, false)
		}
	}

	if term.IsMethod() {
		PostError("cannot call methods in const exprs")
		return P_FAIL
	}

	if len(children) > 1 {
		if term.kind == NL_Name {
			PostError("unknown function name: %s", term.str)
		} else {
			PostError("bad syntax in expression")
		}
		return P_FAIL
	}

	return ctx.EvalTerm(term)
}

func (ctx *EvalContext) Term_ExplicitCall(children []*Node) *Node {
	if len(children) < 1 {
		PostError("missing function for call statement")
		return P_FAIL
	}

	// method call?
	if children[0].IsMethod() {
		return ctx.Term_MethodCall(children)
	}

	t_func := ctx.EvalTerm(children[0])
	if t_func == P_FAIL {
		return P_FAIL
	}

	fun_type := t_func.ty
	if fun_type != nil && fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}
	if fun_type == nil || fun_type.kind != TYP_Function {
		PostError("call requires a function argument")
		return P_FAIL
	}

	t_func = ctx.fu.ShiftComputation(t_func)

	return ctx.Term_FunCall(t_func, children[1:])
}

func (ctx *EvalContext) Term_FunCall(t_func *Node, params []*Node) *Node {
	fun_type := t_func.ty
	if fun_type == nil {
		Dumpty("t_func:", t_func, 3)
		panic("no type in function node")
	}
	if fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}

	t_call := NewNode(NC_Call, "", t_func.pos)
	t_call.ty = fun_type.sub
	t_call.Add(t_func)

	if ctx.GrabParameters(t_call, nil, params, fun_type) != OK {
		return P_FAIL
	}
	return t_call
}

func (ctx *EvalContext) Term_MethodCall(children []*Node) *Node {
	if len(children) < 2 {
		PostError("missing receiver for method call")
		return P_FAIL
	}

	t_method := children[0]

	recv := ctx.GrabReceiver(children[1])
	if recv == P_FAIL {
		return P_FAIL
	}

	// catch attempt to call method on a NULL receiver
	ctx.fu.DoCheckNull(recv)

	belong_type := recv.ty.sub

	method := belong_type.FindMethod(t_method.str)
	if method == nil {
		PostError("unknown method '%s' in type %s",
			t_method.str, belong_type.def.name)
		return P_FAIL
	}

	t_func := NewNode(NP_Method, method.name, t_method.pos)
	t_func.ty = method.ty

	t_call := NewNode(NC_Call, "", t_method.pos)
	t_call.ty = method.ty.sub
	t_call.Add(t_func)

	// this includes the receiver, but it is only there tp simplify the
	// code in GrabParameters(), the receiver will not be parsed again.
	params := children[1:]

	if ctx.GrabParameters(t_call, recv, params, method.ty) != OK {
		return P_FAIL
	}
	return t_call
}

func (ctx *EvalContext) Term_Conv(conv_type *Type, children []*Node) *Node {
	// source and target types must either be integer or float.
	// conversions between int and float require signed integers.

	if len(children) == 0 {
		PostError("bad expression: missing term after type")
		return P_FAIL
	}

	L := ctx.ProcessTerm(children)
	if L == P_FAIL {
		return P_FAIL
	}

	if L.IsLiteral() || L.kind == NP_CSize {
		return ctx.DoTypedLiteral(L, conv_type)
	}

	// check target type makes sense
	switch conv_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool:
		// ok
	default:
		PostError("conversion target cannot be a %s", conv_type.SimpleName())
		return P_FAIL
	}

	src_type := L.ty
	if src_type == nil {
		panic("operand in Term_Conv has no type")
	}

	// check source type makes sense
	switch src_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool:
		// ok
	default:
		PostError("cannot convert from a %s", src_type.SimpleName())
		return P_FAIL
	}

	// check combination of source + dest types
	if (conv_type.kind == TYP_Float && src_type.kind == TYP_Int && src_type.unsigned) ||
		(src_type.kind == TYP_Float && conv_type.kind == TYP_Int && conv_type.unsigned) {
		PostError("cannot convert between float and unsigned int")
		return P_FAIL
	}
	if (conv_type.kind == TYP_Float && src_type.kind == TYP_Bool) ||
		(src_type.kind == TYP_Float && conv_type.kind == TYP_Bool) {
		PostError("cannot convert between float and boolean")
		return P_FAIL
	}

	return ctx.fu.DoConvert(L, conv_type)
}

func (ctx *EvalContext) DoTypedLiteral(lit *Node, conv_type *Type) *Node {
	// check target type makes sense
	switch conv_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool, TYP_Pointer:
		// ok
	default:
		PostError("a literal value cannot be a %s", conv_type.SimpleName())
		return P_FAIL
	}

	switch lit.kind {
	case NL_Integer, NL_Char:
		switch conv_type.kind {
		case TYP_Int:
			if IntegerForceSize(lit, conv_type.bits) != OK {
				return P_FAIL
			}
			lit.ty = conv_type
			return lit

		case TYP_Float:
			if IntegerToFloat(lit) != OK {
				return P_FAIL
			}
			lit.ty = conv_type
			return lit

		case TYP_Bool:
			if IntegerToBool(lit) != OK {
				return P_FAIL
			}
			lit.ty = conv_type
			return lit
		}

	case NL_Bool:
		switch conv_type.kind {
		case TYP_Bool:
			return lit

		case TYP_Int:
			lit.kind = NL_Integer
			lit.ty = conv_type
			return lit
		}

	case NL_Float:
		switch conv_type.kind {
		case TYP_Float:
			lit.ty = conv_type
			if ctx.fu.CheckLiteral(lit) != OK {
				return P_FAIL
			}
			return lit

		case TYP_Int:
			if conv_type.unsigned {
				PostError("cannot convert between float and unsigned int")
				return P_FAIL
			}
			if FloatToInteger(lit) != OK {
				return P_FAIL
			}
			if IntegerForceSize(lit, conv_type.bits) != OK {
				return P_FAIL
			}
			lit.ty = conv_type
			return lit
		}

	case NP_CSize:
		if conv_type.kind == TYP_Int {
			lit.ty = conv_type
			return lit
		}

	default:
		lit.ty = conv_type
		if ctx.fu.CheckLiteral(lit) != OK {
			return P_FAIL
		}
		return lit
	}

	PostError("incompatible type for literal: %s", conv_type.String())
	return P_FAIL
}

func (ctx *EvalContext) Term_RawCast(children []*Node) *Node {
	// source and target type must be one of: int, float, bool, ptr.
	// source and target must be different types.
	// source and target must be same size, no exceptions.

	if len(children) == 0 {
		PostError("missing value in cast statement")
		return P_FAIL
	}

	conv_type, count := ParseTypeSpec(children, "a cast")
	if conv_type == nil {
		return P_FAIL
	}
	children = children[count:]

	if len(children) == 0 {
		PostError("missing value in cast statement")
		return P_FAIL
	}

	L := ctx.ProcessTerm(children)
	if L == P_FAIL {
		return P_FAIL
	}

	if L.IsLiteral() || L.kind == NP_CSize {
		// FIXME strings
		if L.kind != NL_Null {
			PostError("cast cannot be used with a literal value")
			return P_FAIL
		}
		if conv_type != nil {
			L.ty = conv_type
		}
		return L
	}

	if conv_type == nil {
		if L.ty != nil && L.ty.kind == TYP_Pointer {
			conv_type = NewPointerType(rawmem_type)
		}
	}
	if conv_type == nil {
		PostError("missing type in cast statement")
		return P_FAIL
	}

	// check target type makes sense
	switch conv_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("target type of cast cannot be a %s", conv_type.SimpleName())
		return P_FAIL
	}

	src_type := L.ty
	if src_type == nil {
		panic("operand to cast has no type")
	}

	// check source type makes sense
	switch src_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("source type of cast cannot be a %s", src_type.SimpleName())
		return P_FAIL
	}

	// check combination of source + dest types.
	if conv_type.archy != src_type.archy {
		PostError("cannot cast a fixed size type and arch-dependent one")
		return P_FAIL
	}
	if conv_type.bits != src_type.bits {
		PostError("cast requires types with same size")
		return P_FAIL
	}
	if conv_type.kind != TYP_Pointer && conv_type.kind == src_type.kind {
		PostError("cannot cast between two %s types", conv_type.kind.String())
		return P_FAIL
	}

	if L.kind != NC_OpSeq {
		L = ctx.fu.ShiftComputation(L)
		seq := NewNode(NC_OpSeq, "", L.pos)
		seq.Add(L)
		L = seq
	}

	apply := NewNode(NX_Operator, "raw-cast", L.pos)
	apply.ty = conv_type

	L.Add(apply)
	L.ty = apply.ty

	return L
}

func (ctx *EvalContext) Term_MemRead(children []*Node) *Node {
	if len(children) < 1 || children[0].kind != NG_Data {
		PostError("missing access in [] for mem-read statement")
		return P_FAIL
	}
	if len(children) > 1 {
		PostError("extra rubbish at end of mem-read statement")
		return P_FAIL
	}

	t_acc := children[0]
	if t_acc.kind != NG_Data {
		PostError("need access in [] for mem-read, got: %s", t_acc.String())
		return P_FAIL
	}

	op := ctx.EvalTerm(t_acc)
	if op == P_FAIL {
		return P_FAIL
	}

	t_read := NewNode(NC_MemRead, "", t_acc.pos)
	t_read.ty = op.ty
	t_read.Add(op)

	return t_read
}

func (ctx *EvalContext) Term_StackVar(children []*Node) *Node {
	if len(children) < 1 {
		PostError("missing type in stack-var statement")
		return P_FAIL
	}

	ty, count := ParseTypeSpec(children, "a stack var")
	if ty == nil {
		return P_FAIL
	}

	if len(children) > count {
		PostError("extra rubbish at end of stack-var statement")
		return P_FAIL
	}

	t_stackvar := NewNode(NC_StackVar, "", children[0].pos)
	t_stackvar.ty = NewPointerType(ty)

	return t_stackvar
}

func (ctx *EvalContext) Term_Matches(children []*Node) *Node {
	if len(children) < 2 {
		PostError("missing values in matches? statement")
		return P_FAIL
	}

	// collect values, determine a type
	terms := make([]*Node, 0)

	var final_type *Type

	for _, t := range children {
		t_val := ctx.EvalTerm(t)
		if t_val == P_FAIL {
			return P_FAIL
		}

		terms = append(terms, t_val)

		if t_val.ty != nil {
			if final_type == nil {
				final_type = t_val.ty
			} else if TypeIsSubtype(final_type, t_val.ty) {
				final_type = t_val.ty
			}
		}
	}

	/* check types */

	if final_type == nil {
		PostError("unknown type for matches? (untyped literals)")
		return P_FAIL
	}
	if final_type.kind == TYP_Bool {
		PostError("cannot use bool type in matches?")
		return P_FAIL
	}

	// WISH: optimise away unmatchable literals
	//       e.g. x is 8 bits, but we have `matches? x 3456`

	t_match := NewNode(NC_MultiEq, "", children[0].pos)
	t_match.ty = bool_type

	for _, t := range terms {
		t2 := ctx.fu.DoTypeStuff(t, "matches?", final_type)
		if t2 == P_FAIL {
			return P_FAIL
		}
		t2 = ctx.fu.ShiftComputation(t2)

		t_match.Add(t2)
	}

	return t_match
}

func (ctx *EvalContext) Term_MinMax(children []*Node, is_max bool) *Node {
	if len(children) < 2 {
		PostError("missing values in min/max statement")
		return P_FAIL
	}

	// collect values, determine a type
	terms := make([]*Node, 0)

	var final_type *Type

	for _, t := range children {
		t_val := ctx.EvalTerm(t)
		if t_val == P_FAIL {
			return P_FAIL
		}

		terms = append(terms, t_val)

		if t_val.ty != nil {
			if final_type == nil {
				final_type = t_val.ty
			} else if TypeIsSubtype(final_type, t_val.ty) {
				final_type = t_val.ty
			}
		}
	}

	/* check types */

	if final_type == nil {
		PostError("unknown type for min/max (untyped literals)")
		return P_FAIL
	}
	if final_type.kind == TYP_Bool {
		PostError("cannot use bool type in min/max")
		return P_FAIL
	}

	// determine name of operation
	op_name := "???"

	if final_type.kind == TYP_Float {
		if is_max {
			op_name = "fmax"
		} else {
			op_name = "fmin"
		}
	} else {
		if is_max {
			op_name = "imax"
		} else {
			op_name = "imin"
		}
	}

	// WISH: optimise away unneeded terms, e.g. in: `min 1 x 3`

	// build operation sequence
	t_minmax := NewNode(NC_OpSeq, "", children[0].pos)
	t_minmax.ty = final_type

	for idx, t := range terms {
		t2 := ctx.fu.DoTypeStuff(t, "min/max", final_type)
		if t2 == P_FAIL {
			return P_FAIL
		}
		t2 = ctx.fu.ShiftComputation(t2)

		if idx == 0 {
			t_minmax.Add(t2)
		} else {
			op := NewNode(NX_Operator, op_name, t.pos)
			op.ty = final_type
			op.Add(t2)

			t_minmax.Add(op)
		}
	}

	return t_minmax
}

func (ctx *EvalContext) Term_Endian(children []*Node, is_big bool) *Node {
	if len(children) < 1 {
		PostError("missing value in endian statement")
		return P_FAIL
	}

	L := ctx.ProcessTerm(children)
	if L == P_FAIL {
		return P_FAIL
	}
	if L.ty == nil {
		PostError("unknown type for endian (untyped literal)")
		return P_FAIL
	}
	if L.ty.kind != TYP_Int {
		PostError("type not supported by endian: %s", L.ty.String())
		return P_FAIL
	}

	if Options.big_endian == is_big {
		// nothing needed if endianness is same as target arch
		return L
	}

	final_type := L.ty

	if L.kind != NC_OpSeq {
		L = ctx.fu.ShiftComputation(L)
		seq := NewNode(NC_OpSeq, "", L.pos)
		seq.Add(L)
		L = seq
	}

	apply := NewNode(NX_Operator, "endian-swap", L.pos)
	apply.ty = final_type

	L.Add(apply)
	L.ty = apply.ty

	return L
}

func (ctx *EvalContext) GrabReceiver(t_self *Node) *Node {
	rec := ctx.EvalTerm(t_self)
	if rec == P_FAIL {
		return P_FAIL
	}

	if rec.IsLiteral() || rec.kind == NP_CSize {
		PostError("method receiver cannot be a literal")
		return P_FAIL
	}

	self_type := rec.ty
	if self_type == nil {
		panic("receiver without a type")
	}
	if self_type.kind != TYP_Pointer {
		PostError("method receiver must be a pointer, got a %s",
			self_type.SimpleName())
		return P_FAIL
	}
	if self_type.sub.def == nil {
		PostError("method receiver must refer to a custom type")
		return P_FAIL
	}

	return ctx.fu.ShiftComputation(rec)
}

func (ctx *EvalContext) GrabParameters(t_call, recv *Node, params []*Node, fun_type *Type) error {
	if len(params) != len(fun_type.param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.param), len(params))
		return FAILED
	}

	for i, par := range fun_type.param {
		// the receiver of methods is already parsed
		if i == 0 && recv != nil {
			t_call.Add(recv)
			continue
		}

		op_par := ctx.EvalTerm(params[i])
		if op_par == P_FAIL {
			return FAILED
		}

		// allow a global function name, make it a pointer
		if op_par.kind == NP_FuncRef {
			fdef := op_par.def.d_func
			if fdef.inline {
				PostError("cannot take the address of an inline function")
				return FAILED
			}
			op_par.kind = NP_GlobPtr
			op_par.ty   = fdef.ptr
		}

		what := "parameter " + par.name

		op_par = ctx.fu.DoTypeStuff(op_par, what, par.ty)
		if op_par == P_FAIL {
			return FAILED
		}

		op_par = ctx.fu.ShiftComputation(op_par)

		t_call.Add(op_par)
	}

	return OK
}

//----------------------------------------------------------------------

func (ctx *EvalContext) EvaluateExpression(children []*Node) *Node {
	/* this is Dijkstra's shunting-yard algorithm */

	op_stack   := make([]*Operator, 0)
	term_stack := make([]*Node, 0)

	shunt := func() error {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		// possible??
		if len(term_stack) < 2 {
			PostError("FAILURE AT THE SHUNTING YARD")
			return FAILED
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]

		term := ctx.EvaluateBinary(op, L, R)
		if term == P_FAIL {
			return FAILED
		}

		term_stack = term_stack[0 : len(term_stack)-2]
		term_stack = append(term_stack, term)
		return OK
	}

	seen_term := false

	for _, t := range children {
		op := operators[t.str]

		if op != nil {
			if len(op.binary) == 0 {
				panic("OOPSIE IN MATH PARSE")
			}

			// shunt existing operators if they have greater precedence
			for len(op_stack) > 0 {
				top := op_stack[len(op_stack)-1]

				right_assoc := (op.name == "**")

				if top.prec > op.prec ||
					(top.prec == op.prec && !right_assoc) {

					if shunt() != OK {
						return P_FAIL
					}
					continue
				}
				break
			}

			op_stack  = append(op_stack, op)
			seen_term = false

		} else {
			// if not an operator, it must be a term
			term_stack = append(term_stack, t)
			seen_term  = true
		}
	}

	if !seen_term {
		PostError("bad expression, missing term after last operator")
		return P_FAIL
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		if shunt() != OK {
			return P_FAIL
		}
	}

	if len(term_stack) != 1 {
		panic("PARSE MATH FAIlURE")
	}

	// replace input token with root of the expression tree
	return term_stack[0]
}

func (ctx *EvalContext) EvaluateUnary(op *Operator, L *Node) *Node {
	switch L.kind {
	case NL_Integer, NL_Char:
		// if literal has a type, cannot use const expr evaluator
		if L.ty == nil {
			return EV_UnaryInteger(op.name, L)
		}

	case NL_Float:
		if L.ty == nil {
			return EV_UnaryFloat(op.name, L)
		}

	case NL_FltSpec:
		PostError("detected an infinity or NaN in const expression")
		return P_FAIL

	case NL_Bool:
		return EV_UnaryBool(op.name, L)
	}

	if ctx.fu == nil {
		PostError("cannot use operator '%s' in that context", op.name)
		return P_FAIL
	}

	L_type := L.ty
	if L_type == nil {
		PostError("unknown type for %s OPERATOR (untyped literal)", op.name)
		return P_FAIL
	}

	full_name, exist := op.unary[L_type.kind]

	if !exist {
		PostError("type not supported by %s unary op: %s",
			op.name, L_type.String())
		return P_FAIL
	}

	final_type := L_type
	if op.comparison {
		final_type = bool_type
	}

	// optimise boolean not + `matches?`
	if op.name == "not" && L.kind == NC_MultiEq {
		L.negate = ! L.negate
		return L
	}

	if L.kind != NC_OpSeq {
		L = ctx.fu.ShiftComputation(L)
		seq := NewNode(NC_OpSeq, "", L.pos)
		seq.Add(L)
		L = seq
	}

	apply := NewNode(NX_Operator, full_name, L.pos)
	apply.ty = final_type

	L.Add(apply)
	L.ty = apply.ty

	return L
}

func (ctx *EvalContext) EvaluateBinary(op *Operator, L, R *Node) *Node {
	if L.IsLiteral() && R.IsLiteral() {
		// check types
		L_kind := L.kind
		R_kind := R.kind

		if L_kind == NL_FltSpec || R_kind == NL_FltSpec {
			PostError("detected an infinity or NaN in const expression")
			return P_FAIL
		}

		// allow mixing integer are character literals
		if L_kind == NL_Char { L_kind = NL_Integer }
		if R_kind == NL_Char { R_kind = NL_Integer }

		// when mixing integer and float, upgrade the integer to a float
		if L.ty == nil && L_kind == NL_Integer && R_kind == NL_Float {
			if IntegerToFloat(L) != OK {
				return P_FAIL
			}
			L_kind = NL_Float
		}
		if R.ty == nil && R_kind == NL_Integer && L_kind == NL_Float {
			if IntegerToFloat(R) != OK {
				return P_FAIL
			}
			R_kind = NL_Float
		}

		if L_kind != R_kind {
			PostError("type mismatch in const expression")
			return P_FAIL
		}

		switch L_kind {
		case NL_Integer:
			// if either literal has a type, cannot use const expr evaluator
			if L.ty == nil && R.ty == nil {
				// if one of the arguments is negative, use signed ints.
				// otherwise use unsigned ints to compute the result.
				if L.str[0] == '-' || R.str[0] == '-' {
					return EV_BinarySigned(op.name, L, R)
				} else {
					return EV_BinaryUnsigned(op.name, L, R)
				}
			}

		case NL_Float:
			if L.ty == nil && R.ty == nil {
				return EV_BinaryFloat(op.name, L, R)
			}

		case NL_String:
			return EV_BinaryString(op.name, L, R)

		case NL_Bool:
			return EV_BinaryBool(op.name, L, R)
		}
	}

	if ctx.fu == nil {
		PostError("cannot use operator '%s' in that context", op.name)
		return P_FAIL
	}

	/* determine types, check L and R are compatible */

	L_type := L.ty
	R_type := R.ty

	if L_type == nil && (R_type == nil || op.name == "<<" || op.name == ">>") {
		PostError("unknown type for %s operator (untyped literals)", op.name)
		return P_FAIL
	}

	if L_type == nil {
		L.ty = R_type
		L_type = L.ty

		if L_type.kind == TYP_Float {
			switch L.kind {
			case NL_Integer, NL_Char:
				if IntegerToFloat(L) != OK {
					return P_FAIL
				}
			}
		}
		if ctx.fu.CheckLiteral(L) != OK {
			return P_FAIL
		}
	}

	if R_type == nil {
		if op.name == "<<" || op.name == ">>" {
			R.ty = u8_type
		} else if L_type.kind == TYP_Pointer && (op.name == "+" || op.name == "-") {
			R.ty = ssize_type
		} else {
			R.ty = L_type
		}

		R_type = R.ty

		if R_type.kind == TYP_Float {
			switch R.kind {
			case NL_Integer, NL_Char:
				if IntegerToFloat(R) != OK {
					return P_FAIL
				}
			}
		}
		if ctx.fu.CheckLiteral(R) != OK {
			return P_FAIL
		}
	}

	final_type := L_type
	if op.comparison {
		final_type = bool_type
	}

	full_name, exist := op.binary[L_type.kind]

	if !exist {
		PostError("type not supported by %s operator: %s",
			op.name, L_type.String())
		return P_FAIL
	}

	if op.name == "-" && L_type.kind == TYP_Pointer && R_type.kind == TYP_Pointer {
		// ok -- pointer difference is lax with pointer types
		final_type = ssize_type

	} else if (op.name == "+" || op.name == "-") && L_type.kind == TYP_Pointer {
		// these just need an integer, size does not matter
		if R_type.kind != TYP_Int {
			PostError("type not supported by pointer addition: %s",
				R_type.String())
			return P_FAIL
		}

		// need to convert to an arch-dependent type, if not already.
		// using one of same signed-ness may be more efficient.
		if !R_type.archy {
			if R_type.unsigned {
				R_type = usize_type
			} else {
				R_type = ssize_type
			}
			R = ctx.fu.DoConvert(R, R_type)
		}

	} else if op.name == "<<" || op.name == ">>" {
		if R_type.kind != TYP_Int {
			PostError("type not supported for integer shift: %s",
				R_type.String())
			return P_FAIL
		}

		// check that a constant shift count is valid
		if R.kind == NL_Integer {
			val, err := strconv.ParseInt(R.str, 0, 64)

			if err != nil || val < 0 {
				PostError("invalid shift count: %s", R.str)
				return P_FAIL
			}
			if val >= int64(L_type.bits) {
				PostError("shift count too high (>= %d)", L_type.bits)
				return P_FAIL
			}
		}

	} else {
		// check type compatibility, enable auto conversions

		if TypeIsSubtype(R_type, L_type) {
			R = ctx.fu.DoConvert(R, L_type)

		} else if TypeIsSubtype(L_type, R_type) {
			L = ctx.fu.DoConvert(L, R_type)
			if final_type == L_type {
				final_type = R_type
			}

		} else if !TypeCompare(L_type, R_type) {
			PostError("type mismatch between %s args: %s and %s",
				op.name, L_type.String(), R_type.String())
			return P_FAIL
		}
	}

	// check for division by zero
	if op.name == "/" || op.name == "%" {
		if IsLiteralZero(R) {
			PostError("division by zero in expression")
			return P_FAIL
		}
	}

	// check for operations which have no effect
	if !Options.no_optim {
		switch full_name {
		case "iadd":
			if IsLiteralZero(L) { return R }
			if IsLiteralZero(R) { return L }

		case "isub":
			if IsLiteralZero(R) { return L }

		case "imul":
			if IsLiteralOne(L) { return R }
			if IsLiteralOne(R) { return L }

			if IsLiteralZero(L) { ctx.fu.ShiftNeeded(R) ; return L }
			if IsLiteralZero(R) { ctx.fu.ShiftNeeded(L) ; return R }

		case "idivt":
			if IsLiteralOne(R)  { return L }
			if IsLiteralZero(L) { ctx.fu.ShiftNeeded(R) ; return L }

		case "iand":
			if IsLiteralZero(L) { ctx.fu.ShiftNeeded(R) ; return L }
			if IsLiteralZero(R) { ctx.fu.ShiftNeeded(L) ; return R }

		case "ior", "ixor":
			if IsLiteralZero(L) { return R }
			if IsLiteralZero(R) { return L }

		case "band":
			if IsLiteralTrue(L)  { return R }
			if IsLiteralTrue(R)  { return L }

			if IsLiteralFalse(L) { ctx.fu.ShiftNeeded(R) ; return L }
			if IsLiteralFalse(R) { ctx.fu.ShiftNeeded(L) ; return R }

		case "bor":
			if IsLiteralFalse(L) { return R }
			if IsLiteralFalse(R) { return L }

			if IsLiteralTrue(L)  { ctx.fu.ShiftNeeded(R) ; return L }
			if IsLiteralTrue(R)  { ctx.fu.ShiftNeeded(L) ; return R }
		}
	}

	// we prefer to build in a NC_OpSeq, for LHS this is natural,
	// but for RHS the current operator must allow swapping.

	if L.kind == NC_OpSeq {
		R = ctx.fu.ShiftComputation(R)

	} else if (R.kind == NC_OpSeq) && op.reversible {
		tmp := L ; L = R ; R = tmp
		R    = ctx.fu.ShiftComputation(R)

	} else {
		L = ctx.fu.ShiftComputation(L)
		R = ctx.fu.ShiftComputation(R)

		seq := NewNode(NC_OpSeq, "", L.pos)
		seq.Add(L)
		L = seq
	}

	apply := NewNode(NX_Operator, full_name, L.pos)
	apply.ty = final_type
	apply.Add(R)

	L.Add(apply)
	L.ty = apply.ty

	return L
}

func (ctx *EvalContext) EvalTerm(t *Node) *Node {
	// for const expressions, returns a literal token (like NL_Integer),
	// or P_FAIL when there was a parsing problem.  A constant which has
	// not been resolved yet will return P_UNKNOWN.
	//
	// Within function bodies, may also return computations (like NC_OpSeq)
	// and operands (like NP_Local).

	if t.kind == NG_Expr {
		if len(t.children) == 0 {
			PostError("missing computation in ()")
			return P_FAIL
		}
		return ctx.ProcessExpression(t.children)
	}

	if ctx.fu != nil {
		return ctx.fu.ParseOperand(t, true, true)
	}

	switch t.kind {
	case NL_Integer, NL_Char, NL_Float, NL_FltSpec, NL_String, NL_Bool:
		return t

	case NL_Name:
		if !ValidateModUsage(t.module) {
			return P_FAIL
		}
		ReplaceAlias(t)

		def, mod := LookupDef(t.str, t.module)

		if def == nil || def.kind != DEF_Const {
			PostError("no such constant: %s", t.str)
			return P_FAIL
		}
		if mod != cur_mod && !def.IsPublic() {
			PostError("%s is private to module %s", t.str, mod.name)
			return P_FAIL
		}

		other := def.d_const

		if other.expr != nil {
			if ctx.fu != nil {
				panic("uneval constant in function context")
			}
			return P_UNKNOWN
		}
		return other.MakeLiteral(t.pos)

	default:
		PostError("bad value in const expression, got: %s", t.String())
		return P_FAIL
	}
}

func EV_UnaryBool(op_name string, L *Node) *Node {
	switch op_name {
	case "not":
		V := (L.str == "1")
		return EV_MakeBool(! V, L.pos)
	}

	PostError("cannot use operator '%s' in a boolean const-expr", op_name)
	return P_FAIL
}

func EV_BinaryBool(op_name string, L, R *Node) *Node {
	LV := (L.str == "1")
	RV := (R.str == "1")

	var result bool

	switch op_name {
	case "and": result = LV && RV
	case "or":  result = LV || RV
	case "==":  result = LV == RV
	case "!=":  result = LV != RV
	default:
		PostError("cannot use operator '%s' in a boolean const-expr", op_name)
		return P_FAIL
	}

	return EV_MakeBool(result, L.pos)
}

func EV_UnaryFloat(op_name string, L *Node) *Node {
	V, V_err := strconv.ParseFloat(L.str, 64)
	if V_err != nil {
		PostError("bad float value in const expression: %s", L.str)
		return P_FAIL
	}

	switch op_name {
	case "-":
		return EV_MakeFloat(0.0 - V, L.pos)

	case "abs":
		return EV_MakeFloat(math.Abs(V), L.pos)

	case "sqrt":
		if V < 0 {
			PostError("square root of a negative value")
			return P_FAIL
		}
		return EV_MakeFloat(math.Sqrt(V), L.pos)

	case "round": V = math.Round(V)
	case "trunc": V = math.Trunc(V)
	case "floor": V = math.Floor(V)
	case "ceil":  V = math.Ceil (V)

	default:
		PostError("cannot use operator '%s' in a float const-expr", op_name)
		return P_FAIL
	}

	term := EV_MakeFloat(V, L.pos)

	if FloatToInteger(term) != OK {
		return P_FAIL
	}
	return term
}

func EV_BinaryFloat(op_name string, L, R *Node) *Node {
	// WISH: format result as hexadecimal if LHS is hex.

	LV, L_err := strconv.ParseFloat(L.str, 64)
	RV, R_err := strconv.ParseFloat(R.str, 64)

	if L_err != nil {
		PostError("bad float value in const expression: %s", L.str)
		return P_FAIL
	}
	if R_err != nil {
		PostError("bad float value in const expression: %s", R.str)
		return P_FAIL
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV == 0.0 {
			PostError("division by zero in const expression")
			return P_FAIL
		}
	}

	var result float64

	switch op_name {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV - math.Trunc(LV / RV) * RV

	case "==": return EV_MakeBool(LV == RV, L.pos)
	case "!=": return EV_MakeBool(LV != RV, L.pos)
	case "<":  return EV_MakeBool(LV <  RV, L.pos)
	case "<=": return EV_MakeBool(LV <= RV, L.pos)
	case ">":  return EV_MakeBool(LV >  RV, L.pos)
	case ">=": return EV_MakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' in a float const-expr", op_name)
		return P_FAIL
	}

	if InfinityOrNan(result) {
		PostError("detected an infinity or NaN in const expression")
		return P_FAIL
	}

	return EV_MakeFloat(result, L.pos)
}

func EV_UnaryInteger(op_name string, L *Node) *Node {
	V, V_err := strconv.ParseInt(L.str, 0, 64)
	if V_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return P_FAIL
	}

	switch op_name {
	case "-", "~":
		if op_name == "-" {
			V = 0 - V
		} else {
			V = 0 - (V + 1)
		}
		int_str := strconv.FormatInt(V, 10)
		return NewNode(NL_Integer, int_str, L.pos)

	case "abs":
		if V < 0 {
			V = 0 - V
		}
		int_str := strconv.FormatInt(V, 10)
		return NewNode(NL_Integer, int_str, L.pos)
	}

	PostError("cannot use operator '%s' in an integer const-expr")
	return P_FAIL
}

func EV_BinarySigned(op_name string, L, R *Node) *Node {
	LV, L_err := strconv.ParseInt(L.str, 0, 64)
	RV, R_err := strconv.ParseInt(R.str, 0, 64)

	if L_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return P_FAIL
	}
	if R_err != nil {
		PostError("bad integer in const expression: %s", R.str)
		return P_FAIL
	}

	// check for bad shift count
	if op_name == "<<" || op_name == ">>" {
		if RV < 0 {
			PostError("bad shift count in const expression")
			return P_FAIL
		}
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV == 0 {
			PostError("division by zero in const expression")
			return P_FAIL
		}
	}

	var result int64

	switch op_name {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV % RV

	case "&":  result = LV & RV
	case "|":  result = LV | RV
	case "~":  result = LV ^ RV
	case "<<": result = LV << uint64(RV)
	case ">>": result = LV >> uint64(RV)

	case "==": return EV_MakeBool(LV == RV, L.pos)
	case "!=": return EV_MakeBool(LV != RV, L.pos)
	case "<":  return EV_MakeBool(LV <  RV, L.pos)
	case "<=": return EV_MakeBool(LV <= RV, L.pos)
	case ">":  return EV_MakeBool(LV >  RV, L.pos)
	case ">=": return EV_MakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' in an integer const-expr", op_name)
		return P_FAIL
	}

	// WISH: format result as hexadecimal if LHS is hex.

	int_str := strconv.FormatInt(result, 10)
	return NewNode(NL_Integer, int_str, L.pos)
}

func EV_BinaryUnsigned(op_name string, L, R *Node) *Node {
	LV, L_err := strconv.ParseUint(L.str, 0, 64)
	RV, R_err := strconv.ParseUint(R.str, 0, 64)

	if L_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return P_FAIL
	}
	if R_err != nil {
		PostError("bad integer in const expression: %s", R.str)
		return P_FAIL
	}

	// check for subtraction producing a negative value
	if op_name == "-" && RV > LV {
		result  := int64(LV) - int64(RV)
		int_str := strconv.FormatInt(result, 10)
		return NewNode(NL_Integer, int_str, L.pos)
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV == 0 {
			PostError("division by zero in const expression")
			return P_FAIL
		}
	}

	var result uint64

	switch op_name {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV % RV

	case "&":  result = LV & RV
	case "|":  result = LV | RV
	case "~":  result = LV ^ RV
	case "<<": result = LV << RV
	case ">>": result = LV >> RV

	case "==": return EV_MakeBool(LV == RV, L.pos)
	case "!=": return EV_MakeBool(LV != RV, L.pos)
	case "<":  return EV_MakeBool(LV <  RV, L.pos)
	case "<=": return EV_MakeBool(LV <= RV, L.pos)
	case ">":  return EV_MakeBool(LV >  RV, L.pos)
	case ">=": return EV_MakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' in an integer const-expr", op_name)
		return P_FAIL
	}

	// WISH: format result as hexadecimal if LHS is hex.

	int_str := strconv.FormatUint(result, 10)
	return NewNode(NL_Integer, int_str, L.pos)
}

func EV_BinaryString(op_name string, L, R *Node) *Node {
	if op_name != "+" {
		PostError("cannot use operator '%s' on strings", op_name)
		return P_FAIL
	}

	return NewNode(NL_String, L.str + R.str, L.pos)
}

func EV_MakeFloat(V float64, pos Position) *Node {
	term := NewNode(NL_Float, "", pos)
	term.str = strconv.FormatFloat(V, 'g', -1, 64)

	// ensure representation has a dot
	if strings.ContainsRune(term.str, '.') {
		// ok
	} else if strings.ContainsRune(term.str, 'e') {
		// ok
	} else {
		term.str = term.str + ".0"
	}
	return term
}

func EV_MakeBool(b bool, pos Position) *Node {
	term := NewNode(NL_Bool, "0", pos)
	term.ty = bool_type

	if b {
		term.str = "1"
	}
	return term
}
