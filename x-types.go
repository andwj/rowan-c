// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "strconv"

func (gen *Generator) FormVarDefinition(type_name, def_name string) string {
	if strings.IndexRune(type_name, '%') < 0 {
		return type_name + " " + def_name
	}

	// replace the percent sign with the variable name
	var sb strings.Builder

	for _, ch := range type_name {
		if ch == '%' {
			sb.WriteString(def_name)
		} else {
			sb.WriteRune(ch)
		}
	}
	return sb.String()
}

//----------------------------------------------------------------------

// ConvertType converts a Rowan type into an equivalent C type.
// New C typedefs may be created here.
//
// If `var_decl` is true, the returned string may contain a `%` where
// the variable name should go.  If false, the string is always a
// complete type (no percent sign).
//
// WISH: check if any code/vars actually uses the fields of a
//       struct/union, produce an "opaque" definition if not.
func (gen *Generator) ConvertType(ty *Type, var_decl bool, parent *OutThing) string {
	def := ty.def

	if def != nil {
		out := ty._out
		if out == nil {
			name := "ty_" + gen.EncodeName(def.name)
			gen.BuildOutType(ty, name)
			out = ty._out
		}

		if parent != nil {
			parent.AddDependency(out)
		}
		return out.name
	}

	return gen.ConvertRawType(ty, var_decl, parent)
}

func (gen *Generator) ConvertRawType(ty *Type, var_decl bool, parent *OutThing) string {
	// basic types (except pointers) are easy

	if ty == rawmem_type {
		return "_rwRawMem"
	}

	switch ty.kind {
	case TYP_Void:
		return "void"

	case TYP_NoReturn:
		// this must be handled elsewhere (x-funcs code)
		return "void"

	case TYP_Extern:
		return "_rwExtern"

	case TYP_Bool:
		return "_rwBool"

	case TYP_Int:
		if ty.unsigned {
			if ty.archy {
				return "uintptr_t"
			}
			switch ty.bits {
			case 8:  return "uint8_t"
			case 16: return "uint16_t"
			case 32: return "uint32_t"
			case 64: return "uint64_t"
			}
		} else {
			if ty.archy {
				return "intptr_t"
			}
			switch ty.bits {
			case 8:  return "int8_t"
			case 16: return "int16_t"
			case 32: return "int32_t"
			case 64: return "int64_t"
			}
		}
		panic("weird integer type in ConvertType")

	case TYP_Float:
		if ty.bits == 32 {
			return "float"
		} else if ty.bits == 64 {
			return "double"
		}
		panic("weird float type in ConvertType")

	case TYP_Pointer:
		if parent != nil && (ty.sub.kind == TYP_Struct || ty.sub.kind == TYP_Union) {
			parent = nil
		}

		// pointers to an array become pointers to the element type.
		// but for arrays of arrays, create a typedef for inner array.
		sub_type := ty.sub
		if sub_type.kind == TYP_Array {
			sub_type = sub_type.sub
		}

		sub := gen.ConvertType(sub_type, false, parent)
		return sub + "*"

	case TYP_Array:
		// TODO review handling of open arrays (elems == 0)
		if var_decl {
			sub := gen.ConvertType(ty.sub, false, parent)
			size_str := strconv.Itoa(ty.elems)
			return sub + " %[" + size_str + "]"
		} else {
			return gen.CreateArrayType(ty, parent)
		}

	default:
		// TYP_Struct, TYP_Union and TYP_Function cannot occur here, as they
		// can only be defined by a Rowan type definition (never "in line").
		panic("weird struct/union/func type")
	}
}

func (gen *Generator) ConvertParameters(ty *Type, parent *OutThing) string {
	var sb strings.Builder

	sb.WriteRune('(')

	if len(ty.param) == 0 {
		sb.WriteString("void")
	} else {
		for i, par := range ty.param {
			if i > 0 {
				sb.WriteString(", ")
			}
			out_name  := gen.EncodeName(par.name)
			type_name := gen.ConvertType(par.ty, true, parent)
			full_def  := gen.FormVarDefinition(type_name, out_name)

			sb.WriteString(full_def)
		}
	}

	sb.WriteRune(')')

	return sb.String()
}

var array_name_num int = 0

func (gen *Generator) CreateArrayType(ty *Type, parent *OutThing) string {
	if ty._out != nil {
		return ty._out.name
	}

	array_name_num += 1
	name := fmt.Sprintf("_rwArray%03d", array_name_num)

	gen.BuildOutType(ty, name)

	if parent != nil {
		parent.AddDependency(ty._out)
	}
	return name
}

//----------------------------------------------------------------------

func (gen *Generator) BuildOutType(ty *Type, name string) {
	out := NewOutThing(ODEF_Type, name)
	ty._out = out

///	println("BuildOutType:", name)

	// create forward declaration
	switch ty.kind {
	case TYP_Struct:
		out.forward  = fmt.Sprintf("typedef struct %s_s %s;", out.name, out.name)
		out.required = true

	case TYP_Union:
		out.forward  = fmt.Sprintf("typedef union %s_u %s;", out.name, out.name)
		out.required = true
	}

	// create full C definition

	switch ty.kind {
	case TYP_Pointer:
		sub := gen.ConvertType(ty.sub, false, out)
		out.Line("typedef %s* %s;", sub, out.name)

	case TYP_Array:
		if ty.elems == 0 {
			// treat an open array as a pointer
			sub := gen.ConvertType(ty.sub, false, out)
			out.Line("typedef %s* %s;", sub, out.name)
		} else {
			sub := gen.ConvertType(ty.sub, false, out)
			size_str := strconv.Itoa(ty.elems)
			out.Line("typedef %s %s[%s];", sub, out.name, size_str)
		}

	case TYP_Struct, TYP_Union:
		what := ""
		name := out.name

		if ty.kind == TYP_Struct {
			what = "struct"
			name = name + "_s"
		} else {
			what = "union"
			name = name + "_u"
		}

		if !ty.padded {
			what = what + " _RW_PACKED_ATTR"
			out.BeginPacked()
		}

		out.Line("%s %s {", what, name)
		for _, field := range ty.param {
			full_def := gen.BuildConvField(field, out)
			out.Line("\t%s;", full_def)
		}
		out.Line("};")

		if !ty.padded {
			out.EndPacked()
		}

	case TYP_Function:
		ret  := gen.ConvertType(ty.sub, false, out)
		pars := gen.ConvertParameters(ty, out)
		out.Line("typedef %s %s%s;", ret, out.name, pars)

	default:
		// handle type aliases, like: type Duration s32
		sub := gen.ConvertRawType(ty, false, nil)
		out.forward  = fmt.Sprintf("typedef %s %s;", sub, out.name)
		out.required = true
	}

	gen.AddThing(out)
}

func (gen *Generator) BuildConvField(field ParamInfo, parent *OutThing) string {
	out_name  := gen.EncodeFieldName(field.name)
	type_name := gen.ConvertType(field.ty, true, parent)
	full_def  := gen.FormVarDefinition(type_name, out_name)

	return full_def
}
